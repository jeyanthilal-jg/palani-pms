Handlebars.registerHelper('formatFloat', function(value) {
    return value.toFixed(2);
});
Handlebars.registerHelper('basURL', function(value) {
    return $('#base').val();
});

var baseUrl = $('#base').val();
var dboardView = $("#template-meterWidgets").html();
var dboardTemplate = Handlebars.compile(dboardView);  
var jsonMeters = $.parseJSON(meterList);
var mid = $('.panel-group:first-child').data('id');
var loaderImg =  '<div class="graph-preloader" style="height: 200px; position: relative;top: 10px"><svg class="circular" viewBox="25 25 50 50">              <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"></circle></svg></div>';

function templateMeter(meterItems){
	
	console.log(meterItems.meter_id);
	var meter_id = meterItems.meter_id
	var finalMeter = { objects : meterItems  }
	var viewLoad = dboardTemplate(finalMeter);
	$("#collapse-"+meter_id).html(viewLoad);
}



function update_live_data (meter_id){

var ajaxData = $.getJSON(baseUrl+"common/getSingle_meter", { id  : meter_id },function(result){
	 templateMeter(result);
   });
}

function toggleIcon(e) {
//console.log(e.type);
    $(e.target)
        .prev('.panel-heading')
        .find(".more-less")
        .toggleClass('fa-plus-circle fa-minus-circle');

  if(e.type=="shown"){ 
  	 $(e.target).closest('.panel-collapse').empty().append(loaderImg);
	  mid = $(e.target).attr('data-id');
	  update_live_data(mid);
	}
}
//$('.panel-group').on('hidden.bs.collapse', toggleIcon);
//$('.panel-group').on('shown.bs.collapse', toggleIcon); 
$(document).on('hidden.bs.collapse', '.panel-group', toggleIcon);
$(document).on('shown.bs.collapse', '.panel-group', toggleIcon);



$('[data-toggle="collapse"]').click(function(e) {
    //console.log(e.target);
  $('.collapse.show').collapse('hide');
});  

update_live_data(mid);

setInterval( function() { update_live_data(mid) }, 30000 );   
