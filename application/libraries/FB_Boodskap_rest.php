<?php
/**
 * Name:    Fourbends Boodskap API
 * Author:  Fourbends Dev Team
 *          contactus@fourbends.com
 * Url:    http://www.fourbends.com/
 *
 *
 * Created:  02.03.2018
 *
 * Description:  This file is used to connect our PHP system to Boodskap API.
 * Original Author name has been kept but that does not mean that the method has not been modified.
 *
 * Requirements: PHP5 or above
 *
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

get_instance()->load->iface('FbsIotRestInterface');

class FB_Boodskap_rest implements FbsIotRestInterface
{

	protected $CI;
    protected $tables;
	protected $tbl_actvy;
	
	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->config->load('fb_boodskap', TRUE);
		$this->tables = $this->CI->config->item('tables', 'fb_boodskap');
		$this->tbl_actvy = "activities";
		require_once( APPPATH . 'third_party/boodskap/vendor/autoload.php');
		$this->CI->load->helper('security');
		$this->CI->load->driver('cache', array('adapter' => 'file'));
		$this->CI->load->library('parser');
		$this->CI->parser->set_delimiters("__","__");

	}
	
	public function login($email, $password){
		
		$api_instance = new Swagger\Client\Api\LoginApi();
		
		try {
			$result = $api_instance->login($email, $password);
			
			$user_token = $result->getToken();
			$domain_key = $result->getDomainKey();
			$api_key = $result->getApiKey();
			$cUser = $result->getUser();
			$email = $cUser->getEmail();
			$first_name = $cUser->getFirstName();
			$last_name = $cUser->getLastName();
			$country = $cUser->getCountry();
			$state = $cUser->getState();
			$city = $cUser->getCity();
			$address = $cUser->getAddress();
			$zipcode = $cUser->getZipcode();
			$locale = $cUser->getLocale();
			$timezone = $cUser->getTimezone();
			
			$udata = array(
				"user_token" => $user_token,
				"domain_key" => $domain_key,
				"api_key" => $api_key,
			    "email" => $email,
				"first_name" => $first_name,
				"last_name" => $last_name,
				"country" => $country,
				"state" => $state,
				"city" => $city,
				"address" => $address,
				"zipcode" => $zipcode,
				"locale" => $locale,
				"timezone" => $timezone
			);
			$this->CI->session->set_userdata($udata); // Set the session data
			$msg = array("status" => "success", "message" => "Successfully Loggedin");
			return $msg;
		} catch (Exception $e) {
			$smsg = $e->getMessage();
			log_message('error', "Exception when calling LoginApi->login: ".$smsg);
			$msg = array("status" => "fail", "message" => "Invalid Login");
			return $msg;
		}
	}
	
	public function isloggedin(){
		$status = $this->CI->session->has_userdata("user_token");
		return $status;
	}
	
	public function get_fbuser_data($name){
		$udata = $this->CI->session->has_userdata($name);
		if($udata){
			return $this->CI->session->userdata($name);
		}else{
			return "";
		}
	}
	
	public function create_record($table_name = "", $idata = array(), $activity = false){

		if(!$this->isloggedin()){
			$msg = array("status" => "fail", "message" => "Please login to continue");
			return $msg;
		}
			//Remove Leading zero
		foreach($idata as $key=>$val)
		{	
				if(($key<>'pond_id') && ($key<>'from_pond_id') && ($key<>'to_pond_id') && ($key<>'species_id'))
				$val = ltrim($val, '0');				
				$idata[$key]=$val;
		} 

		
		$tcond = (isset($this->tables[$table_name]) && !empty($idata) );
		if($tcond){
			$api_instance = new Swagger\Client\Api\UpsertRecordApi(new GuzzleHttp\Client());
			$tbl_id = $this->tables[$table_name]; // table idata
			$user_token = $this->get_fbuser_data("user_token");
			$ndata = $this->fb_setrecord($table_name, $idata);
			$sdata = json_encode($ndata);
			
			try {
				$result = $api_instance->upsertRecord($user_token, $tbl_id, $sdata);
				$ikey = $result->getRkey();
				/* if(!$activity){
					$this->update_activities($table_name, $ndata, 'add', array() , $ikey); // update the activities
				} */
				
				$msg = array("status" => "success", "message" => "Successfully created record", "rkey" => $ikey);
				return $msg;
			} catch (Exception $e) {
				$smsg = $e->getMessage();
				log_message('error', "Exception when calling UpdateRecordApi->upsertRecord: ".$smsg);
				$msg = array("status" => "fail", "message" => "Invalid Table or Data or Boodskap Server issue");
				return $msg;
			}
			
		}else{
			$msg = array("status" => "fail", "message" => "Invalid Table or Data");
			return $msg;
		}
		
	}
	
	public function test_list_record($table_name = ""){
		if(!$this->isloggedin()){
			$msg = array("status" => "fail", "message" => "Please login to continue");
			fb_pr($msg);
			return $msg;
		}
		
		$api_instance = new Swagger\Client\Api\SearchByQueryApi(new GuzzleHttp\Client());
		$atoken = $this->get_fbuser_data("user_token");
		$tbl_id = $this->tables[$table_name];
		$type = "RECORD";
		$query = new \Swagger\Client\Model\SearchQuery(); // \Swagger\Client\Model\SearchQuery | SearchQuery JSON
		$queryObj = array();
		//$queryObj['query']= new stdClass();
		//"match_all": {} 
		$queryObj['size'] = 1000;
		$qmain = new stdClass();
		$qmain->match_all = new stdClass();
		$queryObj['query']= $qmain;
		$queryObj['sort'] = array("status_off" => array("order" => "desc" )); 
		$qstr = json_encode($queryObj);
		$query['query']= $qstr;
		$query['method']="POST";
		$repositary = "";
		$mapping = "";
		try {
			$sresult = $api_instance->searchByQuery($atoken, $type, $query, $tbl_id, $repositary, $mapping);
			$result = $sresult->getResult();
			print_r($result);
		} catch (Exception $e) {
			echo 'Exception when calling SearchByQueryApi->search: ', $e->getMessage(), PHP_EOL;
		}

	}
	
	// Parameters list ("page_no", "per_page", "uri_segment", "search", "sort_fld", "sort_dir", "page_burl", "table_name")
	
	public function list_record($params = array()){
		if(!$this->isloggedin()){
			$msg = array("status" => "fail", "message" => "Please login to continue");
			return $msg;
		}
		
		$pre_params =  array("page_no" => "1", "per_page" => "10", "uri_segment" => "2", "search" => "", "sort_fld" => "updatedtime", "sort_dir" => "desc",
		"page_burl" => site_url("/"), "table_name" => "");
		
		foreach($pre_params as $pk => $pv){
			if(!isset($params[$pk]) || empty($params[$pk])){
				$params[$pk] = $pv;
			}
		}

		extract($params);
		
		$enc_str = do_hash(json_encode($params));
		$file_name = $table_name.'-'.$enc_str;
		if ( ! $flist_str = $this->CI->cache->get($file_name))
		{ 
			$from = ($page_no <= 1) ? "0" : ( $page_no - 1 ) ;
			$from = ($from * $per_page);
			//"from" : 0
			$page_params = compact("per_page", "uri_segment", "page_burl");
			
			$tcond = isset($this->tables[$table_name]);
			if($tcond){
				$api_instance = new Swagger\Client\Api\SearchByQueryApi(new GuzzleHttp\Client());
				$atoken = $this->get_fbuser_data("user_token");
				$tbl_id = $this->tables[$table_name];
				$type = "RECORD";
				$query = new \Swagger\Client\Model\SearchQuery(); // \Swagger\Client\Model\SearchQuery | SearchQuery JSON
				$search =  (!empty($search)) ? trim($search, "*") : "";
				$range_params = array();
				$chk_dflg = fb_chk_date($search);
				
				if($chk_dflg){
					$range_params = fb_sdate_qstr($search, $table_name);
				}

				// Query string load from parser
				
				$qper_page = $per_page;
				if($per_page=="-1"){
					$tc = $this->get_total_count($table_name);
					$qper_page = ($tc!==false)? $tc : 10000;
				}
				$page_params["per_page"] = $qper_page;
				
				$qpms = array("size" => $qper_page, "search" => $search, "chk_dflg" => $chk_dflg,
				"from" => $from, "orderfld" => $sort_fld, "orderdir" => $sort_dir);
				$qpms = array_merge($qpms, $range_params);
				
				$query_str = $this->CI->parser->parse('query/query-list', $qpms, true);
				
				$query['query']= $query_str;
				$query['method']="POST";
				$repositary = "";
				$mapping = "";
				try {
					$oresult = $api_instance->searchByQuery($atoken, $type, $query, $tbl_id, $repositary, $mapping);
					$sresult = $oresult->getResult();
					$aresult = json_decode($sresult, true);
					$total_count = isset($aresult["hits"]["total"]) ? $aresult["hits"]["total"]: "0";
					$result_set = isset($aresult["hits"]["hits"]) ? $aresult["hits"]["hits"] : array();
					$page_params["total_rows"] = $total_count;
					$page_links = fb_generate_pagination($page_params);
					$page_links = ( !empty($result_set) ) ? $page_links : "";
					$msg = array("status" => "success", "message" => "Search result got it", "total_count" => $total_count,
					"page_links" => $page_links, "result_set" => $result_set);
					
					$flist_str = json_encode($msg);
					if(chk_rst_cache()){
						$this->CI->cache->save($file_name, $flist_str, 86400);
					}
					
					return $msg;
				} catch (Exception $e) {
					$smsg = $e->getMessage();
					log_message('error', "Exception when calling SearchByQueryApi->search: ".$smsg);
					$msg = array("status" => "fail", "message" => "Search functionality error");
					return $msg;
				}
				
			}else{
				$msg = array("status" => "fail", "message" => "Invalid Table name");
				return $msg;
			}
		
		} else {
			$msg = json_decode($flist_str, true);
			return $msg;
		}
		
		
	}
	
	private function fb_setrecord($table_name = "", $idata = array()){
		$newdata = array();
		$tblkeys = $this->CI->config->item($table_name, 'fb_boodskap');
		foreach($tblkeys as $tblkey){
			$newdata[$tblkey] = isset($idata[$tblkey]) ? trim($idata[$tblkey]) : "";
		}
		return $newdata;
	}
	
	private function fb_updaterecord($table_name = "", $idata = array(), $odata = array()){
		$newdata = array();
		$tblkeys = $this->CI->config->item($table_name, 'fb_boodskap');
		foreach($tblkeys as $tblkey){
			$newdata[$tblkey] = isset($idata[$tblkey]) ? trim($idata[$tblkey]) : $odata[$tblkey];
		}
		return $newdata;
	}
	
	public function combo_list($table_name = "", $no_id = false){
		if(!$this->isloggedin()){
			$msg = array("status" => "fail", "message" => "Please login to continue");
			return $msg;
		}
		
		$combo_list = $this->CI->config->item('combo_list', 'fb_boodskap');
		if(!isset($combo_list[$table_name])){
			$msg = array("status" => "fail", "message" => "Please give the correct table name");
			return $msg;
		}
		
		// Cache implemented
		$sno_id = ($no_id)? "with-out-id" : "with-id";
		$file_name = $table_name."-".$sno_id;
		
		if ( ! $fcombo_str = $this->CI->cache->get($file_name))
		{
			$cmb_cfg = $combo_list[$table_name];
			$kfld_val = $cmb_cfg["key"];
			$vfld_val = $cmb_cfg["value"];
			$sfld_val = $cmb_cfg["sort_fld"];
			$sdir_val = $cmb_cfg["sort_dir"];
			$acombo = array();

			$api_instance = new Swagger\Client\Api\SearchByQueryApi(new GuzzleHttp\Client());
			$atoken = $this->get_fbuser_data("user_token");
			$tbl_id = $this->tables[$table_name];
			$type = "RECORD";
			$query = new \Swagger\Client\Model\SearchQuery(); // \Swagger\Client\Model\SearchQuery | SearchQuery JSON
			$queryObj = array();
			$queryObj['size'] = 10000;
			$qmain = new stdClass();
			$qmain->match_all = new stdClass();
			$queryObj['query']= $qmain;
			$queryObj['sort'] = array($sfld_val => array("order" => $sdir_val )); 
			$qstr = json_encode($queryObj);

			$query['query']= $qstr;
			$query['method']="GET";
			$repositary = "";
			$mapping = "";
			try {
				$oresult = $api_instance->searchByQuery($atoken, $type, $query, $tbl_id, $repositary, $mapping);
				$sresult = $oresult->getResult();
				$aresult = json_decode($sresult, true);
				$total_count = isset($aresult["hits"]["total"]) ? $aresult["hits"]["total"]: "0";
				$result_set = isset($aresult["hits"]["hits"]) ? $aresult["hits"]["hits"] : array();
				foreach($result_set as $row){
					$src = $row["_source"];
					$_id = $row["_id"];
					$k = ($kfld_val=="_id") ? $_id : $src[$kfld_val];
					$k = ($no_id) ? $src[$vfld_val] : $k;
					$v = $src[$vfld_val];
					$acombo[$k] = $v;
				}
				$fcombo_str = json_encode($acombo);
				if(chk_rst_cache()){
					$this->CI->cache->save($file_name, $fcombo_str, 86400);
				}
				$msg = array("status" => "success", "message" => "Fetch Table: $table_name result successfully", "combo_list" => $acombo);
				return $msg;
			} catch (Exception $e) {
				$smsg = $e->getMessage();
				log_message('error', "Exception when calling SearchByQueryApi->search: ".$smsg);
				$msg = array("status" => "fail", "message" => "Search functionality error");
				return $msg;
			}
		} else {
			$acombo = json_decode($fcombo_str, true);
			$msg = array("status" => "success", "message" => "Fetch Table: $table_name result successfully", "combo_list" => $acombo);
			return $msg;
		}
		
	}
	
	public function delete_record($table_name = "", $rkey = "", $activity = false){
		if(!$this->isloggedin()){
			$msg = array("status" => "fail", "message" => "Please login to continue");
			return $msg;
		}
		
		$tcond = isset($this->tables[$table_name]);
		if($tcond){
			$api_instance = new Swagger\Client\Api\DeleteRecordApi();
			$tbl_id = $this->tables[$table_name];
			$user_token = $this->get_fbuser_data("user_token");
			try {
				$oresult = $this->get_record($table_name, $rkey);
				$result = $api_instance->deleteRecord($user_token, $tbl_id, $rkey);
				/* if(!$activity){
					$ddata = $oresult["result_set"];
					$this->update_activities($table_name, $ddata, 'delete', array() , $rkey); // update the activities
				} */
				$msg = array("status" => "success", "message" => "Successfully deleted the record");
				return $msg;
			} catch (Exception $e) {
				$smsg = $e->getMessage();
				log_message('error', "Exception when calling DeleteRecordApi->deleteRecord: ".$smsg);
				$msg = array("status" => "fail", "message" => "Server issue or Rkey is invalid.");
				return $msg;
			}
		} else {
			$msg = array("status" => "fail", "message" => "Invalid Table name");
			return $msg;
		}
	}
	
	public function update_record($table_name = "", $idata = array(), $rkey = '', $activity = false){
		if(!$this->isloggedin()){
			$msg = array("status" => "fail", "message" => "Please login to continue");
			return $msg;
		}
			//Remove leading zero
		foreach($idata as $key=>$val)
		{
				if(($key<>'pond_id') && ($key<>'from_pond_id') && ($key<>'to_pond_id') && ($key<>'species_id'))
				$val = ltrim($val, '0');				
				$idata[$key]=$val;
		}
		
		$tcond = (isset($this->tables[$table_name]) && !empty($idata) );
		if($tcond){
			// Fetch record
			$gmsg = $this->get_record($table_name, $rkey);
			if($gmsg["status"] == "success")
			{
				$odata = $gmsg["result_set"];
				$api_instance = new Swagger\Client\Api\UpsertRecordWithIDApi(new GuzzleHttp\Client());
				$tbl_id = $this->tables[$table_name];
				$user_token = $this->get_fbuser_data("user_token");
				
				// User modified fields only affected.
				$ndata = $this->fb_updaterecord($table_name, $idata, $odata);
				$sdata = json_encode($ndata);
				
				try {
					$result = $api_instance->upsertRecordWithID($user_token, $tbl_id, $rkey, $sdata);
					/* if(!$activity){
						$this->update_activities($table_name, $ndata, 'update', $odata, $rkey); // update the activities
					} */
					
					$msg = array("status" => "success", "message" => "Successfully updated record");
					return $msg;
				} catch (Exception $e) {
					$smsg = $e->getMessage();
					log_message('error', "Exception when calling UpdateRecordWithIDApi->upsertRecordWithID: ".$smsg);
					$msg = array("status" => "fail", "message" => "Invalid Table or Data or Boodskap Server issue");
					return $msg;
				}
			} else {
				return $gmsg;
			}
			
		}else{
			$msg = array("status" => "fail", "message" => "Invalid Table or Data");
			return $msg;
		}
		
	}
	
	public function get_record($table_name = "", $rkey = ""){
		$api_instance = new Swagger\Client\Api\SearchByQueryApi( new GuzzleHttp\Client());
		$atoken = $this->get_fbuser_data("user_token");
		$tbl_id = $this->tables[$table_name];
		$type = "RECORD";
		$query = new \Swagger\Client\Model\SearchQuery();
		
		$qmain = new stdClass();
		$qmain->terms = new stdClass();
		$qmain->terms->_id = array($rkey);
		$queryObj['query']= $qmain;
		$qstr = json_encode($queryObj);
		
		$query['query']= $qstr;
		$query['method']="GET";
		$repositary = "";
		$mapping = "";
		try {
			$oresult = $api_instance->searchByQuery($atoken, $type, $query, $tbl_id, $repositary, $mapping);
			$sresult = $oresult->getResult();
			$aresult = json_decode($sresult, true);
			$total_count = isset($aresult["hits"]["total"]) ? $aresult["hits"]["total"]: "0";
			$result_set = isset($aresult["hits"]["hits"]) ? $aresult["hits"]["hits"] : array();
			if(!empty($result_set)){
				list($cresult) = $result_set;
				$csrc = $cresult["_source"];
				$msg = array("status" => "success", "message" => "Successfully fetched result.",
				"result_set" => $csrc);
				return $msg;
			} else {
				$msg = array("status" => "fail", "message" => "Invalid RKEY.");
				return $msg;
			}
		} catch (Exception $e) {
			$smsg = $e->getMessage();
			log_message('error', "Exception when calling SearchApi->search: ".$smsg);
			$msg = array("status" => "fail", "message" => "Search functionality error");
			return $msg;
		}
	}
	
	public function check_duplicate($table_name = "", $fld_name = "", $search = "", $rkey = ""){
		if(!$this->isloggedin()){
			$msg = array("status" => "fail", "message" => "Please login to continue");
			return $msg;
		}
		
		$search = strtolower(trim($search)); // trim the whitepace		
		$bcond = ( !empty($table_name) && !empty($fld_name) && !empty($search) ); // check the predominant fields
		
		if(!$bcond){
			$msg = array("status" => "fail", "message" => "Table Name or Field name or Search text missing");
			return $msg;
		}
		
		$tcond = (isset($this->tables[$table_name])); // check the table is exist or not.
		if($tcond){
			$api_instance = new Swagger\Client\Api\SearchByQueryApi( new GuzzleHttp\Client());
			$atoken = $this->get_fbuser_data("user_token");
			$tbl_id = $this->tables[$table_name];
			$type = "RECORD";
			$query = new \Swagger\Client\Model\SearchQuery(); // \Swagger\Client\Model\SearchQuery | SearchQuery JSON
			$queryObj = array();
			// Set the query string search
			$qmain = new stdClass();
			if(!empty($search))
			{
				$qmain->query_string = new stdClass();
				$qmain->query_string->query = $search;
			}
			
			$queryObj['query']= $qmain;
			$queryObj['size'] = 1000;
			$queryObj['from'] = 0;
			$qstr = json_encode($queryObj);
			$query['query']= $qstr;
			$query['method']="GET";
			$repositary = "";
			$mapping = "";
			try {
				$oresult = $api_instance->searchByQuery($atoken, $type, $query, $tbl_id, $repositary, $mapping);
				$sresult = $oresult->getResult();
				$aresult = json_decode($sresult, true);
				$total_count = isset($aresult["hits"]["total"]) ? $aresult["hits"]["total"]: "0";
				$result_set = isset($aresult["hits"]["hits"]) ? $aresult["hits"]["hits"] : array();
				if( !empty($result_set) )
				{
					$is_find = false;
					
					foreach($result_set as $row){
						$ckey = $row["_id"];
						$src = $row["_source"];
						$fld_val = isset($src[$fld_name]) ? $src[$fld_name] : "";
						$fld_val = strtolower(trim($fld_val));
						
						if(empty($rkey))
						{
							// check duplicate for create new record
							if($fld_val == $search){
								$is_find = true;
							}
							
						} else {
							// check duplicate for update record
							if($fld_val == $search && $rkey!= $ckey){
								$is_find = true;
							}
						}
					}
					
					if(!$is_find){
						$msg = array("status" => "success", "message" => "No duplicate");
						return $msg;
					}else{
						$msg = array("status" => "fail", "message" => "Duplicate occurred");
						return $msg;
					}
					
				}else{
					$msg = array("status" => "success", "message" => "No duplicate");
					return $msg;
				}
			} catch(Exception $e){
				$smsg = $e->getMessage();
				log_message('error', "Exception when calling SearchApi->search: ".$smsg);
				$msg = array("status" => "fail", "message" => "Search functionality error");
				return $msg;
			}
		} else {
			$msg = array("status" => "fail", "message" => "Invalid Table name");
			return $msg;
		}
	}
	
	
	public function update_others($table_name = '',$orecord = array(),$nrecord = array(), $rkey = ''){
		$pcond = ( !empty($table_name) && !empty($orecord) && !empty($nrecord) && !empty($rkey) );
		if($pcond){
			$affect_flds = $this->CI->config->item('affect_flds', 'fb_boodskap');
			$fld_name = $affect_flds[$table_name];
			$oval = $orecord[$fld_name];
			$nval = $nrecord[$fld_name];
			if(strtolower($nval) == strtolower($oval)){
				log_message('info', "We don't need to update >> ".date("d m Y h:i:s a", now()) );
			}else{
				
				$cdata = array();
				$carr = array(
					"table_name" => $table_name, "oval" => $oval, "nval" => $nval, "fld_name" => $fld_name, "rkey" => $rkey
				);
				
				log_message('info', "Update details >> ".date("d m Y h:i:s a", now()).json_encode($cdata) );
				
				$affect_list = $this->CI->config->item('affect_list', 'fb_boodskap');
				$clist = $affect_list[$table_name];
				
				foreach($clist as $citm){
					$flag = true;
					$tarr = explode(":",$citm);
					$ctbl = reset($tarr);
					$cfld = end($tarr);
					$crst = $this->search_list($ctbl, $oval);
					if($crst["status"]=="success"){
						if(!empty($crst["result_set"])){
							foreach($crst["result_set"] as $crow){
								$crkey = $crow["_id"];
								$cidata[$cfld] = $nval;
								$cidata["updatedtime"] = now();
								$cmsg = $this->update_record($ctbl, $cidata, $crkey, true);
								if($cmsg["status"]=="fail"){
									$flag =  false;
								}
							}
						}
					}
				}
				
				log_message('info', "We should update rest of the tables >> ".date("d m Y h:i:s a", now()) );
			}
			
		}else{
			log_message('error', "Table name OR Old record OR New Record missing");
		}
	}
	
	public function search_list($table_name, $search, $size = 10000){
		// Cache implemented
		$file_name = 'search-'.$table_name.'-'.$search.'-'.$size;
		if ( ! $sdata = $this->CI->cache->get($file_name))
		{
			$api_instance = new Swagger\Client\Api\SearchByQueryApi(new GuzzleHttp\Client());
			$atoken = $this->get_fbuser_data("user_token");
			$tbl_id = $this->tables[$table_name];
			$type = "RECORD";
			$query = new \Swagger\Client\Model\SearchQuery(); // \Swagger\Client\Model\SearchQuery | SearchQuery JSON
			$queryObj = array();

			$qmain = new stdClass();
			if(!empty($search))
			{
				$qmain->query_string = new stdClass();
				$qmain->query_string->query = $search;
			}

			$queryObj['query']= $qmain;
			$queryObj['size'] = $size;
			$queryObj['from'] = 0;
			//$queryObj['sort'] = array("updatedtime" => array("order" => "desc" ));
			$qstr = json_encode($queryObj);
			$query['query']= $qstr;
			$query['method']="POST";
			$repositary = "";
			$mapping = "";
			try {
			
				$oresult = $api_instance->searchByQuery($atoken, $type, $query, $tbl_id);
				//print_r($oresult); exit();
				$sresult = $oresult->getResult();
				$aresult = json_decode($sresult, true);
				$total_count = isset($aresult["hits"]["total"]) ? $aresult["hits"]["total"]: "0";
				$result_set = isset($aresult["hits"]["hits"]) ? $aresult["hits"]["hits"] : array();
				$msg = array("status" => "success", "message" => "Search result got it",
				"result_set" => $result_set);
				
				if(chk_rst_cache()){
					$sdata = json_encode($msg);
					$this->CI->cache->save($file_name, $sdata, 84600);
				}
				return $msg;
			} catch (Exception $e) {
				$smsg = $e->getMessage();
				log_message('error', "Exception when calling SearchApi->search: ".$smsg);
				$msg = array("status" => "fail", "message" => "Search functionality error");
				return $msg;
			}
		} else {
			$msg = json_decode($sdata, true);
			return $msg;
		}
		
	}

	
	private function updated_user(){
		$email = $this->get_fbuser_data("email");
		$first_name = $this->get_fbuser_data("first_name");
		$last_name = $this->get_fbuser_data("last_name");
		$auser = compact("email", "first_name", "last_name");
		return $auser;
	}
	

	public function prepare_cache(){
		$this->last_weeks_reports(); // Get last week reports
		$this->search_list("activities",'',10); // Get activities
		$this->search_list("current_stock",''); // Current Stock
		$this->get_sampling(); // Get sampling list
		fb_combo_arr("ponds"); // Ponds list with ID
		fb_combo_arr("feeds", true); // Feeds list with out ID
		fb_combo_arr("feeds"); // Feeds list with ID
		fb_combo_arr("species"); // Feeds list with ID
	}
	
	public function get_query_agg_result($table_name, $query_str){
	  
	  
	  if(!$this->isloggedin()){
	   $msg = array("status" => "fail", "message" => "Please login to continue");
	   return $msg;
	  }
	  
		
	  $api_instance = new Swagger\Client\Api\SearchByQueryApi(new GuzzleHttp\Client());
	  
	  
	  $atoken = $this->get_fbuser_data("user_token");
	  
	  $type = "RECORD";
	  $query = new \Swagger\Client\Model\SearchQuery(); // \Swagger\Client\Model\SearchQuery | SearchQuery JSON
	  
	  $query['query']= $query_str;
	  $query['method']="POST";
		$tbl_id = $this->tables[$table_name];	
	  
	  $repositary = "";
	  $mapping = "";
	  
	  
	  
	  try {
	   $oresult = $api_instance->searchByQuery($atoken, $type, $query, $tbl_id,  $repositary, $mapping);
	  // fb_pr($oresult);
	   
	   $sresult = $oresult->getResult();
	   $aresult = json_decode($sresult, true);
	   $total_count = isset($aresult["hits"]["total"]) ? $aresult["hits"]["total"]: "0";
	   $result_set = isset($aresult["hits"]["hits"]) ? $aresult["hits"]["hits"] : array();
	   $agg = isset($aresult["aggregations"]["runtime"]["buckets"]) ? $aresult["aggregations"]["runtime"]["buckets"] : array();
	   $msg = array("status" => "success", "message" => "Search result got it", "total_count" => $total_count,"result_set" => $result_set,"aggs"=>$agg );
	   return $msg;
	  } catch (Exception $e) {
	   $smsg = $e->getMessage();
	   log_message('error', "Exception when calling SearchApi->search: ".$smsg);
	   $msg = array("status" => "fail", "message" => "Search functionality error");
	   return $msg;
	  }
	 }
	
	public function get_query_result($table_name, $query_str){
	  
	  
	  if(!$this->isloggedin()){
	   $msg = array("status" => "fail", "message" => "Please login to continue");
	   return $msg;
	  }
	  
		
	  $api_instance = new Swagger\Client\Api\SearchByQueryApi(new GuzzleHttp\Client());
	  
	  
	  $atoken = $this->get_fbuser_data("user_token");
	  
	  $type = "RECORD";
	  $query = new \Swagger\Client\Model\SearchQuery(); // \Swagger\Client\Model\SearchQuery | SearchQuery JSON
	  
	  $query['query']= $query_str;
	  $query['method']="POST";
	$tbl_id = $this->tables[$table_name];	
	  
	  $repositary = "";
	  $mapping = "";
	  
	  
	  
	  try {
	   $oresult = $api_instance->searchByQuery($atoken, $type, $query, $tbl_id,  $repositary, $mapping);
	  // fb_pr($oresult);
	   
	   $sresult = $oresult->getResult();
	   $aresult = json_decode($sresult, true);
	   $total_count = isset($aresult["hits"]["total"]) ? $aresult["hits"]["total"]: "0";
	   $result_set = isset($aresult["hits"]["hits"]) ? $aresult["hits"]["hits"] : array();
	   $msg = array("status" => "success", "message" => "Search result got it", "total_count" => $total_count,"result_set" => $result_set);
	   return $msg;
	  } catch (Exception $e) {
	   $smsg = $e->getMessage();
	   log_message('error', "Exception when calling SearchApi->search: ".$smsg);
	   $msg = array("status" => "fail", "message" => "Search functionality error");
	   return $msg;
	  }
	 }
    
    public function get_genset($table_name = "", $device = ""){
		$api_instance = new Swagger\Client\Api\SearchByQueryApi(new GuzzleHttp\Client());
		$atoken = $this->get_fbuser_data("user_token");
		$tbl_id = $this->tables[$table_name];
		$type = "RECORD";
		$query = new \Swagger\Client\Model\SearchQuery();
		
		$qmain = new stdClass();
		$qmain->terms = new stdClass();
		$qmain->terms->device_id = array($device);
		//$qmain->terms->species_id = array($species);
		$queryObj['query']= $qmain;
		$qstr = json_encode($queryObj);
		
		$query['query']= $qstr;
		$query['method']="GET";
		$repositary = "";
		$mapping = "";
		try {
			$oresult = $api_instance->searchByQuery($atoken, $type, $query, $tbl_id, $repositary, $mapping);
			$sresult = $oresult->getResult();
			$aresult = json_decode($sresult, true);
			$total_count = isset($aresult["hits"]["total"]) ? $aresult["hits"]["total"]: "0";
			$result_set = isset($aresult["hits"]["hits"]) ? $aresult["hits"]["hits"] : array();
			if(!empty($result_set)){
				list($cresult) = $result_set;
				$csrc = $cresult;
				$msg = array("status" => "success", "message" => "Successfully fetched result.",
				"result_set" => $csrc);
				return $msg;
			} else {
				$msg = array("status" => "fail", "message" => "Invalid RKEY.");
				return $msg;
			}
		} catch (Exception $e) {
			$smsg = $e->getMessage();
			log_message('error', "Exception when calling SearchApi->search: ".$smsg);
			$msg = array("status" => "fail", "message" => "Search functionality error");
			return $msg;
		}
	}
	public function get_total_count($table_name){
		
		if(!$this->isloggedin()){
			$msg = array("status" => "fail", "message" => "Please login to continue");
			return $msg;
		}
		
		if(! ( isset( $this->tables[$table_name] ) ) ){
			$msg = array("status" => "fail", "message" => "Invalid Table");
			return $msg;
		}
		
		$query_str = $this->CI->parser->parse('query/query-total', array(), true);
		$result = $this->get_query_result($table_name, $query_str);
		
		if($result["status"]=="success"){
			$total_count = $result["total_count"];
			return $total_count;	
		} else {
			return false;
		}
	}
	public function get_dg_status()
	{
		$table_name = "power-meter";
            $from = 0;
            $size = 1;
            $orderfld = "createdtime";
            $orderdir = "desc";

            $this->CI->load->library('parser');
            $this->CI->parser->set_delimiters("__","__");
            $qpms = array("size" => $size, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir);
            $query_str = $this->CI->parser->parse('query/query-simple', $qpms, true);

            $result = $this->get_query_result($table_name, $query_str);
            if($result["status"]=="success"){
                $src = isset($result["result_set"][0]["_source"]) ? $result["result_set"][0]["_source"]["gen"] : array();
                $msg = array("status" => "success", "message" => "Message Data fetched successfully", 
                     "data" => $src);
                    return $msg;
            }else{
                $msg = array("status" => "fail", "message" => "Message Data did not fetch");
                    return $msg;
            }
	}
	public function get_eb_running(){
		$table_name = "eb_status";
			$data=array();
			$from = 0;
			$size = 0;
			$orderfld = "status_on";
			$orderdir = "desc";

			$this->CI->load->library('parser');
			$this->CI->parser->set_delimiters("__","__");
			$qpms = array("size" => $size, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir);
			$query_str = $this->CI->parser->parse('query/eb_script_fields', $qpms, true);
			$result = $this->get_query_agg_result($table_name, $query_str);
			
			if($result["status"]=="success"){
				
               
			   $msg = array("status" => "success", "message" => "Search result got it","aggs"=>$result['aggs'] );
			  
			   return $msg;
            }else{
                $msg = array("status" => "fail", "message" => "Message Data did not fetch");
                    return $msg;
            }
	}
	
}
