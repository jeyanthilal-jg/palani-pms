<?php
/**
 * Name:    Fourbends Boodskap API
 * Author:  Fourbends Dev Team
 *          contactus@fourbends.com
 * Url:    http://www.fourbends.com/
 *
 *
 * Created:  02.03.2018
 *
 * Description:  This file is used to connect our PHP system to Boodskap API.
 * Original Author name has been kept but that does not mean that the method has not been modified.
 *
 * Requirements: PHP5 or above
 *
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

get_instance()->load->iface('IotRestInterface');

class IOT_Rest implements IotRestInterface
{

	protected $CI;
	
	public function __construct()
	{
		$this->CI =& get_instance();
		require_once( APPPATH . 'third_party/boodskap/vendor/autoload.php');

	}
	
	public function login($email, $password){
		
		$api_instance = new Swagger\Client\Api\LoginApi();
		
		try {
			$result = $api_instance->login($email, $password);
			
			$user_token = $result->getToken();
			$domain_key = $result->getDomainKey();
			$api_key = $result->getApiKey();
			$cUser = $result->getUser();
			$email = $cUser->getEmail();
			$first_name = $cUser->getFirstName();
			$last_name = $cUser->getLastName();
			$country = $cUser->getCountry();
			$state = $cUser->getState();
			$city = $cUser->getCity();
			$address = $cUser->getAddress();
			$zipcode = $cUser->getZipcode();
			$locale = $cUser->getLocale();
			$timezone = $cUser->getTimezone();
			
			$udata = array(
				"user_token" => $user_token,
				"domain_key" => $domain_key,
				"api_key" => $api_key,
			    "email" => $email,
				"first_name" => $first_name,
				"last_name" => $last_name,
				"country" => $country,
				"state" => $state,
				"city" => $city,
				"address" => $address,
				"zipcode" => $zipcode,
				"locale" => $locale,
				"timezone" => $timezone
			);
			$this->CI->session->set_userdata($udata); // Set the session data
			$msg = array("status" => "success", "message" => "Successfully Loggedin");
			return $msg;
		} catch (Exception $e) {
			$smsg = $e->getMessage();
			log_message('error', "Exception when calling LoginApi->login: ".$smsg);
			$msg = array("status" => "fail", "message" => "Invalid Login");
			return $msg;
		}
	}
	
	public function isloggedin(){
		$status = $this->CI->session->has_userdata("user_token");
		return $status;
	}
	
	public function get_fbuser_data($name){
		$udata = $this->CI->session->has_userdata($name);
		if($udata){
			return $this->CI->session->userdata($name);
		}else{
			return "";
		}
	}
	
	public function get_message($msg_id){
		//$msg_id = "7777";
		$apiInstance = new Swagger\Client\Api\ListDeviceMessagesApi(
			// If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
			// This is optional, `GuzzleHttp\Client` will be used as default.
			new GuzzleHttp\Client()
		);
		$atoken = $this->get_fbuser_data("user_token"); // string | Auth token of the logged in user
		$page_size = 100; // int | Maximum number of messages to be listed
		$direction = ""; // string | If direction is specified, **muid** is required
		$muid = $msg_id; // string | Last or First message uuid of the previous list operation, **required** if **direction** is specified

		try {
			$result = $apiInstance->listDeviceMessages($atoken, $page_size, $direction, $muid);
			
			$mobj = isset($result["0"]) ? $result["0"] : "";
			if(!empty($mobj)){
				$mdata = $mobj->getData();
				$nuuid = $mobj->getNodeUuid();
				$amdata = json_decode($mdata, true);
				$msg = array("status" => "success", "message" => "Message Data fetched successfully", 
				 "data" => array_merge($amdata, array("nuuid" => $nuuid)));
				return $msg;
				
			}else{
				$msg = array("status" => "fail", "message" => "Message Data did not fetch", 
				 "data" => array());
				return $msg;
			}
			
		} catch (Exception $e) {
			//echo 'Exception when calling ListDeviceMessagesApi->listDeviceMessages: ', $e->getMessage(), PHP_EOL;
			$msg = array("status" => "fail", "message" => "API Issue: ".$e->getMessage() , 
				 "data" => array());
			return $msg;
		}
	}
	
   public function get_query_result($tbl_id, $query_str){
	  
	  
	  if(!$this->isloggedin()){
	   $msg = array("status" => "fail", "message" => "Please login to continue");
	   return $msg;
	  }
	  
		
	  $api_instance = new Swagger\Client\Api\SearchByQueryApi(new GuzzleHttp\Client());
	  
	  
	  $atoken = $this->get_fbuser_data("user_token");
	  
	  $type = "RECORD";
	  $query = new \Swagger\Client\Model\SearchQuery(); // \Swagger\Client\Model\SearchQuery | SearchQuery JSON
	  
	  $query['query']= $query_str;
	  $query['method']="POST";
		
	  
	  $repositary = "";
	  $mapping = "";
	  
	  
	  
	  try {
	   $oresult = $api_instance->SearchByQuery($atoken, $type, $query, $tbl_id);
	   //fb_pr($oresult);
	   
	   $sresult = $oresult->getResult();
	   $aresult = json_decode($sresult, true);
	   $total_count = isset($aresult["hits"]["total"]) ? $aresult["hits"]["total"]: "0";
	   $result_set = isset($aresult["hits"]["hits"]) ? $aresult["hits"]["hits"] : array();
	   $msg = array("status" => "success", "message" => "Search result got it", "total_count" => $total_count,
	   "result_set" => $result_set);
	   return $msg;
	  } catch (Exception $e) {
	   $smsg = $e->getMessage();
	   log_message('error', "Exception when calling SearchApi->search: ".$smsg);
	   $msg = array("status" => "fail", "message" => "Search functionality error");
	   return $msg;
	  }
	 }
	
	public function get_power_detail(){
		$tbl_id = "350412";
		$from = 0;
		$size = 1;
		$orderfld = "createdtime";
		$orderdir = "desc";
		
		$this->CI->load->library('parser');
		$this->CI->parser->set_delimiters("__","__");
		$qpms = array("size" => $size, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir);
		$query_str = $this->CI->parser->parse('query/query-simple', $qpms, true);
		
		$result = $this->get_query_result($tbl_id, $query_str);
		if($result["status"]=="success"){
			$src = isset($result["result_set"][0]["_source"]) ? $result["result_set"][0]["_source"] : array();
			$msg = array("status" => "success", "message" => "Message Data fetched successfully", 
				 "data" => $src);
				return $msg;
		}else{
			$msg = array("status" => "fail", "message" => "Message Data did not fetch");
				return $msg;
		}
		
	}
    
	public function get_power_list(){
		$tbl_id = "350412";
		$from = 0;
		$size = 20;
		$orderfld = "createdtime";
		$orderdir = "desc";
		
		$this->CI->load->library('parser');
		$this->CI->parser->set_delimiters("__","__");
		$qpms = array("size" => $size, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir);
		$query_str = $this->CI->parser->parse('query/query-simple', $qpms, true);
		
		$result = $this->get_query_result($tbl_id, $query_str);

		if($result["status"]=="success"){
			
			$msg = array("status" => "success", "message" => "Message Data fetched successfully", 
				 "data" => $result["result_set"]);
				return $msg;
		}else{
			$msg = array("status" => "fail", "message" => "Message Data did not fetch");
				return $msg;
		}
		
	}


	public function get_records(){
		$tbl_id = "350412";
		$from = 0;
		$size = 1000;
		$orderfld = "createdtime";
		$orderdir = "desc";
		
		$this->CI->load->library('parser');
		$this->CI->parser->set_delimiters("__","__");
		$qpms = array("size" => $size, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir);
		$query_str = $this->CI->parser->parse('query/query-simple', $qpms, true);
		
		$result = $this->get_query_result($tbl_id, $query_str);

		if($result["status"]=="success"){
			
			$msg = array("status" => "success", "message" => "Message Data fetched successfully", 
				 "data" => $result["result_set"]);
				return $msg;
		}else{
			$msg = array("status" => "fail", "message" => "Message Data did not fetch");
				return $msg;
		}
		
	}
    
	public function calc_kwh_eb(){
		$rec_id = "350412";
		$result_set = $this->get_power_detail($rec_id);
		//fb_pr($result);exit;
		
		$time = $this->get_KWH();
		
		if($time["status"]=="success" && $result_set["status"]=="success"){
				$aggs["aggs"] = $time["aggs"];
				
				foreach($aggs as $row){
					$min_time=$row['min_time']['value'];
					$max_time=$row['max_time']['value'];
				}
				$diff = $max_time - $min_time;
				$runtime = gmdate('H',$diff);
				$tot_power = $result_set["data"]["tp"];
				$kwh = floatval($tot_power* $runtime);
				return $kwh;
				
		}
		else{
			$kwh = 1;
			return $kwh;
		}
	}
	public function calc_kwh_dg(){
		$tbl_id = 350409;
			$data=array();
			$from = 0;
			$size = 0;
			$orderfld = "last_on_time";
			$orderdir = "desc";

			$this->CI->load->library('parser');
			$this->CI->parser->set_delimiters("__","__");
			$qpms = array("size" => $size, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir);
			$query_str = $this->CI->parser->parse('query/script_fields', $qpms, true);
			$result = $this->get_query_agg_result($tbl_id, $query_str);
			
			$rec_id = "350412";
			$result_set = $this->get_power_detail($rec_id);
		if($result["status"]=="success" && $result_set["status"]=="success"){
			$aggs["aggs"] = $result["aggs"];
			foreach($aggs as $row){
				
				foreach($row as $result){
					$runtime=$result["key"];
					
				}
				$tot_power = $result_set["data"]["tp"];
				$no_of_hrs = gmdate('H',$runtime);
				$kwh = floatval($tot_power * $no_of_hrs);
				
			}
			return $kwh;
		}
		else{
			$kwh = 1;
			return $kwh;
		}
	}
	
	public function get_totVolt(){
		$tbl_id = "350412";
		$from = 0;
		$size = 1;
		$orderfld = "created";
		$orderdir = "desc";
		
		$this->CI->load->library('parser');
		$this->CI->parser->set_delimiters("__","__");
		$qpms = array("size" => $size, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir);
		$query_str = $this->CI->parser->parse('query/query-simple', $qpms, true);
		
		$result = $this->get_query_result($tbl_id, $query_str);
		if($result["status"]=="success"){
			$src = isset($result["result_set"][0]["_source"]) ? $result["result_set"][0]["_source"]["tpv"] : array();
			$msg = array("status" => "success", "message" => "Message Data fetched successfully", 
				 "data" => $src);
				return $msg;
		}else{
			$msg = array("status" => "fail", "message" => "Message Data did not fetch");
				return $msg;
		}
		
	}  
    
    public function get_singleVolt(){
		$tbl_id = "350412";
		$from = 0;
		$size = 1;
		$orderfld = "created";
		$orderdir = "desc";
		
		$this->CI->load->library('parser');
		$this->CI->parser->set_delimiters("__","__");
		$qpms = array("size" => $size, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir);
		$query_str = $this->CI->parser->parse('query/query-simple', $qpms, true);
		
		$result = $this->get_query_result($tbl_id, $query_str);
		if($result["status"]=="success"){
			$src = isset($result["result_set"][0]["_source"]) ? $result["result_set"][0]["_source"]["tv"] : array();
			$msg = array("status" => "success", "message" => "Message Data fetched successfully", 
				 "data" => $src);
				return $msg;
		}else{
			$msg = array("status" => "fail", "message" => "Message Data did not fetch");
				return $msg;
		}
		
	}  
    
    public function get_totAmp(){
		$tbl_id = "350412";
		$from = 0;
		$size = 1;
		$orderfld = "created";
		$orderdir = "desc";
		
		$this->CI->load->library('parser');
		$this->CI->parser->set_delimiters("__","__");
		$qpms = array("size" => $size, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir);
		$query_str = $this->CI->parser->parse('query/query-simple', $qpms, true);
		
		$result = $this->get_query_result($tbl_id, $query_str);
		if($result["status"]=="success"){
			$src = isset($result["result_set"][0]["_source"]) ? $result["result_set"][0]["_source"]["tc"] : array();
			$msg = array("status" => "success", "message" => "Message Data fetched successfully", 
				 "data" => $src);
				return $msg;
		}else{
			$msg = array("status" => "fail", "message" => "Message Data did not fetch");
				return $msg;
		}
		
	} 
    public function getPowerfactor(){
            $tbl_id = "350412";
            $from = 0;
            $size = 1;
            $orderfld = "created";
            $orderdir = "desc";

            $this->CI->load->library('parser');
            $this->CI->parser->set_delimiters("__","__");
            $qpms = array("size" => $size, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir);
            $query_str = $this->CI->parser->parse('query/query-simple', $qpms, true);

            $result = $this->get_query_result($tbl_id, $query_str);
            if($result["status"]=="success"){
                $src = isset($result["result_set"][0]["_source"]) ? $result["result_set"][0]["_source"]["pf"] : array();
                $msg = array("status" => "success", "message" => "Message Data fetched successfully", 
                     "data" => $src);
                    return $msg;
            }else{
                $msg = array("status" => "fail", "message" => "Message Data did not fetch");
                    return $msg;
            }

        }   
    
        public function gettotWatts(){
            $tbl_id = "350412";
            $from = 0;
            $size = 1;
            $orderfld = "created";
            $orderdir = "desc";

            $this->CI->load->library('parser');
            $this->CI->parser->set_delimiters("__","__");
            $qpms = array("size" => $size, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir);
            $query_str = $this->CI->parser->parse('query/query-simple', $qpms, true);

            $result = $this->get_query_result($tbl_id, $query_str);
            if($result["status"]=="success"){
                $src = $result["result_set"][0]["_source"]; 
                $msg = array("status" => "success", "message" => "Message Data fetched successfully", 
                     "data" => $src);
                    return $msg;
            }else{
                $msg = array("status" => "fail", "message" => "Message Data did not fetch");
                    return $msg;
            }

        }     

    
		public function getDG(){
            $tbl_id = "350412";
            $from = 0;
            $size = 1;
            $orderfld = "created";
            $orderdir = "desc";

            $this->CI->load->library('parser');
            $this->CI->parser->set_delimiters("__","__");
            $qpms = array("size" => $size, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir);
            $query_str = $this->CI->parser->parse('query/query-simple', $qpms, true);

            $result = $this->get_query_result($tbl_id, $query_str);
            if($result["status"]=="success"){
                $src = $result["result_set"][0]["_source"];
                $msg = array("status" => "success", "message" => "Message Data fetched successfully", 
                     "data" => $src);
                    return $msg;
            }else{
                $msg = array("status" => "fail", "message" => "Message Data did not fetch");
                    return $msg;
            }

        } 	
	public function getdg_record(){
		$tbl_id = "350412";
		$from = 0;
		$size = 1000;
		$orderfld = "createdtime";
		$orderdir = "desc";
		
		$this->CI->load->library('parser');
		$this->CI->parser->set_delimiters("__","__");
		$qpms = array("val" => "1", "size" => $size, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir);
		$query_str = $this->CI->parser->parse('query/sample', $qpms, true);
		
		$result = $this->get_query_agg_result($tbl_id, $query_str);

		if($result["status"]=="success"){
			
			$msg = array("status" => "success", "message" => "Message Data fetched successfully", 
				 "data" => $result["result_set"],"aggs"=>$result["aggs"]);
				return $msg;
		}else{
			$msg = array("status" => "fail", "message" => "Message Data did not fetch");
				return $msg;
		}
		
	}
	
	public function geteb_record(){
		$tbl_id = "350412";
		$from = 0;
		$size = 1000;
		$orderfld = "createdtime";
		$orderdir = "desc";
		
		$this->CI->load->library('parser');
		$this->CI->parser->set_delimiters("__","__");
		$qpms = array("val" => "0", "size" => $size, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir);
		$query_str = $this->CI->parser->parse('query/sample', $qpms, true);
		
		$result = $this->get_query_agg_result($tbl_id, $query_str);

		if($result["status"]=="success"){
			
			$msg = array("status" => "success", "message" => "Message Data fetched successfully", 
				 "data" => $result["result_set"],"aggs"=>$result["aggs"]);
				 
				return $msg;
		}else{
			$msg = array("status" => "fail", "message" => "Message Data did not fetch");
				return $msg;
		}
		
	}

    public function usePowerfactor(){
        $result = $this->getPowerfactor();
        if($result["status"] == "success" && !(empty($result['data']))){
			return $result['data'];
		}else{
			return 0;
		}
    }

     public function getAmpValue(){
        $result = $this->get_totAmp();
        if($result["status"] == "success" && !(empty($result['data']))){
			return $result['data'];
		}else{
			return 0;
		}
    }

    public function getVoltValue(){
        $result = $this->get_totVolt();

        if($result["status"] == "success" && !(empty($result['data']))){
			return $result['data'];
		}else{
			return 0;
		}
    }          	

    public function formulaKVA(){
        $PF = $this->usePowerfactor();
        $A = $this->getAmpValue();
        $V = $this->getVoltValue();
        $kVA = round(pow(3,1/3) * $A * $V / 1000,2); 
        return $kVA;
    }

    public function gettotPower()
    {
        $result = $this->gettotWatts();
        if($result["status"] == "success" && !(empty($result['data']))){
			$tp = $result['data']['tp'];
        	return $tp;
		}else{
			return 0;
		}
    }

    public function getsinglewattsValue()
    {
        
        $result = $this->gettotWatts();
        if($result["status"] == "success" && !(empty($result['data']))){
        	$rp=  $result['data']['rp'];
			$bp= $result['data']['bp'];
			$yp = $result['data']['yp'];
			$tp = $result['data']['tp'];
        	$arrPower = array();
        	array_push($arrPower, $rp,$bp,$yp,$tp);
        	return $arrPower;
			
		}else{
			return 0;
		}
    }        	

	public function widget_3Voltage($row_id){

		 $result = $this->get_totVolt();

        if($result["status"] == "success" && !(empty($result['data']))){
			echo '<div class="col-md-3" id="recordsArray_'.$row_id.'">
          <div class="card bg-primary p-20 drag-widget">
            <div class="media widget-ten">
              <div class="media-left meida media-middle"> <span><i class="fa fa-bolt fa-3x"></i></span> </div>
              <div class="media-body media-text-right">
                <p class="m-b-0 m-l-20 media-text-left">Three Phase Voltage</p>
                <h2 class="color-white odometer" id="avgtot3">
                '.$result['data'].'
                </h2>
                <p class="m-b-0">V</p>
              </div>
            </div>
          </div>
        </div>';

		}else{
			echo '<div class="col-md-3" id="recordsArray_'.$row_id.'">
          <div class="card bg-primary p-20 drag-widget">
            <div class="media widget-ten">
              <div class="media-left meida media-middle"> <span><i class="fa fa-bolt fa-3x"></i></span> </div>
              <div class="media-body media-text-right">
                <p class="m-b-0 m-l-20 media-text-left">Three Phase Voltage</p>
                <h2 class="color-white odometer" id="avgtot3">
                 0
                </h2>
                <p class="m-b-0">V</p>
              </div>
            </div>
          </div>
        </div>';
		}
	}

	public function widget_1Voltage($row_id){

		 $result = $this->get_singleVolt();

        if($result["status"] == "success" && !(empty($result['data']))){
			echo '<div class="col-md-3" id="recordsArray_'.$row_id.'">
          <div class="card bg-primary p-20 drag-widget">
            <div class="media widget-ten">
              <div class="media-left meida media-middle"> <span><i class="fa fa-bolt fa-3x"></i></span> </div>
              <div class="media-body media-text-right">
                <p class="m-b-0 m-l-20 media-text-left">Single Phase Voltage</p>
                <h2 class="color-white odometer" id="total-volt-r">
                '.$result['data'].'
                </h2>
                <p class="m-b-0">V</p>
              </div>
            </div>
          </div>
        </div>';

		}else{
			echo '<div class="col-md-3" id="recordsArray_'.$row_id.'">
          <div class="card bg-primary p-20 drag-widget">
            <div class="media widget-ten">
              <div class="media-left meida media-middle"> <span><i class="fa fa-bolt fa-3x"></i></span> </div>
              <div class="media-body media-text-right">
                <p class="m-b-0 m-l-20 media-text-left">Single Phase Voltage</p>
                <h2 class="color-white odometer" id="total-volt-r">
                0
                </h2>
                <p class="m-b-0">V</p>
              </div>
            </div>
          </div>
        </div>';
		}
	}

	public function widget_3KVA($row_id){
		$kva = $this->formulaKVA(); 
		echo '<div class="col-md-3" id="recordsArray_'.$row_id.'">
          <div class="card bg-danger  p-20 drag-widget">
            <div class="media widget-ten">
              <div class="media-left meida media-middle"> <span><i class="fa fa-bolt fa-3x"></i></span> </div>
              <div class="media-body media-text-right">
                  <p class="m-b-0 m-l-20 media-text-left">KVA</p>
                <h2 class="color-white odometer" id="three-kva">
                    '.$kva.'</h2>
                <p class="m-b-0">KVA</p>
              </div>
            </div>
          </div>
        </div>'; 
	}

	public function widget_1KVA($row_id){
		$kva = $this->formulaKVA(); 
		echo '<div class="col-md-3" id="recordsArray_'.$row_id.'">
          <div class="card bg-danger  p-20 drag-widget">
            <div class="media widget-ten">
              <div class="media-left meida media-middle"> <span><i class="fa fa-bolt fa-3x"></i></span> </div>
              <div class="media-body media-text-right">
                  <p class="m-b-0 m-l-20 media-text-left">Single Phase KVA</p>
                <h2 class="color-white odometer" id="single-kva">
                    '.$kva.'</h2>
                <p class="m-b-0">KVA</p>
              </div>
            </div>
          </div>
        </div>'; 
	}

	public function widget_TotalPower($row_id){

		$totPower = $this->gettotPower();
        echo '<div class="col-md-3" id="recordsArray_'.$row_id.'">
          <div class="card bg-success p-20 drag-widget">
            <div class="media widget-ten">
              <div class="media-left meida media-middle"> <span><i class="fa fa-bolt fa-3x"></i></span> </div>
              <div class="media-body media-text-right">
                  <p class="m-b-0 m-l-20 media-text-left">Three Phase Power</p>
                <h2 class="color-white ">
                   '.$totPower.'  (KW)</h2>
              </div>
            </div>
          </div>
        </div>'; 		
	}

	public function widget_KWH($row_id){

		$kwh = $this->calc_kwh_eb();
        echo '<div class="col-md-3" id="recordsArray_'.$row_id.'">
          <div class="card bg-success p-20 drag-widget">
            <div class="media widget-ten">
              <div class="media-left meida media-middle"> <span><i class="fa fa-bolt fa-3x"></i></span> </div>
              <div class="media-body media-text-right">
                  <p class="m-b-0 m-l-20 media-text-left">EB KWH</p>
                <h2 class="color-white ">
                   '.$kwh.'  (KWH)</h2>
              </div>
            </div>
          </div>
        </div>'; 		
	}
	public function widget_dg_KWH($row_id){

		$kwh = $this->calc_kwh_dg();
        echo '<div class="col-md-3" id="recordsArray_'.$row_id.'">
          <div class="card bg-success p-20 drag-widget">
            <div class="media widget-ten">
              <div class="media-left meida media-middle"> <span><i class="fa fa-bolt fa-3x"></i></span> </div>
              <div class="media-body media-text-right">
                  <p class="m-b-0 m-l-20 media-text-left">DG KWH</p>
                <h2 class="color-white ">
                   '.$kwh.'  (KWH)</h2>
              </div>
            </div>
          </div>
        </div>'; 		
	}
	public function widget_SinglePower($row_id){
		$power = $this->getsinglewattsValue();
   	
     	echo '<div class="col-md-4 ui-sortable-handle" id="recordsArray_'.$row_id.'">
          <div class="card bg-success p-20 drag-widget">
            <div class="media widget-ten">
              <div class="media-left meida media-middle"> <span><i class="fa fa-bolt fa-3x"></i></span> </div>           
              <div class="media-body media-text-right">
                  <p class="m-b-4 m-l-10 media-text-left">POWER</p>                
                <div class="row">
                  <div class="col-sm-4">R : <span class="widget-span odometer" id="rp_update">'.round($power[0],2).' </span> W</div>
                  <div class="col-sm-4">B : <span class="widget-span odometer" id="bp_update">'.round($power[1],2).' </span> W</div>
                  <div class="col-sm-4">Y : <span class="widget-span odometer" id="yp_update">'.round($power[2],2).' </span> W</div>
                </div>
                <div class="m-t-6 m-l-20">LL : <span class="widget-span odometer" id="tot_power">'.round($power[3],2).'</span> KW</div>
              </div>
            </div>
          </div>
        </div>';		
	}

	public function widget_Frequency($row_id){
		$result = $this->getFrequency();
		if($result["status"] == "success" && !(empty($result['data']))){
		echo '<div class="col-md-3" id="recordsArray_'.$row_id.'">
          <div class="card bg-warning p-20 drag-widget">
            <div class="media widget-ten">
              <div class="media-left meida media-middle"> <span><i class="fa fa-bolt fa-3x"></i></span> </div>
              <div class="media-body media-text-right">
                <p class="m-b-0 m-l-20 media-text-left">Frequency</p>
                <h2 class="color-white odometer" id="freq">'.$result['data'].'</h2>
                <p class="m-b-0">Hz</p>
              </div>
            </div>
          </div>
        </div>';
    	}else{
		echo '<div class="col-md-3" id="recordsArray_'.$row_id.'">
          <div class="card bg-warning p-20 drag-widget">
            <div class="media widget-ten">
              <div class="media-left meida media-middle"> <span><i class="fa fa-bolt fa-3x"></i></span> </div>
              <div class="media-body media-text-right">
                <p class="m-b-0 m-l-20 media-text-left">Frequency</p>
                <h2 class="color-white odometer" id="freq">0</h2>
                <p class="m-b-0">Hz</p>
              </div>
            </div>
          </div>
        </div>';    		
    	}
	}

	public function widget_Current($row_id){
		$A = $this->getAmpValue();
        echo '<div class="col-md-3" id="recordsArray_'.$row_id.'">
          <div class="card bg-pink p-20 drag-widget">
            <div class="media widget-ten">
              <div class="media-left meida media-middle"> <span><i class="fa fa-bolt fa-3x"></i></span> </div>
              <div class="media-body media-text-right">
                <p class="m-b-0 m-l-20 media-text-left">Current</p>
                <h2 class="color-white odometer" id="total-current">'.$A.'</h2>
                <p class="m-b-0">A</p>
              </div>
            </div>
          </div>
        </div>'; 		
	}

	public function widget_PowerFactor($row_id){
	 $PF= $this->usePowerfactor();
	echo '<div class="col-md-3" id="recordsArray_'.$row_id.'">
          <div class="card bg-dark  p-20 drag-widget">
            <div class="media widget-ten">
              <div class="media-left meida media-middle"> <span><i class="fa fa-bolt fa-3x"></i></span> </div>
              <div class="media-body media-text-right">
                  <p class="m-b-0 m-l-20 media-text-left">Power Factor</p>
                <h2 class="color-white odometer" id="powerfactor">'.$PF.'</h2>
                <p class="m-b-0">PF</p>
              </div>
            </div>
          </div>
        </div>';
	}

	public function widget_gaugeAmp($row_id){
		echo '<div class="col-md-3" id="recordsArray_'.$row_id.'">
          <div class="card drag-widget">
            <div class="card-title">
            </div>
            <div class="card-content">
              <div id="gauge-amp" style="height: 250px;"></div>
            </div>
          </div>
        </div>';	
	}

	public function widget_gaugeVoltage($row_id){
		echo '<div class="col-md-3" id="recordsArray_'.$row_id.'">
          <div class="card drag-widget">
            <div class="card-title">
            </div>
            <div class="card-content">
              <div id="gauge-voltage" style="height: 250px;"></div>
            </div>
          </div>
        </div>';
	}

	public function widget_gaugesingleVoltage($row_id){
			echo '<div class="col-md-3" id="recordsArray_'.$row_id.'">
	          <div class="card drag-widget">
	            <div class="card-title">
	            </div>
	            <div class="card-content">
	              <div id="gauge-voltage-single" style="height: 250px;"></div>
	            </div>
	          </div>
	        </div>';
	}					

	public function widget_consumption($row_id){
		echo '<div class="col-md-12 livegraph_consumption" id="recordsArray_'.$row_id.'">
          <div class="card drag-widget">
            <div class="card-title text-center">
              Consumption 
            </div>
            <div class="card-content">
            <button type="button" class="btn btn-primary btn-sm m-r-20 pull-right" data-toggle="modal" data-target="#livegraph-modal">Live Graph</button>
            <div class="graph-preloader">
        		<svg class="circular" viewBox="25 25 50 50">
					<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"></circle> 
				</svg>
    		</div>
              <div id="linechart" style="height: 400px"></div>
            </div>
          </div>
        </div>';
	}

	public function widget_FrequencyGauge($row_id){
		echo '<div class="col-md-3" id="recordsArray_'.$row_id.'">
          <div class="card drag-widget">
            <div class="card-title">
            </div>
            <div class="card-content">
              <div id="gauge-frequency" style="height: 250px;"></div>
            </div>
          </div>
        </div>';
	}	

	public function getmeter_list($rec_id){
		$tbl_id = $rec_id;
		$from = 0;
		$size = 1000;
		$orderfld = "createdtime";
		$orderdir = "desc";
		
		$this->CI->load->library('parser');
		$this->CI->parser->set_delimiters("__","__");
		$qpms = array("size" => $size, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir);
		$query_str = $this->CI->parser->parse('query/query-simple', $qpms, true);
		
		$result = $this->get_query_result($tbl_id, $query_str);

		if($result["status"]=="success"){
			$src = isset($result["result_set"]) ? $result["result_set"] : array();
			$msg = array("status" => "success", "message" => "Message Data fetched successfully", 
				 "data" => $src);
				return $msg;
		}else{
			$msg = array("status" => "fail", "message" => "Message Data did not fetch");
				return $msg;
		}	
	}

	public function activemeter_list($rec_id){
		$tbl_id = $rec_id;
		$from = 0;
		$size = 1000;
		$orderfld = "createdtime";
		$orderdir = "desc";
		
		$this->CI->load->library('parser');
		$this->CI->parser->set_delimiters("__","__");
		$qpms = array("size" => $size, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir);
		$query_str = $this->CI->parser->parse('query/query-status-filter', $qpms, true);
		
		$result = $this->get_query_result($tbl_id, $query_str);

		if($result["status"]=="success"){
			$src = isset($result["result_set"]) ? $result["result_set"] : array();
			$msg = array("status" => "success", "message" => "Message Data fetched successfully", 
				 "data" => $src);
				return $msg;
		}else{
			$msg = array("status" => "fail", "message" => "Message Data did not fetch");
				return $msg;
		}	
	}

	public function getFrequency(){
        $tbl_id = "350412";
        $from = 0;
        $size = 1;
        $orderfld = "created";
        $orderdir = "desc";

        $this->CI->load->library('parser');
        $this->CI->parser->set_delimiters("__","__");
        $qpms = array("size" => $size, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir);
        $query_str = $this->CI->parser->parse('query/query-simple', $qpms, true);

        $result = $this->get_query_result($tbl_id, $query_str);
        if($result["status"]=="success"){
            $src = isset($result["result_set"][0]["_source"]) ? $result["result_set"][0]["_source"]["fre"] : array();
            $msg = array("status" => "success", "message" => "Message Data fetched successfully", 
                 "data" => $src);
                return $msg;
        }else{
            $msg = array("status" => "fail", "message" => "Message Data did not fetch");
                return $msg;
        }

      }

    public function getRBY_Voltage()
    {
        
        $result = $this->gettotWatts();
        if($result["status"] == "success" && !(empty($result['data']))){
        	$rv=  $result['data']['rv'];
			$bv= $result['data']['bv'];
			$yv = $result['data']['yv'];
			$tpv = $result['data']['tpv'];
        	$arrPower = array();
        	array_push($arrPower, $rv,$bv,$yv,$tpv);
        	return $arrPower;
			
		}else{
			return 0;
		}
    }     

    public function getRBY_Amp()
    {
        
        $result = $this->gettotWatts();
        if($result["status"] == "success" && !(empty($result['data']))){
        	$rc=  $result['data']['rc'];
			$bc= $result['data']['bc'];
			$yc = $result['data']['yc'];
			$tc = $result['data']['tc'];
        	$arrPower = array();
        	array_push($arrPower, $rc,$bc,$yc,$tc);
        	return $arrPower;
			
		}else{
			return 0;
		}
    }  
    public function widget_ViewVoltage($row_id){
		$vol = $this->getRBY_Voltage();     	
     	echo '<div class="col-md-4 ui-sortable-handle" id="recordsArray_'.$row_id.'">
          <div class="card bg-success p-20 drag-widget">
            <div class="media widget-ten">
              <div class="media-left meida media-middle"> <span><i class="fa fa-bolt fa-3x"></i></span> </div>           
              <div class="media-body media-text-right">
                  <p class="m-b-4 m-l-10 media-text-left">VOLTAGE</p>                
                <div class="row">
                  <div class="col-sm-4">R : <span class="widget-span odometer" id="rv_update">'.$vol[0].' </span> V</div>
                  <div class="col-sm-4">B : <span class="widget-span odometer" id="bv_update">'.$vol[1].' </span> V</div>
                  <div class="col-sm-4">Y : <span class="widget-span odometer" id="yv_update">'.$vol[2].' </span> V</div>
                </div>
                <div class="m-t-6 m-l-20">LL : <span class="widget-span odometer" id="avgtot3">'.$vol[3].'</span> V</div>
              </div>
            </div>
          </div>
        </div>';
     }
    public function widget_ViewAmp($row_id){
		$vol = $this->getRBY_Amp();     	
     	echo '<div class="col-md-4 ui-sortable-handle" id="recordsArray_'.$row_id.'">
          <div class="card bg-success p-20 drag-widget">
            <div class="media widget-ten">
              <div class="media-left meida media-middle"> <span><i class="fa fa-bolt fa-3x"></i></span> </div>           
              <div class="media-body media-text-right">
                  <p class="m-b-4 m-l-10 media-text-left">Amp</p>                
                <div class="row">
                  <div class="col-sm-4">R : <span class="widget-span odometer" id="rc_update">'.round($vol[0],2).' </span> A</div>
                  <div class="col-sm-4">B : <span class="widget-span odometer" id="bc_update">'.round($vol[1],2).' </span> A</div>
                  <div class="col-sm-4">Y : <span class="widget-span odometer" id="yc_update">'.round($vol[2],2).' </span> A</div>
                </div>
                <div class="m-t-6 m-l-20">LL : <span class="widget-span odometer" id="total-current">'.round($vol[3],2).'</span> A</div>
              </div>
            </div>
          </div>
        </div>';    	
    }

	public function widget_gaugeKVA($row_id){
		echo '<div class="col-md-3" id="recordsArray_'.$row_id.'">
          <div class="card drag-widget">
            <div class="card-title">
            </div>
            <div class="card-content">
              <div id="gauge-kva" style="height: 250px;"></div>
            </div>
          </div>
        </div>';
	}	    
    
	public function get_recordsByValue($val){

		$tbl_id = "350412";
		$from = 0;
		$size = 1000;
		$orderfld = "createdtime";
		$orderdir = "desc";
		if($val=="1d"){
			$range = "now-1d";
		}elseif ($val=="7d") {
			$range = "now-7d";
		}elseif($val=="1m"){
			$range = "now-1M";
		}elseif ($val=="1y") {
			$range = "now-1y";
		}
		$this->CI->load->library('parser');
		$this->CI->parser->set_delimiters("__","__");
		$qpms = array("size" => $size, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir,"lowv" => $range , "topv" => "now","range_fld" => "createdtime");

		$query_str = $this->CI->parser->parse('query/query-range', $qpms, true);
		
		$result = $this->get_query_result($tbl_id, $query_str);

		if($result["status"]=="success"){
			
			$msg = array("status" => "success", "message" => "Message Data fetched successfully", 
				 "data" => $result["result_set"]);
				return $msg;
		}else{
			$msg = array("status" => "fail", "message" => "Message Data did not fetch");
				return $msg;
		}
		
	}
	public function get_KWH(){
		$tbl_id = "350412";	
			$data=array();
			$from = 0;
			$size = 0;
			$orderfld = "createdtime";
			$orderdir = "desc";

			$this->CI->load->library('parser');
			$this->CI->parser->set_delimiters("__","__");
			$qpms = array("size" => $size, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir);
			$query_str = $this->CI->parser->parse('query/query-per-day', $qpms, true);
			$result = $this->get_query_aggs_result($tbl_id, $query_str);
			
			if($result["status"]=="success"){
				
               
			   $msg = array("status" => "success", "message" => "Search result got it","aggs"=>$result['result_set']['aggregations'] );
			  
			   return $msg;
            }else{
                $msg = array("status" => "fail", "message" => "Message Data did not fetch");
                    return $msg;
            }
	}
	
	public function get_query_aggs_result($tbl_id, $query_str){
	  
	  
	  if(!$this->isloggedin()){
	   $msg = array("status" => "fail", "message" => "Please login to continue");
	   return $msg;
	  }
	  
		
	  $api_instance = new Swagger\Client\Api\SearchByQueryApi(new GuzzleHttp\Client());
	  
	  
	  $atoken = $this->get_fbuser_data("user_token");
	  
	  $type = "RECORD";
	  $query = new \Swagger\Client\Model\SearchQuery(); // \Swagger\Client\Model\SearchQuery | SearchQuery JSON
	  
	  $query['query']= $query_str;
	  $query['method']="POST";
		
	  
	  $repositary = "";
	  $mapping = "";
	  
	  
	  
	  try {
	   $oresult = $api_instance->searchByQuery($atoken, $type, $query, $tbl_id,  $repositary, $mapping);
	  // fb_pr($oresult);
	   
	   $sresult = $oresult->getResult();
	   $aresult = json_decode($sresult, true);
	   $total_count = isset($aresult["hits"]["total"]) ? $aresult["hits"]["total"]: "0";
	   $result_set = isset($aresult["hits"]["hits"]) ? $aresult["hits"]["hits"] : array();
	   $msg = array("status" => "success", "message" => "Search result got it", "total_count" => $total_count,"result_set" => $aresult);
	   return $msg;
	  } catch (Exception $e) {
	   $smsg = $e->getMessage();
	   log_message('error', "Exception when calling SearchApi->search: ".$smsg);
	   $msg = array("status" => "fail", "message" => "Search functionality error");
	   return $msg;
	  }
	}
	
	public function get_query_agg_result($tbl_id, $query_str){
	  
	  
	  if(!$this->isloggedin()){
	   $msg = array("status" => "fail", "message" => "Please login to continue");
	   return $msg;
	  }
	  
		
	  $api_instance = new Swagger\Client\Api\SearchByQueryApi(new GuzzleHttp\Client());
	  
	  
	  $atoken = $this->get_fbuser_data("user_token");
	  
	  $type = "RECORD";
	  $query = new \Swagger\Client\Model\SearchQuery(); // \Swagger\Client\Model\SearchQuery | SearchQuery JSON
	  
	  $query['query']= $query_str;
	  $query['method']="POST";
		//$tbl_id = $this->tables[$table_name];	
	  
	  $repositary = "";
	  $mapping = "";
	  
	  
	  
	  try {
	   $oresult = $api_instance->searchByQuery($atoken, $type, $query, $tbl_id,  $repositary, $mapping);
	  // fb_pr($oresult);
	   
	   $sresult = $oresult->getResult();
	   $aresult = json_decode($sresult, true);
	   $total_count = isset($aresult["hits"]["total"]) ? $aresult["hits"]["total"]: "0";
	   $result_set = isset($aresult["hits"]["hits"]) ? $aresult["hits"]["hits"] : array();
	   $agg = isset($aresult["aggregations"]["runtime"]["buckets"]) ? $aresult["aggregations"]["runtime"]["buckets"] : array();
	   $msg = array("status" => "success", "message" => "Search result got it", "total_count" => $total_count,"result_set" => $result_set,"aggs"=>$agg );
	   return $msg;
	  } catch (Exception $e) {
	   $smsg = $e->getMessage();
	   log_message('error', "Exception when calling SearchApi->search: ".$smsg);
	   $msg = array("status" => "fail", "message" => "Search functionality error");
	   return $msg;
	  }
	 }
	
}
