<?php
/**
 * Name:    Fourbends - Boodskap API
 * Author:  Fourbends Dev Team
 *          contactus@fourbends.com
 * Url:    http://www.fourbends.com/
 *
 *
 * Created:  02.03.2018
 *
 * Description:  This file is used to refer the tables list in boodskap.
 * Original Author name has been kept but that does not mean that the method has not been modified.
 *
 * Requirements: PHP5 or above
 *
 * @filesource
 */

$config["settings"]["rest_api_cache"] = false;
$config["settings"]["output_profiler"] = false;


/* End of file */