<?php
/**
 * Name:    Fourbends - Boodskap API
 * Author:  Fourbends Dev Team
 *          contactus@fourbends.com
 * Url:    http://www.fourbends.com/
 *
 *
 * Created:  02.03.2018
 *
 * Description:  This file is used to refer the tables list in boodskap.
 * Original Author name has been kept but that does not mean that the method has not been modified.
 *
 * Requirements: PHP5 or above
 *
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$config['tables'] = array("eb_status"=>"350415",  "devices" => "350404", "power_manage" => "350405","alerts" => "350406","rooms" => "350407","genset" => "350408","genset_activity" =>"350409","genset_list" =>"350400","power-meter"=>"350412" , "alert_settings" => "350401", "alert_notify" => "350402", "sample_genset_activity" => "350410","alert_settings_dg"=>"350411","meters"=>"350414");

$config['devicesArr'] = array(
   "Light" => array( "key" => "light", "value" => "Light","icon" =>"light-bulb-idle.png"), 
    "Fan" => array( "key" => "fan", "value" => "Fan", "icon" =>"fan-idle.png"),
    "AC" => array( "key" => "ac", "value" => "AC","icon" =>"air-conditioner-idle.png"),
    "Microwave oven" => array( "key" => "oven", "value" => "Microwave oven","icon" =>"microwave-idle.png" ), 
    "Washing Machine" => array( "key" => "wm", "value" => "Washing Machine","icon" =>"washing-machine-idle.png" ), 
    "Lamp" => array( "key" => "lamp", "value" => "Lamp","icon" =>"lamp-idle.png" ), 
    "Water heater" => array( "key" => "heater", "value" => "Water heater","icon" =>"water-heater-idle.png" ), 
    "Fridge" => array( "key" => "fridge", "value" => "Fridge","icon" =>"fridge-idle.png" ), 
);

$config["devices"] = array("amps", "device_id", "device_name", "device_status", "voltage", "hrs", "watts", "device_type","createdtime", "updatedtime"); 
					
$config["power_manage"] = array("amps", "phase", "source", "voltage", "createdtime", "updatedtime");

$config["genset"] = array("device_id","device_name","device_type","status","watts","createdtime","updatedtime");

$config["genset_activity"] = array("device_id","device_name","lastontime", "lastofftime");

$config["rooms"] = array("room_id", "room_name", "room_status", "timeout", "guest_access", "last_active", "timeout_last_active", "timeout_enable", "autowakeuptime", "autowakeuplastran", "createdtime", "updatedtime");

$config["alerts"] = array( "alert_id", "alert_state", "alert_type", "msg", "createdtime", "updatedtime" );

$config["genset_list"] = array( "name", "type", "rpm", "minVal", "maxVal","status","rHour","fuel_consumption","createdtime", "updatedtime" );

$config["power-meter"] = array("bc","bp","bv","byv","fre","gen","pf","rbv","rc","rp","rv","tc","tp","tpv","tv","yc","yp","yrv","createdtime","yv","created","meter_id");

$config["alert_settings"] = array("bcpmax", "bcpmin", "bppmax", "bppmin", "bspmax", "bspmin", "btpmax", "btpmin","createdtime", "fremax", "fremin", "pfmax", "pfmin", "rcpmax", "rcpmin", "rppmax", "rppmin", "rspmax", "rspmin", "rtpmax","rtpmin", "spmax", "spmin", "tcpmax", "tcpmin", "tpmax", "tpmin", "tppmax", "tppmin", "updatedtime", "ycpmax", "ycpmin", "yppmax", "yppmin", "yspmin", "ytpmax", "ytpmin", "yspmax");

$config["alert_settings_dg"] = array("dgbcpmax", "dgbcpmin", "dgbppmax", "dgbppmin", "dgbspmax", "dgbspmin", "dgbtpmax", "dgbtpmin","createdtime", "dgfremax", "dgfremin", "dgpfmax", "dgpfmin", "dgrcpmax", "dgrcpmin", "dgrppmax", "dgrppmin", "dgrspmax", "dgrspmin", "dgrtpmax","dgrtpmin", "dgspmax", "dgspmin", "dgtcpmax", "dgtcpmin", "dgtpmax", "dgtpmin", "dgtppmax", "dgtppmin", "updatedtime", "dgycpmax", "dgycpmin", "dgyppmax", "dgyppmin", "dgyspmin", "dgytpmax", "dgytpmin", "dgyspmax");

$config["alert_notify"] = array("createdtime", "updatedtime" , "name", "ph_no", "email");

$config["sample_genset_activity"] = array("genset_id","genset_name","last_on_time", "last_off_time");

$config["meters"] = array("meter_id","name","description", "status","image","createdtime","updatedtime","kva");

$config["eb_status"]=array("status","status_on","status_off");

$config["timezone"] = "Asia/Kolkata";

