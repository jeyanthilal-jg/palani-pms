{
"size":0,
 "query": { "match_all" : {}   },
	"aggs": {
		
		"runtime": {
			"terms": {
				
				"script": "doc.last_off_time.date.secondOfDay - doc.last_on_time.date.secondOfDay"
			},
			"aggs": {
				"tops": {
				  "top_hits": {
					"size": 100000
				  }
				}
			}
		}
		
	}
}