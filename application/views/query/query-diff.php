{
  "query": {
    "match_all": {}
  },
  "sort": {
    "_script": {
      "type": "number",
     
      "script": {
        "lang": "painless",
        "inline": "doc.last_off_time.date.hourOfDay - doc.last_on_time.date.hourOfDay"
      }
    }
  }
}