{
	"query": {
		 "range" : {
			 "__range_fld__" : {
                "gte": "__lowv__",
                "lte": "__topv__",
				"boost" : 2.0
            }
		 }
    },
	"size" : __size__,
	"from": __from__,
	"sort": { "__orderfld__" : {"order" : "__orderdir__"} }
}