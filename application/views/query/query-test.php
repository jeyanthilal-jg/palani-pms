{
    "query" : {
        "match" : {
            "gen" : "1"
        }
    },
    "aggs" : {
        "per_month" : {
            "date_histogram" : {
                "field" : "createdtime",
                "interval" : "now/d",
				"format" : "yyyy-MM-dd"
            },
            "aggs": {
                "devices": {
                    "sum": {
                        "field": "deviceusage"
                    }
                }
            }
        },
        "monthly_sales": {
            "sum_bucket": {
                "buckets_path": "per_month>devices" 
            }
        }
    }
}