{ 
	"size" : 0,
	"query": { "match":{ "gen" : "__val__" } },
    "aggregations" : {
        "runtime" : {
            "date_histogram" : {
                "field" : "createdtime",
                "interval" : "1H"
            },"aggs": {
				"tops": {
				  "top_hits": {
					"size": 1
				  }
				}
			}
        }
    }
}