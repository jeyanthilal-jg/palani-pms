{
    "query": {
        "constant_score" : {
            "filter" : {
                 "bool" : {
                    "should" : [
                        __should_str__
                    ],
					"must_not" : {
						__must_not_str__
					  }
                }
            }
        }
    },
	"from": __from__,
	"size": __size__,
	"sort": { "pondname" : { "order" : "asc" },  "updatedtime" : {"order" : "desc"} }
}
