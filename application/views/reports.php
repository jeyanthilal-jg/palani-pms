  <!-- Page wrapper  -->
  <div class="page-wrapper"> 
    <!-- Bread crumb -->
    <div class="row page-titles">
      <div class="col-md-5 align-self-center">
        <h3 class="text-primary"><?php echo $title; ?></h3>
      </div>
      <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
          <li class="breadcrumb-item active"><?php echo $title; ?></li>
        </ol>
      </div>
    </div>
    <!-- End Bread crumb --> 
    <!-- Container fluid  -->
    <div class="container-fluid"> 
      <!-- Start Page Content -->
      <div class="row">
        <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Data Export</h4>
                                <h6 class="card-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6>
                                <div class="row">
									<div class="col-md-4">
										<div class="form-group">
											<label class="control-label">From Date</label>
											<input type="text" id="min-date" class="form-control calendar date-range-filter " />
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label class="control-label">To Date</label>
											<input type="text" id="max-date" class="form-control calendar date-range-filter"/>
										</div>
								</div> 
								<div class="table-responsive m-t-40">
								
                                    <table id="report-table" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
								<thead>
									<tr>
										<td>Date - Time</td>
										<td>Power Factor </td>
										<td>Frequency</td>
										<td>Total single Phase Voltage</td>
										<td>Total Three Phase Voltage</td>
										<td>TOTAL Current </td>
										<td>Total POWER (KW) </td>
										<td>Total Power Per Hour</td>
									</tr>
								</thead>
                                <tfoot>
									<tr>
										<td>Date - Time</td>
										<td>Power Factor </td>
										<td>Frequency</td>
										<td>Total single Phase Voltage</td>
										<td>Total Three Phase Voltage</td>
										<td>TOTAL Current </td>
										<td>Total POWER (KW) </td>
										<td>Total Power Per Hour</td>
									</tr>
                                </tfoot>
                                        <tbody>
										<?php //fb_pr($presult_data);
										foreach($presult_data as $row){
											foreach($row as $src){
												
											$source = $src['_source'];
											?>
                                            <tr>
												<td><?php echo fb_convert_jsdate($source["createdtime"]); ?></td>
												<td><?php echo $source["pf"]; ?></td>
												<td><?php echo $source["fre"]; ?></td>
                                                <td><?php echo $source["tv"]; ?></td>
												<td><?php echo $source["tpv"]; ?></td>
												<td><?php echo $source["tc"]; ?></td>
												<td><?php echo $source["tp"]; ?></td>
												<td><?php echo ($source["tp"] * 1 ); ?></td>
                                            </tr>
											<?php } } ?>
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
        </div>
      
      
      <!-- End PAge Content --> 
    </div>
    <!-- End Container fluid  --> 
    <!-- footer --> 
    
    <!-- End footer --> 
  </div>
  <!-- End Page wrapper  --> 
</div>
<!-- End Wrapper -->
</div>
