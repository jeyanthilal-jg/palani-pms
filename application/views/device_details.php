  <!-- header header  -->
  <?php $this->load->view('include/header');	?>
  <!-- End header header --> 
  <!-- Left Sidebar  -->
  <?php $this->load->view('include/left-sidebar');	?>
			<div class="page-wrapper">
            <!-- Bread crumb -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-primary"><?php echo $device['device_name'];?></h3> </div>
                <div class="col-md-7 align-self-center">
                   							
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo site_url("/dashboard");?>">Home</a></li>
                        <li class="breadcrumb-item active">Devices</li>
                    </ol>
                </div>
            </div>
            <!-- End Bread crumb -->
			<!-- Container fluid  -->
            <div class="container-fluid">
                <!-- Start Page Content -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-lg-6">
                              <button class="btn btn-primary m-b-10 m-l-5" id="plain">Plain</button>
                                <button class="btn btn-info m-b-10 m-l-5" id="inverted">Inverted</button>
                                <button class="btn btn-danger m-b-10 m-l-5" id="polar">Polar</button>                        
                        </div>
                          <div class="card">
                              <div id="device_hour"></div>
                        </div>
                    </div>
                    
                    <div class="col-md-12">
                        <div class="col-lg-6">
                              <button class="btn btn-primary m-b-10 m-l-5" id="plain-year">Plain</button>
                                <button class="btn btn-info m-b-10 m-l-5" id="inverted-year">Inverted</button>
                                <button class="btn btn-danger m-b-10 m-l-5" id="polar-year">Polar</button>                        
                        </div>
                          <div class="card">
                              <div id="device_year"></div>
                        </div>
                    </div>
                </div>

                <!-- /# row -->
			</div>
</div>