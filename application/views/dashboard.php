  <!-- header header  -->
  <?php $this->load->view('include/header');	?>
  <!-- End header header --> 
  <!-- Left Sidebar  -->
  <?php $this->load->view('include/left-sidebar');	?>
  <!-- End Left Sidebar  --> 
  <!-- Page wrapper  -->
  <div class="page-wrapper"> 
    <!-- Bread crumb -->
    <div class="row page-titles">
      <div class="col-md-5 align-self-center">
        <h3 class="text-primary">Dashboard</h3>
      </div>
      <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
          <!--<li class="m-r-15">
            <div class="dropdown">
            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Switch DG
            <span class="caret"></span></button>
            <ul class="dropdown-menu">
              <?php 
              $rec_id = "350414";
              $meters = $this->iot_rest->activemeter_list($rec_id);
              $meters = $meters["data"];
              if(count($meters)>0){
              foreach ($meters as $meter) {
                $source = $meter['_source'];
              ?>
                <li><a href="#"><?php echo $source['name']; ?></a></li>
              <?php } } else {?>
                <li>No active DG</li>
              <?php } ?>
            </ul>
            </div>
        
          </li>-->
          <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
          <li class="breadcrumb-item active">Dashboard</li>
        </ol>
      </div>
    </div>
    <!-- End Bread crumb --> 
    <!-- Container fluid  -->
    <div class="container-fluid" id="sortable-area"> 
      <!-- Start Page Content -->
      <div class="row sortable-grid">
      <?php //print_r($result); exit();

      foreach ($result as $row) {
        
        //echo 'widget_'.$row->callBack;
          //call_user_func_array($this->iot_rest->'widget_'.$row->callBack,array());
          call_user_func_array(array($this->iot_rest, 'widget_'.$row->callBack), array($row->id));

        } ?>
      
      <!-- End PAge Content --> 
    </div>

    <!-- End Container fluid  --> 
    <!-- footer --> 
    
    <!-- End footer --> 
  </div>
  <!-- End Page wrapper  --> 
<!-- Delete Modal -->
<div class="modal" id="livegraph-modal" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="staticModalLabel">Live Consumption</h5>
             <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
        </div>
        <div class="modal-body">
        <div class="col-md-12 livegraph_consumption">
          <div class="card">
            <div class="card-content">

            <div class="graph-preloader">
              <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"></circle> 
              </svg>
            </div>
          <!--<div class="button-list">
                <button type="button" class="btn btn-success btn-sm m-b-10 m-l-5 btn-graph-filter" data-val="1d">1d</button>
                <button type="button" class="btn btn-success btn-sm m-b-10 m-l-5 btn-graph-filter" data-val="7d">7d</button>
                <button type="button" class="btn btn-success btn-sm m-b-10 m-l-5 btn-graph-filter" data-val="1m">1m</button>
                <button type="button" class="btn btn-success btn-sm m-b-10 m-l-5 btn-graph-filter" data-val="1y">1Y</button>
        </div>-->
              <div id="livechart" style="height: 400px"></div>
            </div>
          </div>
        </div>          
        </div>
        <div class="modal-footer" >
             <button type="button" id="close" class="btn btn-secondary" data-dismiss="modal">close</button>
        </div>
    </div>
</div>
</div>
<?php $this->load->view('include/footer');	?>
