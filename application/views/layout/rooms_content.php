  <!-- header header  -->
  <?php $this->load->view('./include/header');	?>
  <!-- End header header --> 
  <!-- Left Sidebar  -->
  <?php $this->load->view('./include/left-sidebar');	?>
			<div class="page-wrapper">
            <!-- Bread crumb -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-primary">Rooms</h3> </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo site_url("/dashboard");?>">Home</a></li>
                        <li class="breadcrumb-item active">Rooms</li>
                    </ol>
                </div>
            </div>
            <!-- End Bread crumb -->
			<!-- Container fluid  -->
            <div class="container-fluid">
                <!-- Start Page Content -->
				<div class="row">
				
                    <div class="col-lg-12">
                        <div class="card" id="deviceform-card">
                            <div class="card-title">
                                <h4>Rooms </h4>

                            </div>
                            <div class="card-body">
								<button type="button" class="btn btn-primary btn-sm" style="float: right;" data-toggle="modal" data-target="#room">Add Room</button>
							
                                <div class="table-responsive">
                                    <table class="table table-striped">
                                        <tbody>
										
										<?php foreach($result_set as $row){ 
										  $source = $row["_source"];
										  $rkey = $row["_id"];
										  /* $voltage = $source["voltage"];
										  $current = $source["amps"];
										  $kw = (1.7321 * 0.99 * $current * $voltage)/1000; */
									   ?>
									   
                                            <tr>
												<td><?php echo $source["room_id"]; 
												$d_id = $source["room_id"]; ?></td>
                                                <td scope="row"><b><?php echo $source["room_name"]; ?></b>
												<?php 
												if(!empty($state)):
												//print_r($state);
													$i=0;
													foreach($state as $status):
														$source = $status["_source"];
														
														 if($source["device_status"]=='true')
															 $state="true";
														
														
												?>
                                                <td><?php $state = $source["device_status"];
													echo "<label class='switch'>";
													echo "<input type='checkbox' data-id='$rkey' data-device_id='$d_id' class='rstatus-switch' checked>";
													echo "<span class='slider round'></span>";
													echo "</label>";
												?></td>
											</tr>
                                            
                                            
										<?php $i++;
											endforeach;
											endif;
												} ?>
										
                                        </tbody>
                                    </table>
                                </div>
                            
							</div>
                        </div>
                    </div>
                    <!-- /# column -->
				</div>
                <!-- /# row -->
			</div>
			
<!--device Form-->
<div class="modal" id="room" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
    <form name="rooms" id="room-form" method="post" action="<?php echo base_url('rooms/add_record');?>">
		<div class="modal-content">

			
				<div class="modal-header">
					<h5 class="modal-title" id="deviceLabel"><b>Add Room</b></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
					
				</div>
				<div class="modal-body">
				<div class="modal-loader">
					<div class="form-group">
					<div class="form-group col-md-12">
					<label>Room Id:</label>
						<input type="text" id="roomidmodel" name="room_id"  class="form-control" placeholder="first letter of room and unique number"/>
					</div>
					</div>
					<div class="form-group">
					<div class="form-group col-md-12">
					<label>Room Name:</label>
						<input type="text" id="roomnamemodel" name="room_name" class="form-control" placeholder="Name of the Device"/>
					</div>
					</div>
					<div class="form-group">
						<div class="form-group col-md-12">
						<label>Guest Access:</label>
						<select name="guest_access" id="guest_access" class="form-control">
						  <option selected value="">Choose...</option>
						  <option value="true">Yes</option>
						  <option value="false">No</option>
						</select>
						</div>
					</div>
					<div class="form-group">
						<div class="form-group col-md-12">
						<label>Timeout Enabled:</label>
						<input type="checkbox" name="timeout_enabled"> 
						<input type="number" name="timeout" value="30">minutes
						</div>
					</div>
				</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary">Add Room</button>
					<button type="button" class="btn btn-secondary clear">Clear</button>
				</div>
				
			</div>
		</form>
	</div>
</div>

