<div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
{message}
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
</div>
<br/>