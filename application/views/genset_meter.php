  <!-- Page wrapper  -->
  <div class="page-wrapper"> 
    <!-- Bread crumb -->
    <div class="row page-titles">
      <div class="col-md-5 align-self-center">
        <h3 class="text-primary">Dashboard</h3>
      </div>
      <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
          <li class="breadcrumb-item active">Dashboard</li>
        </ol>
      </div>
    </div>
    <!-- End Bread crumb --> 
    <!-- Container fluid  -->
    <div class="container-fluid"> 
      <!-- Start Page Content -->
      <div class="row">
        <div class="col-md-6">
          <div class="card bg-primary p-20">
            <div class="media widget-ten">
              <div class="media-left meida media-middle"> <span><i class="fa fa-bolt fa-3x"></i></span> </div>
              <div class="media-body media-text-right">
                <p class="m-b-0 m-l-20 media-text-left">Three Phase Voltage</p>
                <h2 class="color-white odometer" id="avgtot3"><?php 
                    $CI =& get_instance();
                    echo $CI->getVoltValue();?></h2>
                <p class="m-b-0">V</p>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="card bg-primary p-20">
            <div class="media widget-ten">
              <div class="media-left meida media-middle"> <span><i class="fa fa-bolt fa-3x"></i></span> </div>
              <div class="media-body media-text-right">
                <p class="m-b-0 m-l-20 media-text-left">Single Phase Voltage</p>
                <h2 class="color-white odometer" id="total-volt-r"><?php 
                    $CI =& get_instance();
                    echo $CI->getVoltSingle();?></h2>
                <p class="m-b-0">V</p>
              </div>
            </div>
          </div>
        </div> 
        <div class="col-md-6">
          <div class="card bg-danger  p-20">
            <div class="media widget-ten">
              <div class="media-left meida media-middle"> <span><i class="fa fa-bolt fa-3x"></i></span> </div>
              <div class="media-body media-text-right">
                  <p class="m-b-0 m-l-20 media-text-left">KVA</p>
                <h2 class="color-white odometer">
                    <?php $CI =& get_instance();
                    echo $CI->formulaKVA(3); ?></h2>
                <p class="m-b-0">KVA</p>
              </div>
            </div>
          </div>
        </div>  
        <div class="col-md-6">
          <div class="card bg-danger  p-20">
            <div class="media widget-ten">
              <div class="media-left meida media-middle"> <span><i class="fa fa-bolt fa-3x"></i></span> </div>
              <div class="media-body media-text-right">
                  <p class="m-b-0 m-l-20 media-text-left">KVA</p>
                <h2 class="color-white odometer">
                    <?php $CI =& get_instance();
                    echo $CI->formulaKVA(1); ?></h2>
                <p class="m-b-0">KVA</p>
              </div>
            </div>
          </div>
        </div>          
        <div class="col-md-6">
          <div class="card bg-success p-20">
            <div class="media widget-ten">
              <div class="media-left meida media-middle"> <span><i class="fa fa-bolt fa-3x"></i></span> </div>
              <div class="media-body media-text-right">
                  <p class="m-b-0 m-l-20 media-text-left">Power</p>
                <h2 class="color-white odometer">
                    <?php $CI =& get_instance();
                    echo $CI->formulaPower(3); ?>(KW)</h2>
              </div>
            </div>
          </div>
        </div>   
        <div class="col-md-6">
          <div class="card bg-success p-20">
            <div class="media widget-ten">
              <div class="media-left meida media-middle"> <span><i class="fa fa-bolt fa-3x"></i></span> </div>
              <div class="media-body media-text-right">
                  <p class="m-b-0 m-l-20 media-text-left">Power</p>
                <h2 class="color-white odometer">
                    <?php $CI =& get_instance();
                    echo $CI->formulaPower(1); ?>(KW)</h2>
              </div>
            </div>
          </div>
        </div>
          <!--<div class="col-md-6">
          <div class="card bg-success p-20">
            <div class="media widget-ten">
              <div class="media-left meida media-middle"> <span><i class="fa fa-bolt fa-3x"></i></span> </div>
              <div class="media-body media-text-right">
                  <p class="m-b-0 m-l-20 media-text-left">KWH</p>
                <h2 class="color-white">
                    <?php $CI =& get_instance();
                    echo $CI->formulaPower(3); ?>(KWH)</h2>
              </div>
            </div>
          </div>
        </div>   
        <div class="col-md-6">
          <div class="card bg-success p-20">
            <div class="media widget-ten">
              <div class="media-left meida media-middle"> <span><i class="fa fa-bolt fa-3x"></i></span> </div>
              <div class="media-body media-text-right">
                  <p class="m-b-0 m-l-20 media-text-left">KWH</p>
                <h2 class="color-white">
                    <?php $CI =& get_instance();
                    echo $CI->formulaPower(1); ?>(KWH)</h2>
              </div>
            </div>
          </div>
        </div>    -->              
        <div class="col-md-6">
          <div class="card bg-warning p-20">
            <div class="media widget-ten">
              <div class="media-left meida media-middle"> <span><i class="fa fa-bolt fa-3x"></i></span> </div>
              <div class="media-body media-text-right">
                <p class="m-b-0 m-l-20 media-text-left">Frequency</p>
                <h2 class="color-white odometer" id="freq"></h2>
                <p class="m-b-0">Hz</p>
              </div>
            </div>
          </div>
        </div>                     

        <div class="col-md-6">
          <div class="card bg-pink p-20">
            <div class="media widget-ten">
              <div class="media-left meida media-middle"> <span><i class="fa fa-bolt fa-3x"></i></span> </div>
              <div class="media-body media-text-right">
                <p class="m-b-0 m-l-20 media-text-left">Current</p>
                <h2 class="color-white odometer" id="total-current"></h2>
                <p class="m-b-0">A</p>
              </div>
            </div>
          </div>
        </div>          
        <div class="col-md-6">
          <div class="card bg-dark  p-20">
            <div class="media widget-ten">
              <div class="media-left meida media-middle"> <span><i class="fa fa-bolt fa-3x"></i></span> </div>
              <div class="media-body media-text-right">
                  <p class="m-b-0 m-l-20 media-text-left">Power Factor</p>
                <h2 class="color-white odometer" id="powerfactor"></h2>
                <p class="m-b-0">PF</p>
              </div>
            </div>
          </div>
        </div>           
      </div>
      
      <!-- /# row -->
      
      <div class="row">
        <div class="col-lg-6">
          <div class="card">
            <div class="card-title">
            </div>
            <div class="card-content">
              <div id="gauge-voltage" style="height: 370px"></div>
            </div>
          </div>
          <!-- /# card --> 
        </div>
        <div class="col-lg-6">
          <div class="card">
            <div class="card-title">
            </div>
            <div class="card-content">
              <div id="gauge-voltage-single" style="height: 370px"></div>
            </div>
          </div>
          <!-- /# card --> 
        </div>        
      </div>
        <div class="row">
                    <!-- /# column -->
        <div class="col-lg-12">
          <div class="card">
            <div class="card-title">
            </div>
            <div class="card-content">
              <div id="gauge-amp" style="height: 370px"></div>
            </div>
          </div>
        </div>
        </div>
      <!-- <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-title">
              Consumption by week
            </div>
            <div class="card-content text-center">
              <svg width="600" height="600" id="energy-d3">
                  <g id="status">
                    <g class="first">
                      <text class="time" x="300" y="237"></text>
                      <text class="value" x="300" y="276"></text>
                      <text class="units" x="300" y="290"></text>
                    </g>
                    <g class="second">
                      <text class="time" x="255" y="320"></text>
                      <text class="value" x="255" y="347"></text>
                      <text class="units" x="255" y="360"></text>
                    </g>
                    <g class="third">
                      <text class="time" x="345" y="320">Year</text>
                      <text class="value" x="345" y="347"></text>
                      <text class="units" x="345" y="360"></text>
                    </g>
                  </g>
                  <text class="week_step" id="downweek" x="70" y="570">-</text>
                  <text class="week_step" id="upweek" x="500" y="570">+</text>
                </svg>
            </div>
          </div>
        </div>
      </div>        -->
      <div class="row">
        <!-- /# column -->
        <div class="col-lg-12">
          <div class="card">
            <div class="card-title">
              
            </div>
            <div class="card-content">
              <div id="linechart" style="height: 370px"></div>
            </div>
          </div>
        </div>
      </div>
    <!-- <div class="row">
        <div class="col-md-6">
          <div class="card">
            <div class="card-title">
              Water level indicator
            </div>
            <div class="card-content">
              <svg id="fillgauge1" width="97%" height="250" ></svg>
            </div>
          </div>
        </div>
        
        <div class="col-md-6">
          <div class="card">
            <div class="card-title">
              Main Lamp
            </div>
            <div class="card-content">
                <div id="mainlamp"></div>
            </div>
          </div>
        </div>
        
        </div> --!>
      
      <!-- End PAge Content --> 
    </div>
    <!-- End Container fluid  --> 
    <!-- footer --> 
    
    <!-- End footer --> 
  </div>
  <!-- End Page wrapper  --> 


