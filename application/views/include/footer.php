    <!-- All Jquery -->
<script src="<?php echo base_url();?>assets/js/lib/jquery/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/jquery-ui/jquery-ui.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/jquery-ui/jquery.ui.touch-punch.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
<script src="<?php echo base_url();?>assets/js/lib/bootstrap/js/popper.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
<script src="<?php echo base_url();?>assets/js/jquery.slimscroll.js"></script>
    <!--Menu sidebar -->
<script src="<?php echo base_url();?>assets/js/sidebarmenu.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/moment/moment.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/underscore/underscore-min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/handlebar/handlebars.min.js"></script>

    <!--stickey kit -->
<script src="<?php echo base_url();?>assets/js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/datepicker/bootstrap-datepicker.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/form-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/echart/echarts.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/highchart/highstock.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/highchart/highcharts-more.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/d3/d3.v5.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/datatables/datatables.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/odometer/jquery-countTo.js"></script>
<!--Custom JavaScript -->
<script src="<?php echo base_url();?>assets/js/liquidFillGauge.js"></script>
<script src="<?php echo base_url();?>assets/js/clock.js"></script>
<script src="<?php echo base_url();?>assets/js/energy.js"></script>
<script src="<?php echo base_url();?>assets/js/genset.js"></script>
<script src="<?php echo base_url();?>assets/js/custom.min.js"></script>
<script src="<?php echo base_url();?>assets/js/app.js"></script>


</body>

</html>