<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Tell the browser to be responsive to screen width -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<!-- Favicon icon -->
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url();?>assets/images/favicon.png">
<title>Power Management System</title>

<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url();?>assets/css/lib/bootstrap/bootstrap.min.css" rel="stylesheet">
<!-- Custom CSS -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/lib/datepicker/bootstrap-datepicker3.min.css" />    
<link href="<?php echo base_url();?>assets/css/helper.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:** -->
<!--[if lt IE 9]>
    <script src="https:**oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https:**oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="fix-header fix-sidebar">
<input type="hidden" id="voltage_value" value="<?php echo getVoltage();?>">   
<input type="hidden" name="kva_value" id="kva_value" value="<?php echo getKVA();?>"> 
<input type="hidden" name="amp_value" id="amp_value" value="<?php echo getAmpValue();?>"> 
<input type="hidden" name="fre_value" id="fre_value" value="<?php echo getFrequency();?>">

<!-- Preloader - style you can find in spinners.css -->
<div class="preloader"> <svg class="circular" viewBox="25 25 50 50">
  <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
  </svg> </div>
<input type="hidden" value="<?php echo base_url();?>" id="base">
<!-- Main wrapper  -->
<div id="main-wrapper"> 
    <div class="header">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- Logo -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="<?php echo base_url(); ?>dashboard">
                        <!-- Logo icon -->
                        <b><span><i class="fa fa-bolt fa-1x"></i></span></b>
                        <!--End Logo icon -->
                        <!-- Logo text -->
                        <span>PMS</span>
                    </a>
                </div>
                <!-- End Logo -->
                <div class="navbar-collapse">
                    <!-- toggle and nav items -->
                    <ul class="navbar-nav mr-auto mt-md-0">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted  " href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                        <li class="nav-item m-l-10"> <a class="nav-link sidebartoggler hidden-sm-down text-muted  " href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                        <!-- End Messages -->
                    </ul>
                    <!-- User profile and search -->
                    <ul class="navbar-nav my-lg-0">
                        <?php 
                        $getSeg= $this->uri->segment(2); 
                        if($getSeg=="view"){?>
                        <li class="nav-item" style="padding:16px;">
                        <span class="text-muted">Running on :  </span><span class="label label-rouded label-danger" id="flash_power"><b>OFF</b></span>
                        </li>
                       <?php }?>
						<li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted  " href="#" id="2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-envelope"></i>
								<div class="notify"> <span class="heartbit"></span> <span class="point"></span> </div>
							</a>
                            <div class="dropdown-menu dropdown-menu-right mailbox animated zoomIn" aria-labelledby="2">
							
									<?php
									//$count=array();
									$count = fb_total_count();
									$result = array();
									$result = fb_fetch_alert();
									
									
									?>
									<ul>
										<li>
											<div class="drop-title">You have <?php echo $count; ?> new messages</div>
										</li>
										<!-- Message -->
										<?php
									   foreach($result as $result_set){
										if(is_array($result_set)){
											foreach($result_set as $row){ 
											
											$source = $row["_source"];
											$rkey = $row["_id"]; 
									    ?>
										<a href="#">
											<div class="mail-content" data-id=<?php echo $rkey; ?>>
												<h5><?php echo $source["alert_type"];?></h5> <span class="mail-desc"><?php echo $source["msg"];?></span> <span class="time"><?php echo fb_convert_date_time($source["updatedtime"]);?></span>
											</div>
										</a>
									
										<?php } } } ?>
									
							
									<li>
										<a class="nav-link text-center" href="<?php echo base_url(); ?>alerts/list_all_alerts"> <strong>View all Alerts</strong> <i class="fa fa-angle-right"></i> </a>
									</li>
								</ul>
							</div>
                        </li>
                        <!-- Profile -->
                        <li class="nav-item" style="padding:16px;">
                        <span><?php 
						   $username = $this->session->userdata("first_name");
						   echo "Welcome <b>". ucfirst($username)."</b>"; ?></span>
                           </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted  " href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?php echo base_url();?>assets/images/users/5.jpg" alt="user" class="profile-pic" /></a>
                            <div class="dropdown-menu dropdown-menu-right animated zoomIn">
                                <ul class="dropdown-user">
                                    <!--<li><a href="#"><i class="ti-settings"></i> Setting</a></li>-->
                                    <li><a href="<?php echo base_url('login/logout');?>"><i class="fa fa-power-off"></i> Logout</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>