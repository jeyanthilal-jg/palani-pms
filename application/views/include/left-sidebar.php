<div class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="nav-devider"></li>
                        <li class="nav-label">Home</li>
                        <li> <a class="" href="<?php echo base_url(); ?>dashboard" aria-expanded="false"><i class="fa fa-tachometer-alt"></i><span class="hide-menu">Dashboard</span></a>

                        </li>

                        <li> <a class="" href="<?php echo base_url(); ?>meters" aria-expanded="false"><i class="fa fa-bolt"></i><span class="hide-menu">Manage DG</span></a>
                        </li>
                         <!--<li> <a class="" href="<?php echo base_url(); ?>dg" aria-expanded="false"><i class="fa fa-power-off"></i><span class="hide-menu">Genset </span></a>
                        </li>-->
                        
                         <!--<li> <a class="" href="<?php echo base_url(); ?>devices" aria-expanded="false"><i class="fa fa-lightbulb"></i><span class="hide-menu">Devices </span></a> 

                        </li>  -->                      
 
                         <li> <a class="has-arrow" href="#" aria-expanded="false"><i class="ti-notepad"></i><span class="hide-menu">Reports</span></a>

                                <ul aria-expanded="false" class="collapse">
                                    <li><a href="<?php echo base_url(); ?>dashboard/geteb_reports">Main Report</a></li>
                                    <li><a href="<?php echo base_url(); ?>dashboard/getdg_reports">DG Report</a></li>
									<li><a href="<?php echo base_url(); ?>dashboard/get_dg_running_hrs">DG Running Report</a></li>
                                </ul>                            

                        </li>
						
						 <li> <a class="" href="<?php echo base_url(); ?>settings" aria-expanded="false"><i class="fa fa-cog"></i><span class="hide-menu">Settings</span></a>

                        </li>
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </div>