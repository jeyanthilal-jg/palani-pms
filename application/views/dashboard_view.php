  <!-- header header  -->
  <?php $this->load->view('include/header');	?>
  <!-- End header header --> 
  <!-- Left Sidebar  -->
  <?php $this->load->view('include/left-sidebar');	?>
  <!-- End Left Sidebar  --> 
  <!-- Page wrapper  -->
  <div class="page-wrapper"> 
    <!-- Bread crumb -->
    <div class="row page-titles">
      <div class="col-md-5 align-self-center">
        <h3 class="text-primary">Dashboard</h3>
      </div>
      <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
          <!--<li class="m-r-15">
            <div class="dropdown">
            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Switch DG
            <span class="caret"></span></button>
            <ul class="dropdown-menu">
              <?php 
              $rec_id = "350414";
              $meters = $this->iot_rest->activemeter_list($rec_id);
              $meters = $meters["data"];
              if(count($meters)>0){
              foreach ($meters as $meter) {
                $source = $meter['_source'];
              ?>
                <li><a href="#"><?php echo $source['name']; ?></a></li>
              <?php } } else {?>
                <li>No active DG</li>
              <?php } ?>
            </ul>
            </div>
        
          </li>-->
          <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
          <li class="breadcrumb-item active">Dashboard</li>
        </ol>
      </div>
    </div>
    <!-- End Bread crumb --> 
    <!-- Container fluid  -->

    <div class="container-fluid" id="dbaord-view"> 
      <?php 
     if(count($presult_data)>0){
       $i=0; 
        foreach($presult_data as $row){
          $source = $row['_source'];
          $id = $source['meter_id'];
          $name = $source['name'];
          $i++;
          if($i==1){
            $icon ="fa-minus-circle";
            $show="show";
          }
          else{
            $icon ="fa-plus-circle";
            $show="";
          }
        ; ?>
    <div class="panel-group" role="tablist" aria-multiselectable="true" data-id="<?=$id ?>">
      <div class="panel panel-default dbaord-view">
      <div class="panel-heading" role="tab" id="heading-<?=$id ?>">
        <h4 class="panel-title bg-primary">
      
        <a role="link" title="View Meter" target="_blank" href="<?php echo base_url();?>dashboard/view/<?=$id ?>" class="pull-right toggle-view">
          <i class="fa fa-info-circle"></i>
        </a>

          <a role="button" data-toggle="collapse" class="" data-parent="#accordion" href="#collapse-<?=$id ?>" aria-expanded="true" aria-controls="collapse-<?=$id ?>">
            <i class="more-less fa <?=$icon?>"></i>
            <?=$name ?>
          </a>
        </h4>
      </div>
     
    <div id="collapse-<?=$id ?>" data-id="<?=$id?>" class="panel-collapse collapse <?=$show?>" role="tabpanel" aria-labelledby="heading-<?=$id ?>">
        <div class="graph-preloader" style="height: 200px; position: relative;top: 10px">
            <svg class="circular" viewBox="25 25 50 50">
              <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"></circle> 
           </svg>
        </div>
               
        </div>
 
      </div>
    </div>
  <?php } }?>
     </div>

  <script type="text/javascript">
    var meterList = '<?php echo json_encode($presult_data); ?>';
  </script>
  <script id="template-meterWidgets" type="text/x-handlebars-template"> 
      {{#objects}}  
        <div class="panel-body row">
            <div class="col-md-4">
            <div class="card bg-success p-20 drag-widget">
              <div class="media widget-ten">
                <div class="media-left meida media-middle"> <span><i class="fa fa-bolt fa-3x"></i></span> </div>           
                <div class="media-body media-text-right">
                    <p class="m-b-4 m-l-10 media-text-left">VOLTAGE</p>                
                  <div class="row">
                    <div class="col-sm-4">R : <span class="widget-span odometer" >{{formatFloat rv}}</span> V</div>
                    <div class="col-sm-4">B : <span class="widget-span odometer" >{{formatFloat bv}}</span> V</div>
                    <div class="col-sm-4">Y : <span class="widget-span odometer" >{{formatFloat yv}}</span> V</div>
                  </div>
                  <div class="m-t-6 m-l-20">LL : <span class="widget-span odometer">{{formatFloat tpv}}</span> V</div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card bg-success p-20 drag-widget">
              <div class="media widget-ten">
                <div class="media-left meida media-middle"> <span><i class="fa fa-bolt fa-3x"></i></span> </div>           
                <div class="media-body media-text-right">
                    <p class="m-b-4 m-l-10 media-text-left">POWER</p>                
                  <div class="row">
                    <div class="col-sm-4">R : <span class="widget-span odometer">{{formatFloat rp}}</span> W</div>
                    <div class="col-sm-4">B : <span class="widget-span odometer">{{formatFloat bp}}</span> W</div>
                    <div class="col-sm-4">Y : <span class="widget-span odometer">{{formatFloat yp}}</span> W</div>
                  </div>
                  <div class="m-t-6 m-l-20">LL : <span class="widget-span odometer">{{formatFloat tp}}</span> KW</div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card bg-success p-20 drag-widget">
              <div class="media widget-ten">
                <div class="media-left meida media-middle"> <span><i class="fa fa-bolt fa-3x"></i></span> </div>           
                <div class="media-body media-text-right">
                    <p class="m-b-4 m-l-10 media-text-left">Amp</p>                
                  <div class="row">
                    <div class="col-sm-4">R : <span class="widget-span odometer">{{formatFloat rc}}</span> A</div>
                    <div class="col-sm-4">B : <span class="widget-span odometer">{{formatFloat bc}}</span> A</div>
                    <div class="col-sm-4">Y : <span class="widget-span odometer">{{formatFloat yc}}</span> A</div>
                  </div>
                  <div class="m-t-6 m-l-20">LL : <span class="widget-span odometer">{{formatFloat tc}}</span> A</div>
                </div>
              </div>
            </div>
          </div> 
          <div class="col-md-3">
          <div class="card bg-danger  p-20 drag-widget">
            <div class="media widget-ten">
              <div class="media-left meida media-middle"> <span><i class="fa fa-bolt fa-3x"></i></span> </div>
              <div class="media-body media-text-right">
                  <p class="m-b-0 m-l-20 media-text-left">KVA</p>
                <h2 class="color-white odometer">{{formatFloat kva}}</h2>
                <p class="m-b-0">KVA</p>
              </div>
            </div>
          </div>
        </div>  
        <div class="col-md-3">
          <div class="card bg-dark  p-20 drag-widget">
            <div class="media widget-ten">
              <div class="media-left meida media-middle"> <span><i class="fa fa-bolt fa-3x"></i></span> </div>
              <div class="media-body media-text-right">
                  <p class="m-b-0 m-l-20 media-text-left">Power Factor</p>
                <h2 class="color-white odometer">{{formatFloat pf}}</h2>
                <p class="m-b-0">PF</p>
              </div>
            </div>
          </div>
        </div>    
        <div class="col-md-3">
          <div class="card bg-warning p-20 drag-widget">
            <div class="media widget-ten">
              <div class="media-left meida media-middle"> <span><i class="fa fa-bolt fa-3x"></i></span> </div>
              <div class="media-body media-text-right">
                <p class="m-b-0 m-l-20 media-text-left">Frequency</p>
                <h2 class="color-white odometer">{{formatFloat fre}}</h2>
                <p class="m-b-0">Hz</p>
              </div>
            </div>
          </div>
        </div>     
          </div> 
         {{/objects}}   


  </script>
</div>
  <!-- End Page wrapper  --> 
<?php $this->load->view('include/footer');	?>
