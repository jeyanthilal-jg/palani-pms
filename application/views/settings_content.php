  <!-- header header  -->
  <?php $this->load->view('include/header');	?>
  <!-- End header header --> 
  <!-- Left Sidebar  -->
  <?php $this->load->view('include/left-sidebar');	?>
  <!-- End Left Sidebar  --> 
  <!-- Page wrapper  -->
  <div class="page-wrapper"> 
    <!-- Bread crumb -->
    <div class="row page-titles">
      <div class="col-md-5 align-self-center">
        <h3 class="text-primary">Settings</h3>
      </div>
      <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
          <li class="breadcrumb-item active">Settings</li>
        </ol>
      </div>
    </div>
    <!-- End Bread crumb --> 
    <!-- Container fluid  -->
	<div class="container-fluid">
                <!-- Start Page Content -->
		<div class="row justify-content">
		
			<div class="col-md-12">
			  <div class="card" id="settings-card">
				<div class="card-title">Alert Setings</div>
				<div class="card-body">
				  
					<button type="button" class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#notify-card">Add New</button>
					<?php if($this->session->flashdata('a_success')) {
					?>
					<div class="sufee-alert alert with-close alert-success alert-dismissible fade show">                       
					<?php echo $this->session->flashdata('a_success');  ?>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
					</div> 
					<?php } ?>                 
					<?php if($this->session->flashdata('update_success')) {
					?>
					<div class="sufee-alert alert with-close alert-success alert-dismissible fade show"> <?php echo $this->session->flashdata('update_success');  ?>
					  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
					</div>
					<?php } ?>
					<?php if($this->session->flashdata('update_failed')) { ?>
					<div class="sufee-alert alert with-close alert-danger alert-dismissible fade show" data-dismiss="alert"> <?php echo $this->session->flashdata('update_failed');  ?>
					  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
					</div>
					<?php } ?>  
					<?php if($this->session->flashdata('failed')) {
					?>
					<div class="sufee-alert alert with-close alert-danger alert-dismissible fade show" data-dismiss="alert">
					<?php echo $this->session->flashdata('failed');  ?>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
					</div> 
					<?php } ?>   
					
					<div class="table-responsive-sm">
					
						<table class="table table-bordered" role="grid" id="settings-table">
							<thead>
								<tr>
									<td>Name</td>
									<td>Phone Num</td>
									<td>Email</td>
									<td>Actions</td>
								</tr>
							</thead>
							 
							<tbody>
								<?php foreach($result_set1 as $key=>$row): 
									  $source = $row["_source"];
									  $rkey = $row["_id"];
									  
									 
								   ?>
								<tr>
								
									<th><?php echo $source["name"]; ?></th>
									<td><?php echo $source["ph_no"]; ?></td>
									<td><?php echo $source["email"]; ?></td>
									<td>
									
									<a href="javascript:void(0);" data-id="<?=$rkey?>" class="edit-notify" ><i class="fa fa-edit"></i></a>&nbsp;&nbsp;
									<a href="#" data-id="<?=$rkey?>" class="delete-modal" data-toggle="modal" data-target="#delete-modal"><i class="fa fa-trash"></i></a>&nbsp;&nbsp;
								
									</td>
								
								</tr>
								<?php endforeach; ?>
							</tbody>
							
								
							
						</table>
					</div>        
				  
				</div>
			  </div>
			</div>
				
			<div class="col-md-12">
			  <div class="card" id="settings-card">
				<div class="card-title">Alert Setings Main</div>
				<div class="card-body">
				  <form name="alert" id="alert-settings-form" method="post" action="<?php echo base_url('settings/create'); ?>">
					<?php if($this->session->flashdata('c_success')) {
					?>
					<div class="sufee-alert alert with-close alert-success alert-dismissible fade show">                       
					<?php echo $this->session->flashdata('c_success');  ?>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
					</div> 
					<?php } ?>                 
					
					<?php if($this->session->flashdata('c_failed')) {
					?>
					<div class="sufee-alert alert with-close alert-danger alert-dismissible fade show" data-dismiss="alert">
					<?php echo $this->session->flashdata('c_failed');  ?>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
					</div> 
					<?php } ?>   
					<?php foreach($result_set as $key=>$row): 
									  $source = $row["_source"];
									  $rkey = $row["_id"];
								
									  ?>
					<div class="table-responsive-sm">
					
						<table class="table table-bordered">
							<thead>
								<tr>
									<td>Values</th>
									<td>Min</th>
									<td>Max</th>
								</tr>
							</thead>
							<tbody>
								
								<tr>
									<th scope="col">R Phase</th>
									<td><input type="text" class="form-control col-md-8" name="rspmin" id="rspmin" value="<?php echo $source['rspmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="rspmax" id="rspmax" value="<?php echo $source['rspmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">B Phase</th>
									<td><input type="text" class="form-control col-md-8" name="bspmin" id="bspmin" value="<?php echo $source['bspmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="bspmax" id="bspmax" value="<?php echo $source['bspmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">Y Phase</th>
									<td><input type="text" class="form-control col-md-8" name="yspmin" id="yspmin" value="<?php echo $source['yspmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="yspmax" id="yspmax" value="<?php echo $source['yspmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">Total Phase</th>
									<td><input type="text" class="form-control col-md-8" name="spmin" id="spmin" value="<?php echo $source['spmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="spmax" id="spmax" value="<?php echo $source['spmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">R Three Phase</th>
									<td><input type="text" class="form-control col-md-8" name="rtpmin" id="rtpmin" value="<?php echo $source['rtpmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="rtpmax" id="rtpmax" value="<?php echo $source['rtpmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">B Three Phase</th>
									<td><input type="text" class="form-control col-md-8" name="btpmin" id="btpmin" value="<?php echo $source['btpmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="btpmax" id="btpmax" value="<?php echo $source['btpmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">Y Three Phase</th>
									<td><input type="text" class="form-control col-md-8" name="ytpmin" id="ytpmin" value="<?php echo $source['ytpmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="ytpmax" id="ytpmax" value="<?php echo $source['ytpmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">Total Three Phase</th>
									<td><input type="text" class="form-control col-md-8" name="tpmin" id="tpmin" value="<?php echo $source['tpmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="tpmax" id="tpmax" value="<?php echo $source['tpmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">R Phase Current</th>
									<td><input type="text" class="form-control col-md-8" name="rcpmin" id="rcpmin" value="<?php echo $source['rcpmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="rcpmax" id="rcpmax" value="<?php echo $source['rcpmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">B Phase Current</th>
									<td><input type="text" class="form-control col-md-8" name="bcpmin" id="bcpmin" value="<?php echo $source['bcpmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="bcpmax" id="bcpmax" value="<?php echo $source['bcpmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">Y Phase Current</th>
									<td><input type="text" class="form-control col-md-8" name="ycpmin" id="ycpmin" value="<?php echo $source['ycpmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="ycpmax" id="ycpmax" value="<?php echo $source['ycpmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">Total Current</th>
									<td><input type="text" class="form-control col-md-8" name="tcpmin" id="tcpmin" value="<?php echo $source['tcpmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="tcpmax" id="tcpmax" value="<?php echo $source['tcpmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">R Phase Power</th>
									<td><input type="text" class="form-control col-md-8" name="rppmin" id="rppmin" value="<?php echo $source['rppmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="rppmax" id="rppmax" value="<?php echo $source['rppmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">B Phase Power</th>
									<td><input type="text" class="form-control col-md-8" name="bppmin" id="bppmin" value="<?php echo $source['bppmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="bppmax" id="bppmax" value="<?php echo $source['bppmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">Y Phase Power</th>
									<td><input type="text" class="form-control col-md-8" name="yppmin" id="yppmin" value="<?php echo $source['yppmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="yppmax" id="yppmax" value="<?php echo $source['yppmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">Total Power</th>
									<td><input type="text" class="form-control col-md-8" name="tppmin" id="tppmin" value="<?php echo $source['tppmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="tppmax" id="tppmax" value="<?php echo $source['tppmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">Frequency</th>
									<td><input type="text" class="form-control col-md-8" name="fremin" id="fremin" value="<?php echo $source['fremin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="fremax" id="fremax" value="<?php echo $source['fremax']; ?>"></td>
									
								</tr>
								<tr>
								
									<th scope="col">Power Factor</th>
									<td><input type="text" class="form-control col-md-8" name="pfmin" id="pfmin" value="<?php echo $source['pfmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="pfmax" id="pfmax" value="<?php echo $source['pfmax']; ?>"></td>
								</tr>
								
							</tbody>
							
							<?php endforeach; ?>	
							
						</table>
					
						<button type="submit" class="btn btn-primary">Save</button>
						<button type="reset" class="btn btn-secondary">Reset</button>
				
					</div>        
				  </form>
				</div>
			  </div>
			</div>
			
			<div class="col-md-12">
			  <div class="card" id="settings-card">
				<div class="card-title">Alert Setings - Genset</div>
				<div class="card-body">
				  <form name="dg-alert" id="alert-settings-dg-form" method="post" action="<?php echo base_url('settings/create_genset'); ?>">
					<?php if($this->session->flashdata('c_dg_success')) {
					?>
					<div class="sufee-alert alert with-close alert-success alert-dismissible fade show">                       
					<?php echo $this->session->flashdata('c_dg_success');  ?>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
					</div> 
					<?php } ?>                 
					
					<?php if($this->session->flashdata('dg_failed')) {
					?>
					<div class="sufee-alert alert with-close alert-danger alert-dismissible fade show" data-dismiss="alert">
					<?php echo $this->session->flashdata('dg_failed');  ?>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
					</div> 
					<?php } ?>   
					<?php foreach($dgresult_set as $key=>$row): 
									  $source = $row["_source"];
									  $rkey = $row["_id"];
								
									  ?>
					<div class="table-responsive-sm">
					
						<table class="table table-bordered">
							<thead>
								<tr>
									<td>Values</th>
									<td>Min</th>
									<td>Max</th>
								</tr>
							</thead>
							<tbody>
								
								<tr>
									<th scope="col">R Single Phase</th>
									<td><input type="text" class="form-control col-md-8" name="dgrspmin" id="dgrspmin" value="<?php echo $source['dgrspmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="dgrspmax" id="dgrspmax" value="<?php echo $source['dgrspmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">B Single Phase</th>
									<td><input type="text" class="form-control col-md-8" name="dgbspmin" id="dgbspmin" value="<?php echo $source['dgbspmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="dgbspmax" id="dgbspmax" value="<?php echo $source['dgbspmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">Y Single Phase</th>
									<td><input type="text" class="form-control col-md-8" name="dgyspmin" id="dgyspmin" value="<?php echo $source['dgyspmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="dgyspmax" id="dgyspmax" value="<?php echo $source['dgyspmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">Total Phase</th>
									<td><input type="text" class="form-control col-md-8" name="dgspmin" id="dgspmin" value="<?php echo $source['dgspmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="dgspmax" id="dgspmax" value="<?php echo $source['dgspmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">R Three Phase</th>
									<td><input type="text" class="form-control col-md-8" name="dgrtpmin" id="dgrtpmin" value="<?php echo $source['dgrtpmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="dgrtpmax" id="dgrtpmax" value="<?php echo $source['dgrtpmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">B Three Phase</th>
									<td><input type="text" class="form-control col-md-8" name="dgbtpmin" id="dgbtpmin" value="<?php echo $source['dgbtpmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="dgbtpmax" id="dgbtpmax" value="<?php echo $source['dgbtpmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">Y Three Phase</th>
									<td><input type="text" class="form-control col-md-8" name="dgytpmin" id="dgytpmin" value="<?php echo $source['dgytpmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="dgytpmax" id="dgytpmax" value="<?php echo $source['dgytpmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">Total Three Phase</th>
									<td><input type="text" class="form-control col-md-8" name="dgtpmin" id="dgtpmin" value="<?php echo $source['dgtpmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="dgtpmax" id="dgtpmax" value="<?php echo $source['dgtpmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">R Phase Current</th>
									<td><input type="text" class="form-control col-md-8" name="dgrcpmin" id="dgrcpmin" value="<?php echo $source['dgrcpmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="dgrcpmax" id="dgrcpmax" value="<?php echo $source['dgrcpmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">B Phase Current</th>
									<td><input type="text" class="form-control col-md-8" name="dgbcpmin" id="dgbcpmin" value="<?php echo $source['dgbcpmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="dgbcpmax" id="dgbcpmax" value="<?php echo $source['dgbcpmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">Y Phase Current</th>
									<td><input type="text" class="form-control col-md-8" name="dgycpmin" id="dgycpmin" value="<?php echo $source['dgycpmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="dgycpmax" id="dgycpmax" value="<?php echo $source['dgycpmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">Total Phase Current</th>
									<td><input type="text" class="form-control col-md-8" name="dgtcpmin" id="dgtcpmin" value="<?php echo $source['dgtcpmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="dgtcpmax" id="dgtcpmax" value="<?php echo $source['dgtcpmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">R Phase Power</th>
									<td><input type="text" class="form-control col-md-8" name="dgrppmin" id="dgrppmin" value="<?php echo $source['dgrppmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="dgrppmax" id="dgrppmax" value="<?php echo $source['dgrppmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">B Phase Power</th>
									<td><input type="text" class="form-control col-md-8" name="dgbppmin" id="dgbppmin" value="<?php echo $source['dgbppmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="dgbppmax" id="dgbppmax" value="<?php echo $source['dgbppmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">Y Phase Power</th>
									<td><input type="text" class="form-control col-md-8" name="dgyppmin" id="dgyppmin" value="<?php echo $source['dgyppmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="dgyppmax" id="dgyppmax" value="<?php echo $source['dgyppmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">Total Phase Power</th>
									<td><input type="text" class="form-control col-md-8" name="dgtppmin" id="dgtppmin" value="<?php echo $source['dgtppmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="dgtppmax" id="dgtppmax" value="<?php echo $source['dgtppmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">Frequency</th>
									<td><input type="text" class="form-control col-md-8" name="dgfremin" id="dgfremin" value="<?php echo $source['dgfremin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="dgfremax" id="dgfremax" value="<?php echo $source['dgfremax']; ?>"></td>
									
								</tr>
								<tr>
								
									<th scope="col">Power Factor</th>
									<td><input type="text" class="form-control col-md-8" name="dgpfmin" id="dgpfmin" value="<?php echo $source['dgpfmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="dgpfmax" id="dgpfmax" value="<?php echo $source['dgpfmax']; ?>"></td>
								</tr>
								
							</tbody>
							
							<?php endforeach; ?>	
							
						</table>
					
						<button type="submit" class="btn btn-primary">Save</button>
						<button type="reset" class="btn btn-secondary">Reset</button>
				
					</div>        
				  </form>
				</div>
			  </div>
			</div>
			
			<div class="col-md-12">
			  <div class="card" id="settings-card">
				<div class="card-title">Alert Setings</div>
				<div class="card-body">
						<input type="number" id="inputunits" name="inputunits" min="1" step="1" style="width: 90px;" required />
						<input type="button" id="calculate" value="Calculate" />
						<div id="info"></div>
						
					<div class="table-responsive-sm">
						<table class="table table-bordered">
						
							<tr style="text-align: center">
								<th rowspan="2" >Units</th><th colspan="2" >Tariff Charges</th><th rowspan="2" >From</th><th rowspan="2" >To</th><th rowspan="2">Cost</th>
							</tr>
							<tr>
								<td>Fixed</td>
								<td>Subsidy</td>
							</tr>
							<tr>
								<td>&#8804; 100</td><td>&#8377; 0</td><td>&#8377; 150</td><td>1</td><td>100</td><td>&#8377; 1.50</td>
							</tr>
							<tr>
								<td>&#8804; 200</td><td>&#8377; 20</td><td>&#8377; 150</td><td>1</td><td>200</td><td>&#8377; 1.50</td>
							</tr>
							<tr>
								<td rowspan="3" >&#8804; 500</td><td rowspan="3" >&#8377; 30</td><td rowspan="3" >&#8377; 150</td><td>1</td><td>100</td><td>&#8377; 1.50</td>
							</tr>
							<tr>
								<td>101</td><td>200</td><td>&#8377; 2.00</td>
							</tr>
							<tr>
								<td>201</td><td>500</td><td>&#8377; 3.00</td>
							</tr>
							<tr>
								<td rowspan="4" >&gt; 500</td><td rowspan="4" >&#8377; 50</td><td rowspan="4" >&#8377; 150</td><td>1</td><td>100</td><td>&#8377; 1.50</td>
							</tr>
							<tr>
								<td>101</td><td>200</td><td>&#8377; 3.50</td>
							</tr>
							<tr>
								<td>201</td><td>500</td><td>&#8377; 4.60</td>
							</tr>
							<tr>
								<td colspan="2" >&gt; 500</td><td>&#8377; 6.60</td>
							</tr>
						</table>
						
					</div>        
				 
				</div>
			  </div>
			</div>
			
		</div>
                <!-- End PAge Content -->
    </div>
            
    <!-- End Page wrapper  --> 
</div>
<!-- End Wrapper -->

<div class="modal" id="notify-card" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
		<form name="notify" id="notify-form" method="post" action="<?php echo base_url('settings/add_record');?>">
			
				<div class="modal-header">
					<h5 class="modal-title" id="deviceLabel"><b>Add New</b></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
					
				</div>
				<div class="modal-body">
				<div class="modal-loader">
					<div class="form-group">
					<div class="form-group col-md-12">
					<label>Name:</label>
						<input type="text" id="name" name="name"  class="form-control" placeholder="Name"/>
					</div>
					</div>
					<div class="form-group">
					<div class="form-group col-md-12">
					<label>Phone Number:</label>
						<input type="text" id="ph_no" name="ph_no" class="form-control" placeholder="Phone Number"/>
					</div>
					</div>
					<div class="form-group">
					<div class="form-group col-md-12">
					<label>Email:</label>
					<input type="text" id="email" name="email" class="form-control" placeholder="Email Id"/>
				  </div>
					</div>
					
				</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary">Add Member</button>
					<button type="reset" class="btn btn-secondary clear">Clear</button>
				</div>
				
			</form>
		</div>
	</div>
</div>


<div class="modal" id="delete-modal" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="staticModalLabel">Delete</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <p>
               Are You Sure Want to Delete
            </p>
        </div>
        <div class="modal-footer">
        	<form method="post"  action="<?php echo base_url('settings/delete');?>">
            <input type="hidden" name="rid" id="delete_rid"/>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary">Confirm</button>
            </form>
        </div>
    </div>
</div>
</div>


<?php $this->load->view('include/footer');	?>
