  <!-- Page wrapper  -->
  <div class="page-wrapper"> 
    <!-- Bread crumb -->
    <div class="row page-titles">
      <div class="col-md-5 align-self-center">
        <h3 class="text-primary">Reports</h3>
      </div>
      <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
          <li class="breadcrumb-item active">Reports</li>
        </ol>
      </div>
    </div>
    <!-- End Bread crumb --> 
    <!-- Container fluid  -->
    <div class="container-fluid"> 
      <!-- Start Page Content -->
      <div class="row">
        <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Data Export</h4>
                                <h6 class="card-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6>
                                <div class="row">
									<div class="col-md-4">
										<div class="form-group">
											<label class="control-label">From Date</label>
											<input type="text" id="min-date" class="form-control calendar date-range-filter " />
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label class="control-label">To Date</label>
											<input type="text" id="max-date" class="form-control calendar date-range-filter"/>
										</div>
								</div> 
								<div class="table-responsive m-t-40">
								
                                    <table id="report-table" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
								<thead>
									<tr>
										<td>Genset Started</td>
										<td>Genset stopped</td>
										<td>Device Id</td>
										<td>Device Name</td>
										
										<td>Running Time <small>(HH:MM:SS)</small></td>
										
									</tr>
								</thead>
                                <tfoot>
									<tr>
										<td>Genset Started</td>
										<td>Genset stopped</td>
										<td>Device Id</td>
										<td>Device Name</td>
										
										<td>Running Time <small>(HH:MM:SS)</small></td>
									</tr>
                                </tfoot>
                                        <tbody>
										<?php //fb_pr($aggs);
											foreach($aggs as $agg):
											foreach($agg as $row):
											//fb_pr($row); exit;
											?>
											<tr>
												<td><?php echo fb_convert_jsdate($row["last_on_time"]); ?></td>
												<td><?php echo fb_convert_jsdate($row["last_off_time"]); ?></td>
												<td><?php echo $row["device_id"]; ?></td>
                                                <td><?php echo $row["device_name"]; ?></td>
												
												<td><?php echo gmdate('H:i:s',$row["rtime"]); ?></td>
                                            </tr>
										<?php endforeach; endforeach; ?>
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
        </div>
      
      
      <!-- End PAge Content --> 
    </div>
    <!-- End Container fluid  --> 
    <!-- footer --> 
    
    <!-- End footer --> 
  </div>
  <!-- End Page wrapper  --> 
</div>
<!-- End Wrapper -->
</div>
