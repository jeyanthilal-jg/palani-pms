  <!-- Page wrapper  -->
  <div class="page-wrapper"> 
    <!-- Bread crumb -->
    <div class="row page-titles">
      <div class="col-md-5 align-self-center">
        <h3 class="text-primary">Reports</h3>
      </div>
      <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
          <li class="breadcrumb-item active">Reports</li>
        </ol>
      </div>
    </div>
    <!-- End Bread crumb --> 
    <!-- Container fluid  -->
    <div class="container-fluid"> 
      <!-- Start Page Content -->
      <div class="row">
        <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Data Export</h4>
                                <h6 class="card-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6>
                                <div class="row">
									<div class="col-md-4">
										<div class="form-group">
											<label class="control-label">From Date</label>
											<input type="text" id="min-date" class="form-control calendar date-range-filter " />
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label class="control-label">To Date</label>
											<input type="text" id="max-date" class="form-control calendar date-range-filter"/>
										</div>
								</div> 
								<div class="table-responsive m-t-40">
								
                                    <table id="alert-table" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
								<thead>
									<tr>
										<td>Date - Time</td>
										<td>Alert ID</td>
										<td>Alert Type</td>
										<td>Alert Message</td>
										<td>Actions</td>
									</tr>
								</thead>
                                <tfoot>
									<tr>
										<td>Date - Time</td>
										<td>Alert ID</td>
										<td>Alert Type</td>
										<td>Alert Message</td>
										<td>Actions</td>
									</tr>
                                </tfoot>
                                        <tbody>
										<?php //fb_pr($result_set);
										foreach($result_set as $row){
											
											$source = $row['_source'];
											$rkey = $row['_id'];
											if($source['alert_state'] == "true"){
											?>
                                            <tr role="row">
												<td bgcolor="#fff"><?php echo fb_convert_jsdate($source["createdtime"]); ?></td>
												<td bgcolor="#fff"><?php echo $source["alert_id"]; ?></td>
                                                <td bgcolor="#fff"><?php echo $source["alert_type"]; ?></td>
												<td bgcolor="#fff"><?php echo $source["msg"]; ?></td>
												<td bgcolor="#fff"><a href="#" data-id="<?php echo $rkey; ?>" class="view-modal" data-toggle="modal" data-target="#view-modal"><i class="fa fa-eye"></i></a>
												<a href="#" data-id="<?=$rkey?>" class="delete-modal" data-toggle="modal" data-target="#delete-modal"><i class="fa fa-trash"></i></a>
												</td>
                                            </tr>
											<?php }
											else{ ?>
												<tr role="row">
													<td bgcolor="#f8f9fa"><?php echo fb_convert_jsdate($source["createdtime"]); ?></td>
													<td bgcolor="#f8f9fa"><?php echo $source["alert_id"]; ?></td>
													<td bgcolor="#f8f9fa"><?php echo $source["alert_type"]; ?></td>
													<td bgcolor="#f8f9fa"><?php echo $source["msg"]; ?></td>
													<td bgcolor="#f8f9fa"><a href="#" data-id="<?php echo $rkey; ?>" class="view-modal" data-toggle="modal" data-target="#view-modal"><i class="fa fa-eye"></i></a>
													<a href="#" data-id="<?=$rkey?>" class="delete-modal" data-toggle="modal" data-target="#delete-modal"><i class="fa fa-trash"></i></a>
													</td>
												</tr>
											
											<?php	}	} ?>
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
        </div>
      
      
      <!-- End PAge Content --> 
    </div>
    <!-- End Container fluid  --> 
    <!-- footer --> 
    
    <!-- End footer --> 
  </div>
  <!-- End Page wrapper  --> 
</div>
<!-- End Wrapper -->

<!-- View Modal -->
<div class="modal" id="view-modal" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="alertmodel">Alert </h5>
           
        </div>
        <div class="modal-body">
            <p><label><?php echo "Created Time"; ?>:</label> &nbsp;&nbsp; <span id="timemodel"></span></p>
            <p><label><?php echo "Alert Id"; ?>:</label>&nbsp;&nbsp; <span id="idmodel"></span></p>
			<p><label><?php echo "Alert Type"; ?>:</label>&nbsp;&nbsp; <span id="alerttypemodel"></span></p>
			<p><label><?php echo "Alert Message"; ?>:</label>&nbsp;&nbsp; <span id="alertmsgmodel"></span></p>
        </div>
        <div class="modal-footer">
             <button type="button" id="close" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
    </div>
</div>
</div>


<!-- Delete Modal -->
<div class="modal" id="delete-modal" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="staticModalLabel">Delete</h5>
            
        </div>
        <div class="modal-body">
            <p>
               <?php echo "Are You Sure Want to delete ? "; ?>
            </p>
        </div>
        <div class="modal-footer" data-id="<?php echo $rkey; ?>">
        	<form method="post"  action="<?php echo base_url('alerts/delete');?>">
            <input type="hidden" name="rid" id="delete_rid"/>
             <button type="button" id="close" class="btn btn-secondary" data-dismiss="modal">close</button>
            <button type="submit" class="btn btn-primary">Confirm</button>
            </form>
        </div>
    </div>
</div>
</div>
</div>
