  <!-- Page wrapper  -->
  <div class="page-wrapper"> 
    <!-- Bread crumb -->
    <div class="row page-titles">
      <div class="col-md-5 align-self-center">
        <h3 class="text-primary">Genset</h3>
      </div>
      <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
        <li> <button type="button" class="btn btn-primary btn-sm m-r-20" data-toggle="modal" data-target="#gensetModal">Add Genset</button></li>
          <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
          <li class="breadcrumb-item active">Genset</li>
        </ol>
      </div>
    </div>
    <!-- End Bread crumb --> 
    <!-- Container fluid  -->
    <div class="container-fluid"> 
      <!-- Start Page Content -->

        <div class="row genset-row">
                    <?php 

                        foreach($result_set as $row): 
                          $source = $row["_source"];
                          $rkey = $row["_id"];                        
                          $name = $source["name"]; 
                          $type = $source["type"];
                          $rpm = $source["rpm"];
                       ?> 
                    <div class="col-md-6">
                        <div class="card p-30">
                            <div class="media">
                                <div class="media-left meida media-middle">
                                    <img src="<?php echo base_url();?>assets/images/icons/generator.png">
                                </div>
                                <div class="media-body media-text-right">
                                    <h2><a href="<?php echo base_url(); ?>genset/genset_load/<?php echo $name;?>"><?php echo $name; ?></a></h2>
                                    <h2><?php echo $rpm; ?>Kv</h2>
                                    <span class="label label-rouded label-primary">Off</span>
                                </div>
                            </div>
                        </div>
                    </div>
            <?php endforeach; ?>     
  
                </div>
      
      <!-- End PAge Content --> 
    </div>
    <!-- End Container fluid  --> 
    <!-- footer --> 
    
    <!-- End footer --> 
  </div>
  <!-- End Page wrapper  --> 
<!-- End Wrapper -->
<!--device Form-->
<div class="modal" id="gensetModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
		<form name="genset" id="genset-form" method="post" action="<?php echo base_url('genset/add_record');?>">
			
				<div class="modal-header">
					<h5 class="modal-title" id="deviceLabel"><b>Add Genset</b></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
					
				</div>
				<div class="modal-body">
				<div class="modal-loader">
					<div class="form-group">
					<div class="form-group col-md-12">
					<label>Name :</label>
						<input type="text" id="name" name="name"  class="form-control" placeholder="Name"/>
					</div>
					</div>
                    <div class="form-group">
					<div class="form-group col-md-12">
					<label>Type :</label>
						<input type="text" id="type" name="type"  class="form-control" placeholder="Type"/>
					</div>
					</div>
                    <div class="form-group">
					<div class="form-group col-md-12">
					<label>RPM :</label>
						<input type="text" id="rpm" name="rpm"  class="form-control" placeholder="RPM"/>
					</div>
					</div>
                    <div class="form-group">
					<div class="form-group col-md-12">
					<label>Minimum Value :</label>
						<input type="text" id="minVal" name="minVal"  class="form-control" placeholder="Minimum value"/>
					</div>
					</div>
                    <div class="form-group">
					<div class="form-group col-md-12">
					<label>Maximum value :</label>
						<input type="text" id="maxVal" name="maxVal"  class="form-control" placeholder="Maximum value"/>
					</div>
					</div>   

				</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary">Add Genset</button>
					<button type="button" class="btn btn-secondary clear">Clear</button>
				</div>
		</form>
	</div>
</div>
</div>
