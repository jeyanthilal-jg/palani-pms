  <!-- header header  -->
  <?php $this->load->view('include/header');	?>
  <!-- End header header --> 
  <!-- Left Sidebar  -->
  <?php $this->load->view('include/left-sidebar');	?>
  <!-- End Left Sidebar  --> 
  <!-- Page wrapper  -->
  <div class="page-wrapper"> 
    <!-- Bread crumb -->
    <div class="row page-titles">
      <div class="col-md-5 align-self-center">
        <h3 class="text-primary">Single Phase</h3>
      </div>
      <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
          <li class="breadcrumb-item active">Single Phase</li>
        </ol>
      </div>
    </div>
    <!-- End Bread crumb --> 
    <!-- Container fluid  -->
    <div class="container-fluid"> 
      <!-- Start Page Content -->
      <div class="row">
        <div class="col-md-4">
          <div class="card bg-primary p-20">
            <div class="media widget-ten">
              <div class="media-left meida media-middle"> <span><i class="fa fa-bolt fa-3x"></i></span> </div>
              <div class="media-body media-text-right">
                <p class="m-b-0 m-l-20 media-text-left">Single Phase Voltage</p>
                <h2 class="color-white" id="total-volt-r"></h2>
                <p class="m-b-0">V</p>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card bg-pink p-20">
            <div class="media widget-ten">
              <div class="media-left meida media-middle"> <span><i class="fa fa-bolt fa-3x"></i></span> </div>
              <div class="media-body media-text-right">
                <p class="m-b-0 m-l-20 media-text-left">Current</p>
                <h2 class="color-white" id="total-current"></h2>
                <p class="m-b-0">Amps</p>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card bg-success p-20">
            <div class="media widget-ten">
              <div class="media-left meida media-middle"> <span><i class="fa fa-bolt fa-3x"></i></span> </div>
              <div class="media-body media-text-right">
                <h2 class="color-white">
                    <?php $CI =& get_instance();
                    echo $CI->formulaPower(3); ?>(KW)</h2>
                <p class="m-b-0">Power</p>
              </div>
            </div>
          </div>
        </div>          
        <div class="col-md-4">
          <div class="card bg-warning p-20">
            <div class="media widget-ten">
              <div class="media-left meida media-middle"> <span><i class="fa fa-bolt fa-3x"></i></span> </div>
              <div class="media-body media-text-right">
                <p class="m-b-0 m-l-20 media-text-left">Frequency</p>
                <h2 class="color-white" id="freq"></h2>
                <p class="m-b-0">Hz</p>
              </div>
            </div>
          </div>
        </div>                 
        <div class="col-md-4">
          <div class="card bg-danger  p-20">
            <div class="media widget-ten">
              <div class="media-left meida media-middle"> <span><i class="fa fa-bolt fa-3x"></i></span> </div>
              <div class="media-body media-text-right">
                <h2 class="color-white">
                    <?php $CI =& get_instance();
                    echo $CI->formulaKVA(3); ?></h2>
                <p class="m-b-0">KVA</p>
              </div>
            </div>
          </div>
        </div>   
        <div class="col-md-4">
          <div class="card bg-dark  p-20">
            <div class="media widget-ten">
              <div class="media-left meida media-middle"> <span><i class="fa fa-bolt fa-3x"></i></span> </div>
              <div class="media-body media-text-right">
                  <p class="m-b-0 m-l-20 media-text-left">Power Factor</p>
                <h2 class="color-white" id="powerfactor"></h2>
                <p class="m-b-0">PF</p>
              </div>
            </div>
          </div>
        </div>           
      </div>
      
      <!-- /# row -->
      
    
      <!-- End PAge Content --> 
    </div>
    <!-- End Container fluid  --> 
    <!-- footer --> 
    
    <!-- End footer --> 
  </div>
  <!-- End Page wrapper  --> 


<?php $this->load->view('include/footer');	?>
