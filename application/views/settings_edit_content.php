<!--Edit Modal -->
<div class="modal-dialog" role="document">
	<div class="modal-content">
		<form name="notify" id="notify-edit-form" method="post" action="<?php echo base_url('settings/update');?>">
			<input type="hidden" name="rkey" value="<?=$rkey?>">
				<div class="modal-header">
					<h5 class="modal-title" id="deviceLabel"><b>Edit</b></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
					
				</div>
				<div class="modal-body">
					<div class="modal-loader">
						<div class="form-group">
							<div class="form-group col-md-12">
							<label>Name:</label>
								<input type="text" id="name" name="name"  class="form-control" value="<?= $record['name']; ?>"/>
							</div>
						</div>
						<div class="form-group">
							<div class="form-group col-md-12">
							<label>Phone Number:</label>
								<input type="text" id="ph_no" name="ph_no" class="form-control" value="<?= $record['ph_no']; ?>"/>
							</div>
						</div>
						<div class="form-group">
							<div class="form-group col-md-12">
								<label>Email:</label>
								<input type="text" id="email" name="email" class="form-control" value="<?= $record['email']; ?>"/>
							</div>
						</div>
						
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary">Update</button>
					<button type="button" class="btn btn-secondary clear">Clear</button>
				</div>
				
			
		</form>
	</div>
</div>
<script>
$("#notify-edit-form").validate({
		onkeyup: false,
	   	onclick: false,
	   	onfocusout: false,
        rules: {
			name: {
                required: true
				
            },
            ph_no: {
                required: true,
				number: true
            },
            email: {
                required: true,
                email:true
            }
        },
        messages: {
			name: {
                required: "please enter Name"
            },
            ph_no: {
                required: "Please enter the Phone"
            },
            email: {
                required: "please Enter Email id",
                
            }
        }

    }); 
</script>