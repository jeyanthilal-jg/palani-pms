  <!-- Page wrapper  -->
  <div class="page-wrapper"> 
    <!-- Bread crumb -->
    <div class="row page-titles">
      <div class="col-md-5 align-self-center">
        <h3 class="text-primary">Genset</h3>
      </div>
      <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
          <li class="breadcrumb-item active">Genset 1</li>
        </ol>
      </div>
    </div>
    <!-- End Bread crumb --> 
    <!-- Container fluid  -->
    <div class="container-fluid"> 
      <!-- Start Page Content -->

        <div class="row genset-row">
                    <div class="col-md-6">
                        <div class="card p-30">
                            <div class="media">
                                <div class="media-left meida media-middle">
                                    <img src="<?php echo base_url();?>assets/images/icons/generator.png">
                                </div>
                                <div class="media-body media-text-right">
                                    <h2>Generator is</h2>
                                    <span class="label label-rouded label-primary">Off</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card p-30">
                            <div class="media">
                                <div class="media-left meida media-middle">
                                    <img src="<?php echo base_url();?>assets/images/icons/time.png">
                                </div>
                                <div class="media-body media-text-right">
                                    <h2>Runnning Hours</h2>
                                    <span class="label label-rouded label-primary">0 Hrs</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card p-30">
                            <div class="media">
                                <div class="media-left meida media-middle">
                                    <img src="<?php echo base_url();?>assets/images/icons/gas-station.png">
                                </div>
                                <div class="media-body media-text-right">
                                    <h2>Fuel consumption</h2>
                                    <span class="label label-rouded label-primary m-b-0">0 Ltr</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card p-30">
                            <div class="media">
                                <div class="media-left meida media-middle">
                                    <img src="<?php echo base_url();?>assets/images/icons/plug.png">
                                </div>
                                <div class="media-body media-text-right">
                                    <h2>Load</h2>
                                    <span class=" label label-rouded label-primary m-b-0">0%</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
      
      <!-- End PAge Content --> 
    </div>
    <!-- End Container fluid  --> 
    <!-- footer --> 
    
    <!-- End footer --> 
  </div>
  <!-- End Page wrapper  --> 
<!-- End Wrapper -->
