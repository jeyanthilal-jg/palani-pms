  <!-- Page wrapper  -->
  <div class="page-wrapper"> 
    <!-- Bread crumb -->
    <div class="row page-titles">
      <div class="col-md-5 align-self-center">
        <h3 class="text-primary">Manage DG</h3>
      </div>
      <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
          <li class="breadcrumb-item active">Manage DG</li>
        </ol>
      </div>
    </div>
    <!-- End Bread crumb --> 
    <!-- Container fluid  -->
    <div class="container-fluid"> 
      <!-- Start Page Content -->
      <?php
       if($this->session->flashdata('delete_success')) {   ?>
      <div class="alert alert-success alert-dismissible fade show text-center">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
      <strong>Great !</strong> Meter has been deleted succesfully
      </div>
      <?php } if ($this->session->flashdata('delete_failed')) { ?>
        <div class="alert alert-danger alert-delete_failed fade show text-center">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
      <strong>oops !</strong> There is something wrong, try again
      </div>
      <?php }    ?>
      <?php if($this->session->flashdata('update_success')) {   ?>
      <div class="alert alert-success alert-dismissible fade show text-center">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
      <strong>Great !</strong> Meter has been updated succesfully
      </div>
      <?php } if ($this->session->flashdata('update_failed')) { ?>
        <div class="alert alert-danger alert-delete_failed fade show text-center">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
      <strong>oops !</strong> There is something wrong, try again
      </div>
      <?php }    ?> 
      <?php if($this->session->flashdata('meter_success')) {   ?>
      <div class="alert alert-success alert-dismissible fade show text-center">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
      <strong>Great !</strong> Meter has been added succesfully
      </div>
      <?php } if ($this->session->flashdata('meter_failed')) { ?>
        <div class="alert alert-danger alert-dismissible fade show text-center">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
      <strong>oops !</strong> There is something wrong, try again
      </div>
      <?php }    ?>      
      <div class="row">
        <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <button type="button" class="btn btn-primary btn-sm m-r-20 pull-right" data-toggle="modal" data-target="#meter-modal">Add DG</button>                            	
                                <h4 class="card-title">Data Export</h4>
                                <h6 class="card-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6>

                                <div class="row">

								<div class="table-responsive m-t-40">
								
                                    <table id="meters-table" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
								<thead>
									<tr>
										<td>DG ID</td>
										<td>Name</td>
										<td>KVA</td>
										<td>Description</td>
										<td>Image</td>
										<td>Status </td>
										<td>Action</td>
									</tr>
								</thead>
                                <tfoot>
									<tr>
										<td>DG ID</td>
										<td>Name</td>
										<td>KVA</td>
										<td>Description</td>
										<td>Image</td>
										<td>Status </td>
										<td>Action</td>
									</tr>
                                </tfoot>
                                        <tbody>
										<?php //fb_pr($presult_data);
										if(count($presult_data)>0){
										foreach($presult_data as $row){
											$source = $row['_source'];
											$source = $row['_source'];
											$rkey = $row['_id'];
											$status= $source["status"]
											?>
                                            <tr>
												<td><?php echo $source["meter_id"]; ?></td>
                                                <td><?php echo $source["name"]; ?></td>
                                                <td><?php echo $source["kva"]; ?></td>
												<td><?php echo $source["description"]; ?></td>
												<td><img height="100px" width="100px" src="<?php echo base_url().'/meter-images/'.$source["image"]; ?>"></td>
												<td><?php 
												if($source["status"]=="true")
												{
													echo "Active";
												}else{
													echo "In active";
												} ?></td>
												<td>
												<?php 
												if($source["status"]=="true")
												{
													echo "Active";
												?>
													<a href="<?php echo base_url().'meters/updateStatus/'.$rkey.'/false';?>" title="De-activate" data-id="<?php echo $rkey; ?>"><i 
														class="fa fa-eye"></i></a>
												<?php } else { ?>
													<a href="<?php echo base_url().'meters/updateStatus/'.$rkey.'/true';?>" title="Activate" data-id="<?php echo $rkey; ?>">
														<i class="fa fa-eye-slash"
														></i></a>
													<?php } ?>													
														<a href="#" data-id="<?php echo $rkey; ?>" class="delete-modal" data-toggle="modal" data-target="#delete-modal"><i class="fa fa-trash"></i></a>
														<a href="#" data-id="<?php echo $rkey; ?>" class="edit-meter" data-toggle="modal" data-target="#edit-modal"><i class="fa fa-edit"></i></a>														
												</td>
                                            </tr>
										<?php  } } else{?>
											<tr>
											<td></td>
											<td></td>
											<td></td>
											<td>No Record found</td>
											<td></td>
											<td></td>
											<td></td>
											</tr>
                                         <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
        </div>
      
      
      <!-- End PAge Content --> 
    </div>
    <!-- End Container fluid  --> 
    <!-- footer --> 
    
    <!-- End footer --> 
  </div>
  <!-- End Page wrapper  --> 
</div>
<!-- End Wrapper -->
<!-- Delete Modal -->
<div class="modal" id="delete-modal" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="staticModalLabel">Delete</h5>
            
        </div>
        <div class="modal-body">
            <p>
               <?php echo "Are You Sure Want to delete ? "; ?>
            </p>
        </div>
        <div class="modal-footer" data-id="<?php echo $rkey; ?>">
        	<form method="post"  action="<?php echo base_url('meters/delete');?>">
            <input type="hidden" name="rid" id="delete_rid"/>
             <button type="button" id="close" class="btn btn-secondary" data-dismiss="modal">close</button>
            <button type="submit" class="btn btn-primary">Confirm</button>
            </form>
        </div>
    </div>
</div>
</div>

<!--Meter Form modal-->
<div class="modal" id="meter-modal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    <form name="device" id="meter-form" method="post" enctype="multipart/form-data" action="<?php echo base_url('meters/add_meter');?>">
      
        <div class="modal-header">
          <h5 class="modal-title" id="deviceLabel"><b>Add DG</b></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
          
        </div>
        <div class="modal-body">
        <div class="modal-loader">
          <div class="form-group">
          <div class="form-group col-md-12">
          <label>DG Id:</label>
            <input type="text" id="meter_id" name="meter_id"  class="form-control" placeholder="first letter of DG and unique number"/>
          </div>
          </div>
          <div class="form-group">
          <div class="form-group col-md-12">
          <label>DG Name:</label>
            <input type="text" id="meter_name" name="meter_name" class="form-control" placeholder="Name of the DG"/>
          </div>
          </div>
          <div class="form-group">
          <div class="form-group col-md-12">
          <label>KVA:</label>
            <input type="text" id="meter_kva" name="kva" class="form-control" placeholder="KVA of the DG"/>
          </div>
          </div>        
          <div class="form-group">
          <div class="form-group col-md-12">
          <label>Description:</label>  
          <input type="text" id="meter_desc" name="meter_desc" class="form-control" placeholder="Description">        
          
          </div>
          </div>          
          <div class="form-group">
          <div class="form-group col-md-12">
          <label>Image:</label>
          <input type="file" name="meter_image" id="meter_imagefile" class="form-control">
          <div id="meter_image"></div>
          </div>
          </div>
          
        </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary title-btn">Add DG</button>
          <button type="button" class="btn btn-secondary clear">Clear</button>
        </div>
      </form>
      </div>

  </div>
</div>
</div>
