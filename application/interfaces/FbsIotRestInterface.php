<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

interface FbsIotRestInterface {
    public function login($email, $password);
    public function isloggedin();
    public function get_fbuser_data($name);
    public function create_record($table_name = "", $idata = array(), $activity = false);
    public function test_list_record($table_name = "");
    public function list_record($params = array());
    public function combo_list($table_name = "");
    public function delete_record($table_name = "", $rkey = "");
    public function update_record($table_name = "", $idata = array(), $rkey = '', $activity = false);
    public function get_record($table_name = "", $rkey = "");
    public function check_duplicate($table_name = "", $fld_name = "", $search = "", $rkey = "");
    public function update_others($table_name = '',$orecord = array(),$nrecord = array(), $rkey = '');
    public function search_list($table_name, $search);
   
}