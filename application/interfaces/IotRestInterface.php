<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

interface IotRestInterface {
    public function login($email, $password);
    public function isloggedin();
    public function get_fbuser_data($name);
}