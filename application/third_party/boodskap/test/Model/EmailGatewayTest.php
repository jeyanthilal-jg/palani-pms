<?php
/**
 * EmailGatewayTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * User API
 *
 * Boodskap IoT Platform (User API)
 *
 * OpenAPI spec version: 1.0.6
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.3.1
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Swagger\Client;

/**
 * EmailGatewayTest Class Doc Comment
 *
 * @category    Class */
// * @description EmailGateway
/**
 * @package     Swagger\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class EmailGatewayTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "EmailGateway"
     */
    public function testEmailGateway()
    {
    }

    /**
     * Test attribute "host"
     */
    public function testPropertyHost()
    {
    }

    /**
     * Test attribute "port"
     */
    public function testPropertyPort()
    {
    }

    /**
     * Test attribute "user"
     */
    public function testPropertyUser()
    {
    }

    /**
     * Test attribute "password"
     */
    public function testPropertyPassword()
    {
    }

    /**
     * Test attribute "primary_email"
     */
    public function testPropertyPrimaryEmail()
    {
    }

    /**
     * Test attribute "bounce_email"
     */
    public function testPropertyBounceEmail()
    {
    }

    /**
     * Test attribute "ssl"
     */
    public function testPropertySsl()
    {
    }

    /**
     * Test attribute "tls"
     */
    public function testPropertyTls()
    {
    }

    /**
     * Test attribute "debug"
     */
    public function testPropertyDebug()
    {
    }
}
