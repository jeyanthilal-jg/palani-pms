# RecordDefinition

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**domain_key** | **string** |  | [optional] 
**id** | **int** |  | 
**name** | **string** |  | 
**description** | **string** |  | [optional] 
**fields** | [**\Swagger\Client\Model\RecordField[]**](RecordField.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


