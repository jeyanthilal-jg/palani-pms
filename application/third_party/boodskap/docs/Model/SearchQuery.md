# SearchQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**extra_path** | **string** |  | [optional] 
**query** | **string** | ElasticSearch Query String | 
**params** | [**\Swagger\Client\Model\Param[]**](Param.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


