# UDPGateway

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**port** | **int** |  | 
**threads** | **int** |  | 
**decoder_code** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


