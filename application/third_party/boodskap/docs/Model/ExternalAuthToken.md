# ExternalAuthToken

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**domain_key** | **string** |  | 
**api_key** | **string** |  | 
**user_id** | **string** |  | 
**token** | **string** |  | 
**expire_at** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


