# Widget

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**domain_key** | **string** |  | [optional] 
**client_domain_key** | **string** |  | [optional] 
**category** | **string** |  | [optional] 
**tags** | **string** |  | [optional] 
**widgetid** | **string** |  | [optional] 
**widgetname** | **string** |  | [optional] 
**widgetimage** | **string** |  | [optional] 
**widgetscreens** | **string[]** |  | [optional] 
**version** | **string** |  | [optional] 
**enabled** | **bool** |  | [optional] 
**published** | **bool** |  | [optional] 
**code** | **string** |  | [optional] 
**createdby** | **string** |  | [optional] 
**createdbyemail** | **string** |  | [optional] 
**createdtime** | **int** |  | [optional] 
**updatedtime** | **int** |  | [optional] 
**importedtime** | **int** |  | [optional] 
**config** | **string** |  | [optional] 
**description** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


