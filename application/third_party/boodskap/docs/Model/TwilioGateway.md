# TwilioGateway

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sid** | **string** |  | 
**token** | **string** |  | 
**primary_phone** | **string** |  | 
**debug** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


