# Location

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**domain_key** | **string** |  | [optional] 
**name** | **string** |  | 
**at** | **string** |  | [optional] 
**lat** | **double** |  | [optional] 
**lon** | **double** |  | [optional] 
**pin** | **string** |  | 
**location** | **string** |  | [optional] 
**category1** | **string** |  | [optional] 
**category2** | **string** |  | [optional] 
**category3** | **string** |  | [optional] 
**category4** | **string** |  | [optional] 
**category5** | **string** |  | [optional] 
**category6** | **string** |  | [optional] 
**category7** | **string** |  | [optional] 
**category8** | **string** |  | [optional] 
**category9** | **string** |  | [optional] 
**category10** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


