# Notification

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**domain_key** | **string** |  | [optional] 
**id** | **string** |  | [optional] 
**created** | **int** |  | [optional] 
**author** | **string** |  | [optional] 
**session** | **string** |  | [optional] 
**entity** | **string** |  | 
**severity** | **int** |  | 
**message** | **string** |  | 
**data** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


