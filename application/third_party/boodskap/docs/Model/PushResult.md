# PushResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**muid** | **string** | message unique identifier | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


