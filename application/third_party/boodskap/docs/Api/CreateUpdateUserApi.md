# Swagger\Client\CreateUpdateUserApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**upsertUser**](CreateUpdateUserApi.md#upsertUser) | **POST** /user/upsert/{atoken} | Create or update user


# **upsertUser**
> \Swagger\Client\Model\Success upsertUser($atoken, $user)

Create or update user

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CreateUpdateUserApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$atoken = "atoken_example"; // string | Auth token of the logged in user
$user = new \Swagger\Client\Model\User(); // \Swagger\Client\Model\User | User object

try {
    $result = $apiInstance->upsertUser($atoken, $user);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CreateUpdateUserApi->upsertUser: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **atoken** | **string**| Auth token of the logged in user |
 **user** | [**\Swagger\Client\Model\User**](../Model/User.md)| User object |

### Return type

[**\Swagger\Client\Model\Success**](../Model/Success.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

