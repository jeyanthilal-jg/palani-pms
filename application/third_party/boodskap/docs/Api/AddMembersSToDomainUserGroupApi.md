# Swagger\Client\AddMembersSToDomainUserGroupApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addMembersToDomainUserGroup**](AddMembersSToDomainUserGroupApi.md#addMembersToDomainUserGroup) | **POST** /domain/user/group/add/{atoken}/{gid} | Add Members(s) to Doain User Group


# **addMembersToDomainUserGroup**
> \Swagger\Client\Model\Success addMembersToDomainUserGroup($atoken, $gid, $user_ids)

Add Members(s) to Doain User Group

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\AddMembersSToDomainUserGroupApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$atoken = "atoken_example"; // string | Auth token of the logged in user
$gid = 56; // int | Group Id
$user_ids = array(new \Swagger\Client\Model\string[]()); // string[] | Array of user IDs

try {
    $result = $apiInstance->addMembersToDomainUserGroup($atoken, $gid, $user_ids);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AddMembersSToDomainUserGroupApi->addMembersToDomainUserGroup: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **atoken** | **string**| Auth token of the logged in user |
 **gid** | **int**| Group Id |
 **user_ids** | **string[]**| Array of user IDs |

### Return type

[**\Swagger\Client\Model\Success**](../Model/Success.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

