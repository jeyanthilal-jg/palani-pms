# Swagger\Client\CreateUpdateUserGroupPropertyApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**storeUserGroupProperty**](CreateUpdateUserGroupPropertyApi.md#storeUserGroupProperty) | **POST** /user/group/property/upsert/{atoken} | Create / Update User Group Property


# **storeUserGroupProperty**
> \Swagger\Client\Model\Success storeUserGroupProperty($atoken, $entity)

Create / Update User Group Property

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CreateUpdateUserGroupPropertyApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$atoken = "atoken_example"; // string | Auth token of the logged in user
$entity = new \Swagger\Client\Model\UserGroupProperty(); // \Swagger\Client\Model\UserGroupProperty | UserGroupProperty JSON object

try {
    $result = $apiInstance->storeUserGroupProperty($atoken, $entity);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CreateUpdateUserGroupPropertyApi->storeUserGroupProperty: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **atoken** | **string**| Auth token of the logged in user |
 **entity** | [**\Swagger\Client\Model\UserGroupProperty**](../Model/UserGroupProperty.md)| UserGroupProperty JSON object |

### Return type

[**\Swagger\Client\Model\Success**](../Model/Success.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

