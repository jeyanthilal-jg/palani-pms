# Swagger\Client\CreateUpdateUserPropertyApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**storeUserProperty**](CreateUpdateUserPropertyApi.md#storeUserProperty) | **POST** /user/property/upsert/{atoken} | Create / Update User Property


# **storeUserProperty**
> \Swagger\Client\Model\Success storeUserProperty($atoken, $entity)

Create / Update User Property

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CreateUpdateUserPropertyApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$atoken = "atoken_example"; // string | Auth token of the logged in user
$entity = new \Swagger\Client\Model\UserProperty(); // \Swagger\Client\Model\UserProperty | UserProperty JSON object

try {
    $result = $apiInstance->storeUserProperty($atoken, $entity);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CreateUpdateUserPropertyApi->storeUserProperty: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **atoken** | **string**| Auth token of the logged in user |
 **entity** | [**\Swagger\Client\Model\UserProperty**](../Model/UserProperty.md)| UserProperty JSON object |

### Return type

[**\Swagger\Client\Model\Success**](../Model/Success.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

