# Swagger\Client\DeleteStoredDomainUserGroupPropertyApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteDomainUserGroupProperty**](DeleteStoredDomainUserGroupPropertyApi.md#deleteDomainUserGroupProperty) | **DELETE** /domain/user/group/property/delete/{atoken}/{gid}/{name} | Delete Stored Domain User Group Property


# **deleteDomainUserGroupProperty**
> \Swagger\Client\Model\Success deleteDomainUserGroupProperty($atoken, $gid, $name)

Delete Stored Domain User Group Property

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DeleteStoredDomainUserGroupPropertyApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$atoken = "atoken_example"; // string | Auth token of the logged in domain
$gid = 56; // int | 
$name = "name_example"; // string | 

try {
    $result = $apiInstance->deleteDomainUserGroupProperty($atoken, $gid, $name);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeleteStoredDomainUserGroupPropertyApi->deleteDomainUserGroupProperty: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **atoken** | **string**| Auth token of the logged in domain |
 **gid** | **int**|  |
 **name** | **string**|  |

### Return type

[**\Swagger\Client\Model\Success**](../Model/Success.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

