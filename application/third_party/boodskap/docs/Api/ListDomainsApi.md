# Swagger\Client\ListDomainsApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**callList**](ListDomainsApi.md#callList) | **GET** /domain/list/{atoken}/{pageSize} | List Domains


# **callList**
> string[] callList($atoken, $page_size, $direction, $dkey)

List Domains

Returns all domain keys from the system that the user has access to

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ListDomainsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$atoken = "atoken_example"; // string | Auth token of the logged in user
$page_size = 56; // int | Maximum number of domains to be listed
$direction = "direction_example"; // string | If direction is specified, **dkey** is required
$dkey = "dkey_example"; // string | Last or First domain key of the previous list operation, **required** if **direction** is specified

try {
    $result = $apiInstance->callList($atoken, $page_size, $direction, $dkey);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ListDomainsApi->callList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **atoken** | **string**| Auth token of the logged in user |
 **page_size** | **int**| Maximum number of domains to be listed |
 **direction** | **string**| If direction is specified, **dkey** is required | [optional]
 **dkey** | **string**| Last or First domain key of the previous list operation, **required** if **direction** is specified | [optional]

### Return type

**string[]**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

