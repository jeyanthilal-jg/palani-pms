# Swagger\Client\UnlinkADomainApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**unlinkDomain**](UnlinkADomainApi.md#unlinkDomain) | **GET** /domain/unlink/{atoken} | Unlink a Domain


# **unlinkDomain**
> \Swagger\Client\Model\Success unlinkDomain($atoken, $domain_key)

Unlink a Domain

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\UnlinkADomainApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$atoken = "atoken_example"; // string | 
$domain_key = "domain_key_example"; // string | Domain key

try {
    $result = $apiInstance->unlinkDomain($atoken, $domain_key);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UnlinkADomainApi->unlinkDomain: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **atoken** | **string**|  |
 **domain_key** | **string**| Domain key |

### Return type

[**\Swagger\Client\Model\Success**](../Model/Success.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

