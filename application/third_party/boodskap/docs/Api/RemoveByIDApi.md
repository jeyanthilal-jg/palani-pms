# Swagger\Client\RemoveByIDApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**removeByID**](RemoveByIDApi.md#removeByID) | **GET** /elastic/remove/{atoken}/{type}/{entityId} | Remove By ID


# **removeByID**
> \Swagger\Client\Model\SearchResult removeByID($atoken, $type, $entity_id)

Remove By ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\RemoveByIDApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$atoken = "atoken_example"; // string | Auth token of the logged in user
$type = "type_example"; // string | 
$entity_id = "entity_id_example"; // string | 

try {
    $result = $apiInstance->removeByID($atoken, $type, $entity_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RemoveByIDApi->removeByID: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **atoken** | **string**| Auth token of the logged in user |
 **type** | **string**|  |
 **entity_id** | **string**|  |

### Return type

[**\Swagger\Client\Model\SearchResult**](../Model/SearchResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

