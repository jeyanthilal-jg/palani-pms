# Swagger\Client\LinkAnotherDomainApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**linkDomain**](LinkAnotherDomainApi.md#linkDomain) | **GET** /domain/link/{atoken} | Link Another Domain


# **linkDomain**
> \Swagger\Client\Model\LinkedDomain linkDomain($atoken, $domain_key, $api_key, $label)

Link Another Domain

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\LinkAnotherDomainApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$atoken = "atoken_example"; // string | 
$domain_key = "domain_key_example"; // string | Domain key
$api_key = "api_key_example"; // string | Domain API key
$label = "label_example"; // string | Menu Label

try {
    $result = $apiInstance->linkDomain($atoken, $domain_key, $api_key, $label);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling LinkAnotherDomainApi->linkDomain: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **atoken** | **string**|  |
 **domain_key** | **string**| Domain key |
 **api_key** | **string**| Domain API key |
 **label** | **string**| Menu Label |

### Return type

[**\Swagger\Client\Model\LinkedDomain**](../Model/LinkedDomain.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

