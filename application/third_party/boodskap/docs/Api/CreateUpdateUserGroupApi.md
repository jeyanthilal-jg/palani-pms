# Swagger\Client\CreateUpdateUserGroupApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**upsertUserGroup**](CreateUpdateUserGroupApi.md#upsertUserGroup) | **POST** /user/group/upsert/{atoken} | Create / Update User Group


# **upsertUserGroup**
> \Swagger\Client\Model\Success upsertUserGroup($atoken, $entity)

Create / Update User Group

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CreateUpdateUserGroupApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$atoken = "atoken_example"; // string | Auth token of the logged in user
$entity = new \Swagger\Client\Model\UserGroup(); // \Swagger\Client\Model\UserGroup | UserGroup JSON object

try {
    $result = $apiInstance->upsertUserGroup($atoken, $entity);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CreateUpdateUserGroupApi->upsertUserGroup: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **atoken** | **string**| Auth token of the logged in user |
 **entity** | [**\Swagger\Client\Model\UserGroup**](../Model/UserGroup.md)| UserGroup JSON object |

### Return type

[**\Swagger\Client\Model\Success**](../Model/Success.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

