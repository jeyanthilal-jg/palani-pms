# Swagger\Client\DeleteStoredUserPropertyApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteUserProperty**](DeleteStoredUserPropertyApi.md#deleteUserProperty) | **DELETE** /user/property/delete/{atoken}/{ouid}/{name} | Delete Stored User Property


# **deleteUserProperty**
> \Swagger\Client\Model\Success deleteUserProperty($atoken, $ouid, $name)

Delete Stored User Property

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DeleteStoredUserPropertyApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$atoken = "atoken_example"; // string | Auth token of the logged in user
$ouid = "ouid_example"; // string | Owner User ID
$name = "name_example"; // string | 

try {
    $result = $apiInstance->deleteUserProperty($atoken, $ouid, $name);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeleteStoredUserPropertyApi->deleteUserProperty: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **atoken** | **string**| Auth token of the logged in user |
 **ouid** | **string**| Owner User ID |
 **name** | **string**|  |

### Return type

[**\Swagger\Client\Model\Success**](../Model/Success.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

