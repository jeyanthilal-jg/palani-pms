# Swagger\Client\UploadFileAsApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**uploadFileAs**](UploadFileAsApi.md#uploadFileAs) | **POST** /files/upload/as/{atoken} | Upload File As


# **uploadFileAs**
> \Swagger\Client\Model\IDResult uploadFileAs($atoken, $content)

Upload File As

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\UploadFileAsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$atoken = "atoken_example"; // string | Auth token of the logged in user
$content = new \Swagger\Client\Model\FileContent(); // \Swagger\Client\Model\FileContent | FileContent JSON object

try {
    $result = $apiInstance->uploadFileAs($atoken, $content);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UploadFileAsApi->uploadFileAs: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **atoken** | **string**| Auth token of the logged in user |
 **content** | [**\Swagger\Client\Model\FileContent**](../Model/FileContent.md)| FileContent JSON object |

### Return type

[**\Swagger\Client\Model\IDResult**](../Model/IDResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

