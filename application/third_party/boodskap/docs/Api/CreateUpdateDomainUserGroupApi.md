# Swagger\Client\CreateUpdateDomainUserGroupApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**upsertDomainUserGroup**](CreateUpdateDomainUserGroupApi.md#upsertDomainUserGroup) | **POST** /domain/user/group/upsert/{atoken} | Create / Update Domain User Group


# **upsertDomainUserGroup**
> \Swagger\Client\Model\Success upsertDomainUserGroup($atoken, $entity)

Create / Update Domain User Group

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CreateUpdateDomainUserGroupApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$atoken = "atoken_example"; // string | Auth token of the logged in user
$entity = new \Swagger\Client\Model\DomainUserGroup(); // \Swagger\Client\Model\DomainUserGroup | DomainUserGroup JSON object

try {
    $result = $apiInstance->upsertDomainUserGroup($atoken, $entity);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CreateUpdateDomainUserGroupApi->upsertDomainUserGroup: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **atoken** | **string**| Auth token of the logged in user |
 **entity** | [**\Swagger\Client\Model\DomainUserGroup**](../Model/DomainUserGroup.md)| DomainUserGroup JSON object |

### Return type

[**\Swagger\Client\Model\Success**](../Model/Success.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

