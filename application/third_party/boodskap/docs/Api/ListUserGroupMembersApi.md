# Swagger\Client\ListUserGroupMembersApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**listUserGroupMembers**](ListUserGroupMembersApi.md#listUserGroupMembers) | **GET** /user/group/listmembers/{atoken}/{ouid}/{gid}/{pageSize} | List User Group Members


# **listUserGroupMembers**
> \Swagger\Client\Model\User[] listUserGroupMembers($atoken, $ouid, $gid, $page_size, $direction, $uid)

List User Group Members

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ListUserGroupMembersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$atoken = "atoken_example"; // string | Auth token of the logged in user
$ouid = "ouid_example"; // string | Owner User ID
$gid = 56; // int | Group id to list the devices from
$page_size = 56; // int | Maximum number of devices to be listed
$direction = "direction_example"; // string | If direction is specified, **uid** is required
$uid = "uid_example"; // string | Last or First user id of the previous list operation, **required** if **direction** is specified

try {
    $result = $apiInstance->listUserGroupMembers($atoken, $ouid, $gid, $page_size, $direction, $uid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ListUserGroupMembersApi->listUserGroupMembers: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **atoken** | **string**| Auth token of the logged in user |
 **ouid** | **string**| Owner User ID |
 **gid** | **int**| Group id to list the devices from |
 **page_size** | **int**| Maximum number of devices to be listed |
 **direction** | **string**| If direction is specified, **uid** is required | [optional]
 **uid** | **string**| Last or First user id of the previous list operation, **required** if **direction** is specified | [optional]

### Return type

[**\Swagger\Client\Model\User[]**](../Model/User.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

