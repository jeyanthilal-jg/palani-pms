# Swagger\Client\CreateUpdateDomainAssetGroupApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**upsertDomainAssetGroup**](CreateUpdateDomainAssetGroupApi.md#upsertDomainAssetGroup) | **POST** /domain/asset/group/upsert/{atoken} | Create / Update Domain Asset Group


# **upsertDomainAssetGroup**
> \Swagger\Client\Model\Success upsertDomainAssetGroup($atoken, $entity)

Create / Update Domain Asset Group

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CreateUpdateDomainAssetGroupApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$atoken = "atoken_example"; // string | Auth token of the logged in user
$entity = new \Swagger\Client\Model\DomainAssetGroup(); // \Swagger\Client\Model\DomainAssetGroup | DomainAssetGroup JSON object

try {
    $result = $apiInstance->upsertDomainAssetGroup($atoken, $entity);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CreateUpdateDomainAssetGroupApi->upsertDomainAssetGroup: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **atoken** | **string**| Auth token of the logged in user |
 **entity** | [**\Swagger\Client\Model\DomainAssetGroup**](../Model/DomainAssetGroup.md)| DomainAssetGroup JSON object |

### Return type

[**\Swagger\Client\Model\Success**](../Model/Success.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

