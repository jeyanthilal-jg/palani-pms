# Swagger\Client\RetreiveDomainUserGroupPropertyApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getDomainUserGroupProperty**](RetreiveDomainUserGroupPropertyApi.md#getDomainUserGroupProperty) | **GET** /domain/user/group/property/get/{atoken}/{gid}/{name} | Retreive Domain User Group Property


# **getDomainUserGroupProperty**
> \Swagger\Client\Model\DomainUserGroupProperty getDomainUserGroupProperty($atoken, $gid, $name)

Retreive Domain User Group Property

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\RetreiveDomainUserGroupPropertyApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$atoken = "atoken_example"; // string | Auth token of the logged in domain
$gid = 56; // int | Group ID
$name = "name_example"; // string | 

try {
    $result = $apiInstance->getDomainUserGroupProperty($atoken, $gid, $name);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RetreiveDomainUserGroupPropertyApi->getDomainUserGroupProperty: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **atoken** | **string**| Auth token of the logged in domain |
 **gid** | **int**| Group ID |
 **name** | **string**|  |

### Return type

[**\Swagger\Client\Model\DomainUserGroupProperty**](../Model/DomainUserGroupProperty.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

