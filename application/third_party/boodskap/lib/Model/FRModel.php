<?php
/**
 * FRModel
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Boodskap API
 *
 * Boodskap IoT Platform API
 *
 * OpenAPI spec version: 2.0.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.3.1
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Swagger\Client\Model;

use \ArrayAccess;
use \Swagger\Client\ObjectSerializer;

/**
 * FRModel Class Doc Comment
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class FRModel implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $swaggerModelName = 'FRModel';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = [
        'name' => 'string',
        'algorithm' => 'string',
        'image_width' => 'int',
        'image_height' => 'int',
        'train_every' => 'int',
        'created_at' => 'int',
        'trained_at' => 'int'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerFormats = [
        'name' => null,
        'algorithm' => null,
        'image_width' => 'int32',
        'image_height' => 'int32',
        'train_every' => 'int32',
        'created_at' => 'int64',
        'trained_at' => 'int64'
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'name' => 'name',
        'algorithm' => 'algorithm',
        'image_width' => 'imageWidth',
        'image_height' => 'imageHeight',
        'train_every' => 'trainEvery',
        'created_at' => 'createdAt',
        'trained_at' => 'trainedAt'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'name' => 'setName',
        'algorithm' => 'setAlgorithm',
        'image_width' => 'setImageWidth',
        'image_height' => 'setImageHeight',
        'train_every' => 'setTrainEvery',
        'created_at' => 'setCreatedAt',
        'trained_at' => 'setTrainedAt'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'name' => 'getName',
        'algorithm' => 'getAlgorithm',
        'image_width' => 'getImageWidth',
        'image_height' => 'getImageHeight',
        'train_every' => 'getTrainEvery',
        'created_at' => 'getCreatedAt',
        'trained_at' => 'getTrainedAt'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    const ALGORITHM_EIGEN = 'EIGEN';
    const ALGORITHM_FISHER = 'FISHER';
    const ALGORITHM_LBPH = 'LBPH';
    

    
    /**
     * Gets allowable values of the enum
     *
     * @return string[]
     */
    public function getAlgorithmAllowableValues()
    {
        return [
            self::ALGORITHM_EIGEN,
            self::ALGORITHM_FISHER,
            self::ALGORITHM_LBPH,
        ];
    }
    

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['name'] = isset($data['name']) ? $data['name'] : null;
        $this->container['algorithm'] = isset($data['algorithm']) ? $data['algorithm'] : null;
        $this->container['image_width'] = isset($data['image_width']) ? $data['image_width'] : null;
        $this->container['image_height'] = isset($data['image_height']) ? $data['image_height'] : null;
        $this->container['train_every'] = isset($data['train_every']) ? $data['train_every'] : null;
        $this->container['created_at'] = isset($data['created_at']) ? $data['created_at'] : null;
        $this->container['trained_at'] = isset($data['trained_at']) ? $data['trained_at'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        if ($this->container['name'] === null) {
            $invalidProperties[] = "'name' can't be null";
        }
        if ($this->container['algorithm'] === null) {
            $invalidProperties[] = "'algorithm' can't be null";
        }
        $allowedValues = $this->getAlgorithmAllowableValues();
        if (!in_array($this->container['algorithm'], $allowedValues)) {
            $invalidProperties[] = sprintf(
                "invalid value for 'algorithm', must be one of '%s'",
                implode("', '", $allowedValues)
            );
        }

        if ($this->container['image_width'] === null) {
            $invalidProperties[] = "'image_width' can't be null";
        }
        if ($this->container['image_height'] === null) {
            $invalidProperties[] = "'image_height' can't be null";
        }
        if ($this->container['train_every'] === null) {
            $invalidProperties[] = "'train_every' can't be null";
        }
        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {

        if ($this->container['name'] === null) {
            return false;
        }
        if ($this->container['algorithm'] === null) {
            return false;
        }
        $allowedValues = $this->getAlgorithmAllowableValues();
        if (!in_array($this->container['algorithm'], $allowedValues)) {
            return false;
        }
        if ($this->container['image_width'] === null) {
            return false;
        }
        if ($this->container['image_height'] === null) {
            return false;
        }
        if ($this->container['train_every'] === null) {
            return false;
        }
        return true;
    }


    /**
     * Gets name
     *
     * @return string
     */
    public function getName()
    {
        return $this->container['name'];
    }

    /**
     * Sets name
     *
     * @param string $name name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->container['name'] = $name;

        return $this;
    }

    /**
     * Gets algorithm
     *
     * @return string
     */
    public function getAlgorithm()
    {
        return $this->container['algorithm'];
    }

    /**
     * Sets algorithm
     *
     * @param string $algorithm algorithm
     *
     * @return $this
     */
    public function setAlgorithm($algorithm)
    {
        $allowedValues = $this->getAlgorithmAllowableValues();
        if (!in_array($algorithm, $allowedValues)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid value for 'algorithm', must be one of '%s'",
                    implode("', '", $allowedValues)
                )
            );
        }
        $this->container['algorithm'] = $algorithm;

        return $this;
    }

    /**
     * Gets image_width
     *
     * @return int
     */
    public function getImageWidth()
    {
        return $this->container['image_width'];
    }

    /**
     * Sets image_width
     *
     * @param int $image_width image_width
     *
     * @return $this
     */
    public function setImageWidth($image_width)
    {
        $this->container['image_width'] = $image_width;

        return $this;
    }

    /**
     * Gets image_height
     *
     * @return int
     */
    public function getImageHeight()
    {
        return $this->container['image_height'];
    }

    /**
     * Sets image_height
     *
     * @param int $image_height image_height
     *
     * @return $this
     */
    public function setImageHeight($image_height)
    {
        $this->container['image_height'] = $image_height;

        return $this;
    }

    /**
     * Gets train_every
     *
     * @return int
     */
    public function getTrainEvery()
    {
        return $this->container['train_every'];
    }

    /**
     * Sets train_every
     *
     * @param int $train_every train_every
     *
     * @return $this
     */
    public function setTrainEvery($train_every)
    {
        $this->container['train_every'] = $train_every;

        return $this;
    }

    /**
     * Gets created_at
     *
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->container['created_at'];
    }

    /**
     * Sets created_at
     *
     * @param int $created_at created_at
     *
     * @return $this
     */
    public function setCreatedAt($created_at)
    {
        $this->container['created_at'] = $created_at;

        return $this;
    }

    /**
     * Gets trained_at
     *
     * @return int
     */
    public function getTrainedAt()
    {
        return $this->container['trained_at'];
    }

    /**
     * Sets trained_at
     *
     * @param int $trained_at trained_at
     *
     * @return $this
     */
    public function setTrainedAt($trained_at)
    {
        $this->container['trained_at'] = $trained_at;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}


