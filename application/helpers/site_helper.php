<?php

function fb_pr($pdata)
{
	echo "<pre>";
	print_r($pdata);
	echo "</pre>";
}

function fb_generate_pagination($params = array()){
	$ci =& get_instance();
	$ci->load->library('pagination');
	
	/* This Application Must Be Used With BootStrap 3 *  */
	$config['full_tag_open'] = "<ul class='pagination'>";
	$config['full_tag_close'] ="</ul>";
	$config['num_tag_open'] = '<li class="paginate_button page-item">';
	$config['num_tag_close'] = '</li>';
	$config['cur_tag_open'] = "<li class='paginate_button page-item active'><a href='#'>";
	$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
	$config['next_tag_open'] = "<li class=\"paginate_button page-item next\">";
	$config['next_tag_close'] = "</li>";
	$config['prev_tag_open'] = "<li class=\"paginate_button page-item previous\">";
	$config['prev_tag_close'] = "</li>";
	$config['first_tag_open'] = "<li class=\"paginate_button page-item\">";
	$config['first_tag_close'] = "</li>";
	$config['last_tag_open'] = "<li class=\"paginate_button page-item\">";
	$config['last_tag_close'] = "</li>";
	
	//$config['base_url'] = site_url('/home/index');
	$config['base_url'] = $params['page_burl'];
	$config['total_rows'] = $params['total_rows'];
	$config['per_page'] = $params['per_page'];
	$config['use_page_numbers'] = TRUE;
	$config['reuse_query_string'] = TRUE;
	$config['uri_segment'] = $params['uri_segment'];

	$ci->pagination->initialize($config);

	$plinks = $ci->pagination->create_links();
	return $plinks;
}

function fb_combo_arr($table_name = "ponds1", $no_id = false){
	$ci =& get_instance();
	$msg = $ci->fb_rest->combo_list($table_name, $no_id);
	$cmb_list = array();
	if($msg["status"] == "success"){
		$cmb_list = $msg["combo_list"];
	}
	return $cmb_list;
}

function fb_convert_time($date){
	$time = strtotime($date);
	$time = $time;
	return $time;
}

function fb_convert_date($time){
	$date = date("m/d/Y", $time);
	return $date;
}

function fb_convert_date_time($time){
	$date = date("m/d/Y H:i:s a", $time);
	return $date;
}

function fb_text($key){
	$ci =& get_instance();
	$txt = $ci->lang->line($key);
	$txt = !empty($txt) ? $txt : ucfirst(str_replace("_", " ", $key));
	return $txt;
}

function fb_lang_menu(){
	$ci =& get_instance();
	$ci->load->library('parser');
	$clang = $ci->config->item('language', 'fb_boodskap');
	$lang = $ci->fb_rest->get_fbuser_data('lang');
	$lang = (! empty($lang) ) ? $lang : "en";
	$cur_lang = $clang[$lang];
	$plang = array();
	$plang["cur_lang"] = $cur_lang["disp_txt"];
	$plang["cur_lang_icon"] = $cur_lang["icon_cls"];
	$lang_items = array();
	foreach($clang as $k => $alang){
		if ($lang == $k )
		  continue;
	    $curi_str = uri_string();
		$lang_url = site_url("common/change_lang?lang=".$k."&ruri=".$curi_str);
		$lang_text = $alang["disp_txt"];
		$lang_icon = $alang["icon_cls"];
		$lang_items[] = array("lang_url" => $lang_url, "lang_text" => $lang_text, "lang_icon" => $lang_icon);
	}
	$plang["lang_items"] = $lang_items;
	
	$ci->parser->set_delimiters("{", "}");
	$slang = $ci->parser->parse('include/lang_menu', $plang, true);
	return $slang;
	
}


function fb_cur_lang(){
	$ci =& get_instance();
	$clang = $ci->config->item('language', 'fb_boodskap');
	$lang = $ci->fb_rest->get_fbuser_data('lang');
	$lang = (! empty($lang) ) ? $lang : "en";
	$alang = $clang[$lang];
	return $alang;
}

function fb_jslang(){
	$ci =& get_instance();
	$alang = fb_cur_lang();
	//js_text_lang
	$lang = $ci->lang->load('js_text', $alang["lang_name"], true);
	return $lang;
}

function fb_message($type, $msg){
	$ci =& get_instance();
	$ci->load->library('parser');
	$params = array("message" => $msg);
	return $ci->parser->parse('message/'.$type, $params, true);
}

function fb_common_js(){
	$ci =& get_instance();
	$ci->load->library('parser');
	$params["site_url"] = site_url("/");
	$ajs_lang = fb_jslang();
	$params["site_err_lang"] = json_encode($ajs_lang);
	$cjs = $ci->parser->parse('include/common_js', $params, true);
	return $cjs;
	//$file = "common.js";
	//file_put_contents(FCPATH."assets/js/$file",utf8_encode($cjs),LOCK_EX);
}

function fb_sdate_qstr($dstr, $table_name){
	$ci =& get_instance();
	$ci->config->load('fb_boodskap', TRUE);
	$asearch_date = $ci->config->item('asearch_date', 'fb_boodskap');
	$srch_fld = $asearch_date[$table_name];
	$dtime = fb_convert_time($dstr);
	$edstr = date("m/d/Y", $dtime);
	$stime = $dtime;
	$etime = $dtime + ((24 * 60 * 60) - 1 );
	$lowv = $dtime;
	$topv = $etime;
	$range_fld = $srch_fld;
	return compact("lowv", "topv", "range_fld");
}

function fb_chk_date($dstr){
	$adate = explode("/",$dstr);
	if( count($adate) == 3)
	{
		$m = isset($adate[0]) ? $adate[0] : "";
		$d = isset($adate[1]) ? $adate[1] : "";
		$y = isset($adate[2]) ? $adate[2] : "";

		$m = (int) $m;
		$d = (int) $d;
		$y = (int) $y;
	
		$dflag = checkdate($m, $d, $y);
		return $dflag;
	} else {
		return false;
	}
}

function disp_weight($tcnt, $puwt){
	if($tcnt <= 0){
		return "-";
	}else{
		$twt = ($tcnt * $puwt);
		$twt = round($twt,2);
		return $twt;
	}
}

function chk_rst_cache(){
	$ci =& get_instance();
	$ci->config->load("fb_settings", TRUE);
	$asettings = $ci->config->item("settings", "fb_settings");
	$frst_set = $asettings["rest_api_cache"];
	return $frst_set;
}

function fb_clear_cache($sname){
	$ci =& get_instance();
	$ci->load->driver('cache', array('adapter' => 'file'));
	
	$flist = is_array($sname) ? $sname : array($sname);
	foreach($flist as $fname){
		$cache_path = config_item("cache_path");
		$cache_path = empty($cache_path) ? APPPATH.'cache/' : $cache_path;
		$cflist = glob($cache_path.$fname);
		foreach($cflist as $cfname){
			$cname = basename($cfname);
			$ci->cache->delete($cname);
		}
	}
}

function fb_convert_jsdate($time){
	$ctime = substr($time,0,10);
	$timezone  = 'UP55';
	$daylight_saving = false;
	$ctime = gmt_to_local($ctime, $timezone, $daylight_saving);
	$date = date("m/d/Y h:i:s a", $ctime);
	return $date;
}
function fb_fetch_alert(){
		$ci =& get_instance();
		$table_name="alerts";
		$from = 0;
		$size = 5;
		$orderfld = "createdtime";
		$orderdir = "desc";
		$data=array();
		$ci->load->library('parser');
		$ci->parser->set_delimiters("__","__");
		$qpms = array("size" => $size, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir);
		$query_str = $ci->parser->parse('query/query-filter', $qpms, true);
		$result = $ci->fb_rest->get_query_result($table_name, $query_str);
		return $result;
}
function fb_total_count(){
	$ci =& get_instance();
	$table_name = "alerts";
		/* if(! ( isset( $ci->tables[$table_name] ) ) ){
			$msg = array("status" => "fail", "message" => "Invalid Table");
			return $msg;
		} */
		$total=0;
		$query_str = $ci->parser->parse('query/query-filter', array(), true);
		$result = $ci->fb_rest->get_query_result($table_name, $query_str);
		//return $result;
		if($result["status"]=="success"){
			$total_count = $result["total_count"];
			//fb_pr($total_count);
            return $total_count;	
		} else {
			return $total;
		} 
}

function fb_hex2float($number) {
    $binfinal = sprintf("%016b",hexdec($number));
    $sign = substr($binfinal, 0, 1);
    $exp = substr($binfinal, 1, 8);
    $mantissa = "1".substr($binfinal, 9);
    $mantissa = str_split($mantissa);
    $exp = bindec($exp)-127;
    $significand=0;
	
    for ($i = 0; $i < count($mantissa); $i++) {
        $significand += (1 / pow(2,$i))*$mantissa[$i];
    }
    return $significand * pow(2,$exp) * ($sign*-2+1);
}


function fb_unit_hex($h){
	$dh = '0X'.dechex($h);
	$f = fb_hex2float($dh);
	return $f;
}

function get_dg_status(){
	$ci =& get_instance();
	$rec_id = "350412";
	$state = $ci->iot_rest->getDG($rec_id);
	
	if($state['status']=="success"){
		$dgstate = $state['data'];
		return $dgstate;
	}
	
}

function getVoltage(){
	$ci =& get_instance();
	$rec_id = "350412";
	$state = $ci->iot_rest->get_totVolt();
	
	if($state['status']=="success"){
		$dgstate = $state['data'];
		return $dgstate;
	}
}

function getPowerfactor(){
	$ci =& get_instance();
    $result = $ci->iot_rest->getPowerfactor();
    if($result["status"] == "success" && !(empty($result['data']))){
		return $result['data'];
	}else{
		return 0;
	}
}
 
 function getAmpValue(){
 	$ci =& get_instance();
    $result = $ci->iot_rest->get_totAmp();
    if($result["status"] == "success" && !(empty($result['data']))){
		return $result['data'];
	}else{
		return 0;
	}
}

function getKVA(){
	$ci =& get_instance();

 		$PF = getPowerfactor();
        $A = getAmpValue();
        $V = getVoltage();
        $kVA = round(pow(3,1/3) * $A * $V / 1000,2); 

        echo $kVA;	
}

function getFrequency(){
	$ci =& get_instance();

	$result= $ci->iot_rest->getFrequency();
    if($result["status"] == "success" && !(empty($result['data']))){
		return $result['data'];
	}else{
		return 0;
	}	
}