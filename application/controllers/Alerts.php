<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Alerts extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	
	
	public function __construct() {
		parent::__construct();
		$this->load->helper('date');
		
	}

	public function index()
	{
		if($this->fb_rest->isloggedin()){
			$data = array();
			
			
			$table_name = "alerts";
			$table_name = "alerts";
				$from = 0;
				$size = 10000;
				$orderfld = "createdtime";
				$orderdir = "desc";
				$this->load->library('parser');
				$this->parser->set_delimiters("__","__");
				$qpms = array("size" => $size, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir);
				$query_str = $this->parser->parse('query/get_unread_alert', $qpms, true);
				$result = $this->fb_rest->get_query_result($table_name, $query_str);
		
  		    //$this->load->view('include/header');
			$this->load->view('include/left_menu');
			 
			//fb_pr($msg);
			if($result["status"] == "success")
			{
				$data["total_count"] = $result["total_count"];
				$data["page_links"] = $result["page_links"];
				$data["result_set"] = $result["result_set"];
				$this->load->view("include/header", $data);
			}else{
				$this->load->view("layout/error", $data);
			}
			$this->load->view('include/footer');
			
		}else{
			redirect('/login');
		}
	
	}
	public function list_record(){
	$table_name = "alerts";
	$this->fb_rest->test_list_record($table_name);
	}
	public function list_all_alerts(){
		if($this->fb_rest->isloggedin()){
				$data = array();
				$table_name = "alerts";
				$params =  array("sort_fld" => "createdtime", "sort_dir" => "asc","table_name" => $table_name);
				$result  = $this->fb_rest->list_record($params);
			//fb_pr($result); exit;
			
				$this->load->view('include/header');
				$this->load->view('include/left-sidebar');
				 
				//fb_pr($msg);
				if($result["status"] == "success")
				{
					
					$data["result_set"] = $result["result_set"];
					$this->load->view("alerts_content", $data);
				}else{
					$this->load->view("layout/error", $data);
				}
				$this->load->view('include/footer');
				
			}else{
				redirect('/login');
			}
	}
	function delete(){
		$table_name="alerts";
		$rkey = $this->input->post("rid");	
		$result= $this->fb_rest->delete_record($table_name, $rkey);
		
		if($result['status']=="success"){
			//$this->session->set_flashdata('delete_success',fb_text("delete_success"));
			redirect('/alerts/list_all_alerts');
		}else{
			//$this->session->set_flashdata('delete_failed',fb_text("delete_failed"));
			redirect('/alerts/list_all_alerts');
		}
	}
	function view_delete(){
		$this->load->view('include/header');
		$this->load->view('include/left-sidebar');
		$this->load->view("view_delete");
		$this->load->view('include/footer');
	}
}