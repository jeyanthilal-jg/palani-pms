<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	
	
	public function __construct() {
		parent::__construct();
	}

	public function index()
	{
		
		if($this->fb_rest->isloggedin()){
			$data=array();
			$presult_data = array();
			$rec_id = "350412";
			$result = $this->iot_rest->get_records($rec_id);
			//print_r($result); exit();
            $this->load->view('include/header');
            $this->load->view('include/left-sidebar');
			if($result["status"] == "success"){
				
				
				$data["presult_data"] = $result["data"];
				$this->load->view('reports',$data);	
			}
			else{
				$this->load->view("layout/error", $data);
			}
            $this->load->view('include/footer');
		}else{
			redirect('/login');
		}
	}
	
	

	
}
