<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Genset extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	
	
	public function __construct() {
		parent::__construct();
        $this->load->helper('date');
	}

	public function index()
	{
        if($this->fb_rest->isloggedin()){
			$data = array();
			$page_no = $this->uri->segment('2');
			$per_page = $this->input->get_post("no_items", true);
			$search = $this->input->get_post("search", true);
			$sort_fld = $this->input->get_post("sort_fld", true);
			$sort_dir = $this->input->get_post("sort_dir", true);
			$page_burl = site_url("/genset");
			$this->table="genset";
			$table_name = $this->table;
			$params =  array("page_no" => $page_no, "per_page" => $per_page, "uri_segment" => "2",
			"search" => $search, "sort_fld" => $sort_fld, "sort_dir" => $sort_dir, "page_burl" => $page_burl, "table_name" => $table_name);
            $this->load->view('include/header');
            $this->load->view('include/left-sidebar');
            $msg  = $this->fb_rest->list_record($params);
			if($msg["status"] == "success")
			{
				$data["page_links"] = $msg["page_links"];
				$data["result_set"] = $msg["result_set"];
				$this->load->view("genset", $data);
			}else{
				$this->load->view("layout/error", $data);
			}
			$this->load->view('include/footer');
			
		}else{
			redirect('/login');
		}
		
	}
    
    public function addtoGenset(){

    $device_id = $this->input->post("device_id");
    $device_name = $this->input->post("device_name");
    $watts = $this->input->post("watts");
    $device_type = $this->input->post("device_type");

    $table_name = "genset";
    $idata = array("device_id" => $device_id, 
    "device_name" => $device_name,
    "status" => 1,
    "watts" => $watts,
    "device_type" => $device_type,
    "createdtime" => now(), 
    "updatedtime" => now());

        $result = $this->fb_rest->create_record($table_name, $idata);
        if($result['status']=="success"){
           echo "success";
        }else{
            echo "error";
        }
    }
    
    public function deleteGenset(){
        $device_id = $this->input->post("device_id");
        $table_name="genset";
		$result= $this->fb_rest->delete_record($table_name, $device_id);
		
		if($result['status']=="success"){
			echo "success";
		}else{
			echo "failed";
		}
    }
    
    public function list_record(){
		$table_name = "genset_list";
		$this->fb_rest->test_list_record($table_name);
	}   
    
    public function genset_load($name){
        if($this->fb_rest->isloggedin()){
			$data = array();
            $this->load->view('include/header');
            $this->load->view('include/left-sidebar');
            if($name){
            	$this->load->view("genset_load");
            }else{
            	$this->load->view("layout/404_error", $data);
            }
			$this->load->view('include/footer');
			
		}else{
			redirect('/login');
		}        
        
    }
    
public function genset_list(){
        if($this->fb_rest->isloggedin()){
			$data = array();
			$page_no = $this->uri->segment('2');
			$per_page = $this->input->get_post("no_items", true);
			$search = $this->input->get_post("search", true);
			$sort_fld = $this->input->get_post("sort_fld", true);
			$sort_dir = $this->input->get_post("sort_dir", true);
			$page_burl = site_url("/genset");
			$this->table="genset_list";
			$table_name = $this->table;
			$params =  array("page_no" => $page_no, "per_page" => $per_page, "uri_segment" => "2",
			"search" => $search, "sort_fld" => $sort_fld, "sort_dir" => $sort_dir, "page_burl" => $page_burl, "table_name" => $table_name);
            $this->load->view('include/header');
            $this->load->view('include/left-sidebar');
            $msg  = $this->fb_rest->list_record($params);
			if($msg["status"] == "success")
			{
				$data["page_links"] = $msg["page_links"];
				$data["result_set"] = $msg["result_set"];
				$this->load->view("genset_list", $data);
			}else{
				$this->load->view("layout/error", $data);
			}
			$this->load->view('include/footer');
			
		}else{
			redirect('/login');
		}      
        
    }    
    
	public function add_record(){
		$form_data = $this->input->post();


			$table_name = "genset_list";
			$idata = array("name" => $this->input->post("name") , 
			"type" => $this->input->post("type"), 
			"rpm" => $this->input->post("rpm"),
			"minVal" => $this->input->post("minVal"),
			"maxVal" => $this->input->post("maxVal"),
			"createdtime" => now(), 
			"updatedtime" => now());
			
			$result = $this->fb_rest->create_record($table_name, $idata);

            if($result['status']=="success"){
                $this->session->set_flashdata('success',fb_text("success"));
                redirect('genset/genset_list');
            }else{
                $this->session->set_flashdata('failed',fb_text("failed"));
                redirect('genset/genset_list');
            }
	}
    

	
	

	
}
