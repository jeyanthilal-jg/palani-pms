<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Meters extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->helper('date');
		$this->session->keep_flashdata('update_success');
		$this->session->keep_flashdata('update_failed');				
	}

	public function index()
	{
		if($this->fb_rest->isloggedin()){

		$data=array();
		$presult_data = array();
		$rec_id = "350414";
		$result = $this->iot_rest->getmeter_list($rec_id);
		//print_r($result); exit();
        $this->load->view('include/header');
        $this->load->view('include/left-sidebar');
		if($result["status"] == "success"){
			
			
			$data["presult_data"] = $result["data"];
			$this->load->view('meters',$data);	
		}
		else{
			$this->load->view("layout/error", $data);
		}
        $this->load->view('include/footer');			

		}else{
			redirect('/login');
		}
	}

	public function add_meter(){
		
		$table_name = "meters";
    	$config['upload_path'] = './meter-images/';
		$config['allowed_types'] = 'gif|jpg|png'; 

        $this->load->library('upload', $config);

		$this->upload->do_upload('meter_image');

		$data = $this->upload->data();
     	$meter_image = $data['file_name'];

     	 $action = $this->input->post("action");
     	 $rid = $this->input->post("rid");
		 $meter_id = $this->input->post("meter_id");
		 $meter_name = $this->input->post("meter_name");
		 $meter_desc = $this->input->post("meter_desc");
		 $kva = $this->input->post("kva");

     	 if($action=="update" && $rid){
     	 	$form_data = $this->input->post();
     	 	$form_data['updatedtime']=now();
     	 	$form_data['name']=$meter_name;
     	 	$form_data['description'] = $meter_desc;
     	 	if($meter_image)
     	 	$form_data['image'] = $meter_image;
     	 	
     	 	$result = $this->fb_rest->update_record($table_name,$form_data,$rid);
     	 }else{
			
			$idata = array("meter_id" => $meter_id, 
			"name" => $meter_name, 
			"description" => $meter_desc,
			"image" => $meter_image,
			"status" => "true",
			"kva" => $kva,
			"createdtime" => now(), 
			"updatedtime" => now());
			$result = $this->fb_rest->create_record($table_name, $idata);     	 	
     	 }

			if($result['status']=="success"){
				$this->session->set_flashdata('meter_success','meter added/updated successfully');
				redirect('/meters');
			}else{
				$this->session->set_flashdata('meter_failed','please try again later');
				redirect('/meters');
			}

	}	

	public function delete(){
		$table_name="meters";
		$rkey = $this->input->post("rid");	
		$result= $this->fb_rest->delete_record($table_name, $rkey);
		
		if($result['status']=="success"){
			$this->session->set_flashdata('delete_success',"meter deleted");
			redirect('/meters');
		}else{
			$this->session->set_flashdata('delete_failed',"issue deleting meter");
			redirect('/meters');
		}
	}

	public function updateStatus($rid,$status){
		$table_name="meters";
		$form_data =  array();
		if ($status=="false") {
			$form_data['status']="false";	
		}else{
			$form_data['status']="true";	
		}
		//print_r($form_data); exit();
		$rkey = $rid;
		$result = $this->fb_rest->update_record($table_name,$form_data,$rkey);
		//print_r($result); exit();
		if($result['status']=="success"){
			$this->session->set_flashdata('update_success',"meter status update success");
			redirect('/meters');
		}else{
			$this->session->set_flashdata('update_failed',"meter status update failed");
			redirect('/meters');
		}
	}				
}
?>