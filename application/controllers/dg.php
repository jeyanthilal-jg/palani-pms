<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dg extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	
	
	public function __construct() {
		parent::__construct();
	}

	public function index()
	{
		
		if($this->fb_rest->isloggedin()){
			$this->load->view('dashboard');			
		}else{
			redirect('/login');
		}
	}
    
    public function formulaPower($phase){
        $watts = $this->getwattsValue();       
        $kW = $watts / 1000;
        return $kW;
    }
    
    public function formulaKVA($phase){
        $PF = $this->getPowerfactor();
        $A = $this->getAmpValue();
        $V = $this->getVoltValue();

        if($phase==1){
            $kVA = round(($A * $V ) /1000,2);
        }else{
            $kVA = round(pow(3,1/3) * $A * $V / 1000,2); 
        }
        
        return $kVA;
    }
	
	
	public function get_message(){
		$rec_id = "350412";
		$result = $this->iot_rest->get_power_detail($rec_id);
				
		
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($result));
	}
	
	
	public function get_last_message(){
		$msg_id = "350405";
		$apiInstance = new Swagger\Client\Api\ListDeviceMessagesApi(
			// If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
			// This is optional, `GuzzleHttp\Client` will be used as default.
			new GuzzleHttp\Client()
		);
		$atoken = $this->fb_rest->get_fbuser_data("user_token"); // string | Auth token of the logged in user
		$page_size = 1; // int | Maximum number of messages to be listed
		$direction = ""; // string | If direction is specified, **muid** is required
		$muid = $msg_id; // string | Last or First message uuid of the previous list operation, **required** if **direction** is specified

		try {
			$result = $apiInstance->listDeviceMessages($atoken, $page_size, $direction, $muid);
			$mobj = isset($result["0"]) ? $result["0"] : "";
			if(!empty($mobj)){
				$mdata = $mobj->getData();
				$nuuid = $mobj->getNodeUuid();
				fb_pr($result);
				fb_pr($mdata);
				echo "Node uuid: $nuuid <br/>";
				
			}else{
				echo "Empty Message";
			}
			
		} catch (Exception $e) {
			echo 'Exception when calling ListDeviceMessagesApi->listDeviceMessages: ', $e->getMessage(), PHP_EOL;
		}
	}
	
	public function get_new_message(){
		$msg_id = "350412";
		$result = $this->iot_rest->get_message($msg_id);
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($result));
	}
    
    public function getVolt(){
        $result = $this->iot_rest->get_totVolt();

		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($result));
    }
    
    public function getVoltValue(){
        $result = $this->iot_rest->get_totVolt();

        if($result["status"] == "success" && !(empty($result['data']))){
			return $result['data'];
		}else{
			return 0;
		}
    }  
    
     public function getVoltSingle(){
        $result = $this->iot_rest->get_singleVolt();
       if($result["status"] == "success" && !(empty($result['data']))){
			return $result['data'];
		}else{
			return 0;
		}
    }   
    
    
     public function getVoltSinglegauge(){
        $result = $this->iot_rest->get_singleVolt();
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($result));
    }
    
    public function getAmp(){
        $result = $this->iot_rest->get_totAmp();
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($result));
    }
    
     public function getAmpValue(){
        $result = $this->iot_rest->get_totAmp();
        if($result["status"] == "success" && !(empty($result['data']))){
			return $result['data'];
		}else{
			return 0;
		}
    }
    
    public function getPowerfactor(){
        $result = $this->iot_rest->getPowerfactor();
        if($result["status"] == "success" && !(empty($result['data']))){
			return $result['data'];
		}else{
			return 0;
		}
    }
    
    public function getwattsValue()
    {
        
        $result = $this->iot_rest->gettotWatts();
        if($result["status"] == "success" && !(empty($result['data']))){
			return $result['data'];
		}else{
			return 0;
		}
    }
    
    public function voltampGraph(){
        
            $data=array();
			$presult_data = array();
			$rec_id = "350412";
			$result = $this->iot_rest->get_power_list($rec_id);
			//fb_pr($result);
			//print_r($result);
			if($result["status"] == "success"){
			$pdata = $result["data"];
			
             $this->output
			->set_content_type('application/json')
			->set_output(json_encode($pdata));
			
			}
    }
    
    public function voltampMsg(){
        
        $rec_id = "350412";
		$result = $this->iot_rest->get_power_detail($rec_id);
				
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($result));
        
    }
    
	public function test_message()
	{
		$result = array();
		$record = array();
		$msg_id = "350412";
		$result = $this->iot_rest->get_message($msg_id);
		$row = $result['data'];
		fb_pr($row);
		
			if($row['GEN']==0){
				$table_name = "alert_settings";
				$params =  array("per_page" =>1,"sort_fld" => "createdtime","sort_dir" => "desc","table_name" => $table_name);
				$msg  = $this->fb_rest->list_record($params);
				if($msg['status']=="success"){
					$record = $msg['result_set'];
					foreach($record as $rcd){
						$src = $rcd['_source'];
						
					}
				}
				if($row['TV']>$src['spmin']){
					if(($row['RV']>$src['rspmin'] && $row['BV']>$src['bspmin'])||($row['RV']>$src['rspmin'])){
						if($row['RV']>$src['rspmin']){
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "B and R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
					}
					elseif(($row['BV']>$src['bspmin'] && $row['YV']>$src['yspmin'])||($row['YV']>$src['yspmin'])){
						if($row['YV']>$src['yspmin']){
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
					elseif(($row['BV']>$src['bspmin'] && $row['RV']>$src['rspmin']) || ($row['BV']>$src['bspmin'])){
						if($row['BV']>$src['bspmin']){
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
				}
				
				if($row['TV']<$src['spmax']){
					if(($row['RV']<$src['rspmax'] && $row['BV']<$src['bspmax'])||($row['RV']<$src['rspmax'])){
						if($row['RV']<$src['rspmax']){
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "B and R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
					}
					elseif(($row['BV']<$src['bspmax'] && $row['YV']<$src['yspmax'])||($row['YV']<$src['yspmax'])){
						if($row['YV']<$src['yspmax']){
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
					elseif(($row['BV']<$src['bspmax'] && $row['RV']<$src['rspmax']) || ($row['BV']<$src['bspmax'])){
						if($row['BV']<$src['bspmax']){
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
				}
				
				if($row['TPV']>$src['tpmin']){
					if(($row['RBV']>$src['rtpmin'] && $row['BYV']>$src['btpmin'])||($row['RBV']>$src['rtpmin'])){
						if($row['RBV']>$src['rtpmin']){
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "B and R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
					}
					elseif(($row['BYV']>$src['btpmin'] && $row['YRV']>$src['ytpmin'])||($row['YRV']>$src['ytpmin'])){
						if($row['YRV']>$src['tpmin']){
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
					elseif(($row['BYV']>$src['btpmin'] && $row['RBV']>$src['ttpmin']) || ($row['BYV']>$src['btpmin'])){
						if($row['BYV']>$src['btpmin']){
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
				}
				
				if($row['TPV']<$src['tpmax']){
					if(($row['RBV']<$src['rtpmax'] && $row['BYV']<$src['btpmax'])||($row['RBV']<$src['rtpmax'])){
						if($row['RBV']<$src['rtpmax']){
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "B and R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
					}
					elseif(($row['BYV']<$src['btpmax'] && $row['YRV']<$src['ytpmax'])||($row['YRV']<$src['ytpmax'])){
						if($row['YRV']<$src['ytpmax']){
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
					elseif(($row['BYV']<$src['btpmax'] && $row['RBV']<$src['rtpmax']) || ($row['BYV']<$src['btpmax'])){
						if($row['BYV']<$src['btpmax']){
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
				}
				
				if($row['TC']>$src['tcpmin']){
					if(($row['RC']>$src['rcpmin'] && $row['BC']>$src['bcpmin'])||($row['RC']>$src['rcpmin'])){
						if($row['RC']>$src['rcpmin']){
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "R Single Phase Coltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "B and R Single Phase Coltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
					}
					elseif(($row['BC']>$src['bcpmin'] && $row['YC']>$src['ycpmin'])||($row['YC']>$src['ycpmin'])){
						if($row['YC']>$src['ycpmin']){
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "R Single Phase Coltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Coltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
					elseif(($row['BC']>$src['bcpmin'] && $row['RC']>$src['rcpmin']) || ($row['BC']>$src['bcpmin'])){
						if($row['BC']>$src['bcpmin']){
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "R Single Phase Coltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Coltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
				}
				
				if($row['TC']<$src['tcpmax']){
					if(($row['RC']<$src['rcpmax'] && $row['BC']<$src['bcpmax'])||($row['RC']<$src['rcpmax'])){
						if($row['RC']<$src['rcpmax']){
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "R Single Phase Coltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "B and R Single Phase Coltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
					}
					elseif(($row['BC']<$src['bcpmax'] && $row['YC']<$src['ycpmax'])||($row['YC']<$src['ycpmax'])){
						if($row['YC']<$src['ycpmax']){
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "R Single Phase Coltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Coltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
					elseif(($row['BC']<$src['bcpmax'] && $row['RC']<$src['rcpmax']) || ($row['BC']<$src['bcpmax'])){
						if($row['BC']<$src['bcpmax']){
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "R Single Phase Coltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Coltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
				}
				
				if($row['TP']>$src['tppmin']){
					if(($row['RP']>$src['rppmin'] && $row['BP']>$src['bppmin'])||($row['RP']>$src['rppmin'])){
						if($row['RP']>$src['rppmin']){
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "R Single Phase Poltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "B and R Single Phase Poltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
					}
					elseif(($row['BP']>$src['bppmin'] && $row['YP']>$src['yppmin'])||($row['YP']>$src['yppmin'])){
						if($row['YP']>$src['yppmin']){
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "R Single Phase Poltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Poltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
					elseif(($row['BP']>$src['bppmin'] && $row['RP']>$src['rppmin']) || ($row['BP']>$src['bppmin'])){
						if($row['BP']>$src['bppmin']){
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "R Single Phase Poltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Poltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
				}
								
				if($row['TP']<$src['tppmax']){
					if(($row['RP']<$src['rppmax'] && $row['BP']<$src['bppmax'])||($row['RP']<$src['rppmax'])){
						if($row['RP']<$src['rppmax']){
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "R Single Phase Poltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "B and R Single Phase Poltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
					}
					elseif(($row['BP']<$src['bppmax'] && $row['YP']<$src['yppmax'])||($row['YP']<$src['yppmax'])){
						if($row['YP']<$src['yppmax']){
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "R Single Phase Poltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Poltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
					elseif(($row['BP']<$src['bppmax'] && $row['RP']<$src['rppmax']) || ($row['BP']<$src['bppmax'])){
						if($row['BP']<$src['bppmax']){
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "R Single Phase Poltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Poltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
				}
								
				if($row['FRE']> $src['fremin']){
					$table_name= "alerts";
					$idata = array("alert_id" => "1",
					"alert_type" => "warning",				
					"alert_state" => "true", 
					"msg" => "High Frequency",
					"createdtime" => now(), 
					"updatedtime" => now());
					//$result = $this->fb_rest->create_record($table_name, $idata);
					//fb_pr($result);
				}
				
				if($row['FRE']<$src['fremax']){
					$table_name= "alerts";
					$idata = array("alert_id" => "1",
					"alert_type" => "warning",				
					"alert_state" => "true", 
					"msg" => "High Frequency",
					"createdtime" => now(), 
					"updatedtime" => now());
					//$result = $this->fb_rest->create_record($table_name, $idata);
					//fb_pr($result);
				}
				
				if($row['PF']>$src['pfmin']){
					$table_name= "alerts";
					$idata = array("alert_id" => "1",
				"alert_type" => "warning",				
				"alert_state" => "true", 
				"msg" => "Problem Power Factor ",
				"createdtime" => now(), 
				"updatedtime" => now());
					//$result = $this->fb_rest->create_record($table_name, $idata);
					//fb_pr($result);
				}
				
				if($row['PF']<$src['pfmax']){
					$table_name= "alerts";
					$idata = array("alert_id" => "1",
				"alert_type" => "warning",				
				"alert_state" => "true", 
				"msg" => "Problem Power Factor ",
				"createdtime" => now(), 
				"updatedtime" => now());
					//$result = $this->fb_rest->create_record($table_name, $idata);
					//fb_pr($result);
				}
				
			}else{
				$table_name = "alert_settings_dg";
				$params =  array("per_page" =>1,"sort_fld" => "createdtime","sort_dir" => "desc","table_name" => $table_name);
				$msg  = $this->fb_rest->list_record($params);
				if($msg['status']=="success"){
					$record = $msg['result_set'];
					foreach($record as $rcd){
						$src = $rcd['_source'];
						
					}
				}
				if($row['TV']>$src['spmin']){
					if(($row['RV']>$src['rspmin'] && $row['BV']>$src['bspmin'])||($row['RV']>$src['rspmin'])){
						if($row['RV']>$src['rspmin']){
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "B and R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
					}
					elseif(($row['BV']>$src['bspmin'] && $row['YV']>$src['yspmin'])||($row['YV']>$src['yspmin'])){
						if($row['YV']>$src['yspmin']){
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
					elseif(($row['BV']>$src['bspmin'] && $row['RV']>$src['rspmin']) || ($row['BV']>$src['bspmin'])){
						if($row['BV']>$src['bspmin']){
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
				}
				
				if($row['TV']<$src['spmax']){
					if(($row['RV']<$src['rspmax'] && $row['BV']<$src['bspmax'])||($row['RV']<$src['rspmax'])){
						if($row['RV']<$src['rspmax']){
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "B and R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
					}
					elseif(($row['BV']<$src['bspmax'] && $row['YV']<$src['yspmax'])||($row['YV']<$src['yspmax'])){
						if($row['YV']<$src['yspmax']){
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
					elseif(($row['BV']<$src['bspmax'] && $row['RV']<$src['rspmax']) || ($row['BV']<$src['bspmax'])){
						if($row['BV']<$src['bspmax']){
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
				}
				
				if($row['TPV']>$src['tpmin']){
					if(($row['RBV']>$src['rtpmin'] && $row['BYV']>$src['btpmin'])||($row['RBV']>$src['rtpmin'])){
						if($row['RBV']>$src['rtpmin']){
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "B and R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
					}
					elseif(($row['BYV']>$src['btpmin'] && $row['YRV']>$src['ytpmin'])||($row['YRV']>$src['ytpmin'])){
						if($row['YRV']>$src['tpmin']){
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
					elseif(($row['BYV']>$src['btpmin'] && $row['RBV']>$src['ttpmin']) || ($row['BYV']>$src['btpmin'])){
						if($row['BYV']>$src['btpmin']){
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
				}
				
				if($row['TPV']<$src['tpmax']){
					if(($row['RBV']<$src['rtpmax'] && $row['BYV']<$src['btpmax'])||($row['RBV']<$src['rtpmax'])){
						if($row['RBV']<$src['rtpmax']){
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "B and R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
					}
					elseif(($row['BYV']<$src['btpmax'] && $row['YRV']<$src['ytpmax'])||($row['YRV']<$src['ytpmax'])){
						if($row['YRV']<$src['ytpmax']){
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
					elseif(($row['BYV']<$src['btpmax'] && $row['RBV']<$src['rtpmax']) || ($row['BYV']<$src['btpmax'])){
						if($row['BYV']<$src['btpmax']){
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
				}
				
				if($row['TC']>$src['tcpmin']){
					if(($row['RC']>$src['rcpmin'] && $row['BC']>$src['bcpmin'])||($row['RC']>$src['rcpmin'])){
						if($row['RC']>$src['rcpmin']){
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "R Single Phase Coltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "B and R Single Phase Coltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
					}
					elseif(($row['BC']>$src['bcpmin'] && $row['YC']>$src['ycpmin'])||($row['YC']>$src['ycpmin'])){
						if($row['YC']>$src['ycpmin']){
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "R Single Phase Coltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Coltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
					elseif(($row['BC']>$src['bcpmin'] && $row['RC']>$src['rcpmin']) || ($row['BC']>$src['bcpmin'])){
						if($row['BC']>$src['bcpmin']){
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "R Single Phase Coltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Coltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
				}
				
				if($row['TC']<$src['tcpmax']){
					if(($row['RC']<$src['rcpmax'] && $row['BC']<$src['bcpmax'])||($row['RC']<$src['rcpmax'])){
						if($row['RC']<$src['rcpmax']){
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "R Single Phase Coltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "B and R Single Phase Coltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
					}
					elseif(($row['BC']<$src['bcpmax'] && $row['YC']<$src['ycpmax'])||($row['YC']<$src['ycpmax'])){
						if($row['YC']<$src['ycpmax']){
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "R Single Phase Coltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Coltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
					elseif(($row['BC']<$src['bcpmax'] && $row['RC']<$src['rcpmax']) || ($row['BC']<$src['bcpmax'])){
						if($row['BC']<$src['bcpmax']){
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "R Single Phase Coltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Coltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
				}
				
				if($row['TP']>$src['tppmin']){
					if(($row['RP']>$src['rppmin'] && $row['BP']>$src['bppmin'])||($row['RP']>$src['rppmin'])){
						if($row['RP']>$src['rppmin']){
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "R Single Phase Poltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "B and R Single Phase Poltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
					}
					elseif(($row['BP']>$src['bppmin'] && $row['YP']>$src['yppmin'])||($row['YP']>$src['yppmin'])){
						if($row['YP']>$src['yppmin']){
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "R Single Phase Poltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Poltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
					elseif(($row['BP']>$src['bppmin'] && $row['RP']>$src['rppmin']) || ($row['BP']>$src['bppmin'])){
						if($row['BP']>$src['bppmin']){
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "R Single Phase Poltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Poltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
				}
								
				if($row['TP']<$src['tppmax']){
					if(($row['RP']<$src['rppmax'] && $row['BP']<$src['bppmax'])||($row['RP']<$src['rppmax'])){
						if($row['RP']<$src['rppmax']){
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "R Single Phase Poltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "B and R Single Phase Poltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
					}
					elseif(($row['BP']<$src['bppmax'] && $row['YP']<$src['yppmax'])||($row['YP']<$src['yppmax'])){
						if($row['YP']<$src['yppmax']){
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "R Single Phase Poltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Poltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
					elseif(($row['BP']<$src['bppmax'] && $row['RP']<$src['rppmax']) || ($row['BP']<$src['bppmax'])){
						if($row['BP']<$src['bppmax']){
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "R Single Phase Poltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => range(1,10000), 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Poltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
				}
								
				if($row['FRE']> $src['fremin']){
					$table_name= "alerts";
					$idata = array("alert_id" => "1",
					"alert_type" => "warning",				
					"alert_state" => "true", 
					"msg" => "High Frequency",
					"createdtime" => now(), 
					"updatedtime" => now());
					//$result = $this->fb_rest->create_record($table_name, $idata);
					//fb_pr($result);
				}
				
				if($row['FRE']<$src['fremax']){
					$table_name= "alerts";
					$idata = array("alert_id" => "1",
					"alert_type" => "warning",				
					"alert_state" => "true", 
					"msg" => "High Frequency",
					"createdtime" => now(), 
					"updatedtime" => now());
					//$result = $this->fb_rest->create_record($table_name, $idata);
					//fb_pr($result);
				}
				
				if($row['PF']>$src['pfmin']){
					$table_name= "alerts";
					$idata = array("alert_id" => "1",
				"alert_type" => "warning",				
				"alert_state" => "true", 
				"msg" => "Problem Power Factor ",
				"createdtime" => now(), 
				"updatedtime" => now());
					//$result = $this->fb_rest->create_record($table_name, $idata);
					//fb_pr($result);
				}
				
				if($row['PF']<$src['pfmax']){
					$table_name= "alerts";
					$idata = array("alert_id" => "1",
				"alert_type" => "warning",				
				"alert_state" => "true", 
				"msg" => "Problem Power Factor ",
				"createdtime" => now(), 
				"updatedtime" => now());
					//$result = $this->fb_rest->create_record($table_name, $idata);
					//fb_pr($result);
				}
				

			}
	}
	public function send_mail() { 

		$config = Array(
			'protocol' => 'smtp',
			'smtp_host' => 'ssl://smtp.googlemail.com',
			'smtp_port' => 465,
			'smtp_user' => 'lalasarjun@gmail.com',
			'smtp_pass' => 'sourashtraclg',
			'mailtype'  => 'html', 
			'charset'   => 'iso-8859-1'
		);
		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");

		$this->email->from('lalasarjun@gmail.com', 'JeyanthiLal');
        $this->email->to('lalaslal@gmail.com'); 

        $this->email->subject('Email Test');
        $this->email->message('Testing the email class.');

		$result = $this->email->send();
        echo $result;
      } 

	
}
