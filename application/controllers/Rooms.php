<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rooms extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	
	
	public function __construct() {
		parent::__construct();
		$this->load->helper('date');
		//$this->table="rooms";
	}

	public function index()
	{
		if($this->fb_rest->isloggedin()){
			$data = array();
			$page_no = $this->uri->segment('2');
			$per_page = $this->input->get_post("no_items", true);
			$search = $this->input->get_post("search", true);
			$sort_fld = $this->input->get_post("sort_fld", true);
			$sort_dir = $this->input->get_post("sort_dir", true);
			$page_burl = site_url("/rooms");
			$table_name="rooms";
			
			$params =  array("page_no" => $page_no, "per_page" => $per_page, "uri_segment" => "2",
			"search" => $search, "sort_fld" => $sort_fld, "sort_dir" => $sort_dir, "page_burl" => $page_burl, "table_name" => $table_name);
			
			$data["sort_fld"] = $sort_fld;
			$data["sort_dir"] = $sort_dir;
			$data["search"] = $search;
			$data["per_page"] = $per_page;
			
			$sort_columns = array("room_id", "room_name");
			//echo fb_text("feed_stock_distribution_list"); 
			$hstr = array("room_id" =>"rooms_id", "room_name" => "room_name");
			
			$theader = "";
			
			foreach($hstr as $hk => $hv)
			{
				if(in_array($hk, $sort_columns)){
					$cdir = ($hk == $sort_fld) ? (($sort_dir=="asc") ? "desc" : "asc" ) : "asc";
					$pstr = (!empty($per_page)) ? $per_page : "10";
					$srt_params = array("sort_fld" => $hk, "sort_dir" => $cdir, "no_items" => $pstr, "search" => $search);
					$srt_str = http_build_query($srt_params);
					$srt_url = site_url("/rooms?$srt_str");
					$cdir_icon = "";
					if(!empty($sort_fld)){
						$cdir_icon = ($hk == $sort_fld) ? 
						(($sort_dir=="asc") ? "&nbsp;<i class=\"fa fa-sort-asc\"></i>" : "&nbsp;<i class=\"fa fa-sort-desc\"></i>" ) : "";
					}
					$thstr = $hv.$cdir_icon;
					$thtml = "<th><a href='$srt_url'>$thstr</a></th>";
					$theader .= $thtml."\n";
				}else{
					$theader .= "<th>$hv</th>\n";
				}
			}
			
			$data["theader"] = $theader;
			$msg  = $this->fb_rest->list_record($params);
		
		
  		//fb_pr($msg);
			
			$data["state"] = $this->getStatus(); 
			
			
			if($msg["status"] == "success")
			{
				//fb_pr($data["state"]);
				$data["page_links"] = $msg["page_links"];
				$data["result_set"] = $msg["result_set"];
				$this->load->view("layout/rooms_content", $data);
			}else{
				$this->load->view("layout/error", $data);
			}
			$this->load->view('include/footer');
			
		}else{
			redirect('/login');
		}
	}
	public function add_record(){
		$form_data = $this->input->post();
		
		$room_id = $this->input->post("room_id");
		$room_name = $this->input->post("room_name");
		$grant_access = $this->input->post("grant_access");
		$timeout = $this->input->post("timeout");
		$timeout_enabled = $this->input->post("timeout_enabled");
		
			$table_name = "rooms";
			$idata = array("room_id" => $room_id, 
			"room_name" => $room_name, 
			"grant_access" => $grant_access,
			"timeout" => $timeout,
			"timeout_enabled" => $timeout_enabled,
			"last_active" => now(),
			"autowakeuptime" => strtotime("0 day midnight"),
			"autowakeuplastran" => now(),
			"createdtime" => now(),
			"updatedtime" => now()
			);
		
			$result = $this->fb_rest->create_record($table_name, $idata);
			 fb_pr($result);
		if($result['status']=="success"){
			$this->session->set_flashdata('success',fb_text("success"));
			redirect('/rooms');
		}else{
			$this->session->set_flashdata('failed',fb_text("failed"));
			redirect('/rooms');
		}
	}
	public function list_record(){
		$table_name = "rooms";
		$this->fb_rest->test_list_record($table_name);
	}
	public function updatedevice_status(){
		$room_id = $this->input->post("room_id");
		
		$table_name = "rooms";
		$from = 0;
		$size = 100;
		$orderfld = "updatedtime";
		$orderdir = "desc";
		$this->load->library('parser');
		$this->parser->set_delimiters("__","__");
		$qpms = array("size" => $size, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir, "room_id" => $room_id);
		$query_str = $this->parser->parse('query/update_state', $qpms, true);
		$result = $this->fb_rest->get_query_result($table_name, $query_str);
		//print_r($result);
		//echo json_encode($result,true);
	     if($result["status"] == "success"){
			$rst = $result["result_set"];
			foreach($rst as $row){
				$idata = $row["_source"];
				if($idata["device_status"] == "true"){
				$idata["device_status"] = "false";
				}
				elseif($idata["device_status"] == "false"){
				$idata["device_status"] = "true";
				}
				fb_pr($idata);
				fb_pr($row["_id"]); 
				$this->fb_rest->update_record($table_name, $idata, $row["_id"]);
			}
		} 
		
   }
   	/* public function getStatus(){
	 $ci =& get_instance();
	  $table_name = "devices";
	  $result = $ci->fb_rest->search_list($table_name,'',100);	
	  //print_r($result);
	  $state = isset($result["result_set"])?$result["result_set"]:array();
	  return $state;
	} */
	
	public function getStatus(){
		$room_id = $this->input->post("room_id");
		//$room_id="H";
		$table_name = "devices";
		$from = 0;
		$size = 100;
		$orderfld = "updatedtime";
		$orderdir = "desc";
		$this->load->library('parser');
		$this->parser->set_delimiters("__","__");
		$qpms = array("size" => $size, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir, "device_id" => $room_id);
		$query_str = $this->parser->parse('query/query1', $qpms, true);
		$result = $this->fb_rest->get_query_result($table_name, $query_str);
		//echo json_encode($result,true);
		//fb_pr(json_encode($result,true));
		$state = isset($result["result_set"])?$result["result_set"]:array();
		return $state;
    }
   
}