<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	
	public function __construct() {
		parent::__construct();
		$this->load->database();
	}

	/*function _remap($method,$args)
	{
	 
	    if (method_exists($this, $method))
	    {
	           $this->$method($args);
	    }
	    else
	    {
	        $this->index($method,$args);
	    }
	 
	}*/

	public function index()
	{
		if($this->fb_rest->isloggedin()){
			$query = $this->db->query('SELECT * FROM dashboard ORDER BY position ASC');
			$data['result'] = $query->result();
			$presult_data = array();
			$rec_id = "350414";
			$result = $this->iot_rest->getmeter_list($rec_id);
			//print_r($result);
			if($result["status"] == "success"){
				$data["presult_data"] = $result["data"];
			}

			$this->load->view('dashboard_view',$data);			
		}else{
			redirect('/login');
		}	

	}

	public function view($id){
				
		if($this->fb_rest->isloggedin()){
			$dataArr= array();
			$query = $this->db->query('SELECT * FROM dashboard ORDER BY position ASC');
			$data['result'] = $query->result();
			$dataArr = array_merge(array('result' => $data['result']),array('meter_id'=>$id));
			//print_r($dataArr); exit();
			$this->load->view('dashboard',$dataArr);			
		}else{
			redirect('/login');
		}		
	
	}

    public function singlePower(){
    	$power = $this->getsinglewattsValue(); 
        return $power;      
    }
    
    public function formulaKVA(){
        $PF = $this->getPowerfactor();
        $A = $this->getAmpValue();
        $V = $this->getVoltValue();
        $kVA = round(pow(3,1/3) * $A * $V / 1000,2); 

        echo $kVA;
    }
	
	  public function kvaAjax(){
	  	$phase = $this->input->post('phase');
        $PF = $this->getPowerfactor();
        $A = $this->getAmpValue();
        $V = $this->getVoltValue();

        if($phase==1){
            $kVA = round(($A * $V ) /1000,2);
        }else{
            $kVA = round(pow(3,1/3) * $A * $V / 1000,2); 
        }
        
        echo $kVA;
    }
	public function get_message(){
		$rec_id = "350412";
		$result = $this->iot_rest->get_power_detail($rec_id);
				
		
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($result));
	}
	
	
	public function get_last_message(){
		$msg_id = "350405";
		$apiInstance = new Swagger\Client\Api\ListDeviceMessagesApi(
			// If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
			// This is optional, `GuzzleHttp\Client` will be used as default.
			new GuzzleHttp\Client()
		);
		$atoken = $this->fb_rest->get_fbuser_data("user_token"); // string | Auth token of the logged in user
		$page_size = 1; // int | Maximum number of messages to be listed
		$direction = ""; // string | If direction is specified, **muid** is required
		$muid = $msg_id; // string | Last or First message uuid of the previous list operation, **required** if **direction** is specified

		try {
			$result = $apiInstance->listDeviceMessages($atoken, $page_size, $direction, $muid);
			$mobj = isset($result["0"]) ? $result["0"] : "";
			if(!empty($mobj)){
				$mdata = $mobj->getData();
				$nuuid = $mobj->getNodeUuid();
				fb_pr($result);
				fb_pr($mdata);
				echo "Node uuid: $nuuid <br/>";
				
			}else{
				echo "Empty Message";
			}
			
		} catch (Exception $e) {
			echo 'Exception when calling ListDeviceMessagesApi->listDeviceMessages: ', $e->getMessage(), PHP_EOL;
		}
	}
	
	public function get_new_message(){
		$msg_id = "350412";
		$result = $this->iot_rest->get_message($msg_id);
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($result));
	}
    
    public function getVolt(){
        $result = $this->iot_rest->get_totVolt();

		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($result));
    }
    
    public function getVoltValue(){
        $result = $this->iot_rest->get_totVolt();

        if($result["status"] == "success" && !(empty($result['data']))){
			return $result['data'];
		}else{
			return 0;
		}
    }  
    
     public function getVoltSingle(){
        $result = $this->iot_rest->get_singleVolt();
       if($result["status"] == "success" && !(empty($result['data']))){
			return $result['data'];
		}else{
			return 0;
		}
    }   
    
    
     public function getVoltSinglegauge(){
        $result = $this->iot_rest->get_singleVolt();
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($result));
    }
    
    public function getAmp(){
        $result = $this->iot_rest->get_totAmp();
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($result));
    }
    
     public function getAmpValue(){
        $result = $this->iot_rest->get_totAmp();
        if($result["status"] == "success" && !(empty($result['data']))){
			return $result['data'];
		}else{
			return 0;
		}
    }
    
    public function getPowerfactor(){
        $result = $this->iot_rest->getPowerfactor();
        if($result["status"] == "success" && !(empty($result['data']))){
			return $result['data'];
		}else{
			return 0;
		}
    }
    
    public function getsinglewattsValue()
    {
        
        $result = $this->iot_rest->gettotWatts();
        if($result["status"] == "success" && !(empty($result['data']))){
        	$rp=  $result['data']['rp'];
			$bp= $result['data']['bp'];
			$yp = $result['data']['yp'];
        	$arrPower = array();
        	array_push($arrPower, $rp,$bp,$yp);
        	return $arrPower;
			
		}else{
			return 0;
		}
    }

    public function gettotPower()
    {
        
        $result = $this->iot_rest->gettotWatts();
        if($result["status"] == "success" && !(empty($result['data']))){
			$tp = $result['data']['tp'];
        	return $tp;
		}else{
			return 0;
		}
    }
    
    public function voltampGraph(){
        
            $data=array();
			$presult_data = array();
			$rec_id = "350412";
			$result = $this->iot_rest->get_power_list($rec_id);
			//fb_pr($result);
			//print_r($result);
			if($result["status"] == "success"){
			$pdata = $result["data"];
			
             $this->output
			->set_content_type('application/json')
			->set_output(json_encode($pdata));
			
			}
    }
    
    public function voltampMsg(){
        
        $rec_id = "350412";
		$result = $this->iot_rest->get_power_detail($rec_id);
				
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($result));
        
    }
    
	public function test_message()
	{
		$result = array();
		$record = array();
		$msg_id = "350412";
		$result = $this->iot_rest->get_message($msg_id);
		$row = $result['data'];
		fb_pr($row);
		
			if($row['GEN']==0){
				$id = $this->fb_rest->get_total_count("alerts");
				$table_name = "alert_settings";
				$params =  array("per_page" =>1,"sort_fld" => "createdtime","sort_dir" => "desc","table_name" => $table_name);
				$msg  = $this->fb_rest->list_record($params);
				if($msg['status']=="success"){
					$record = $msg['result_set'];
					foreach($record as $rcd){
						$src = $rcd['_source'];
						
					}
				}
				if($row['TV']>$src['spmin']){
					
					if(($row['RV']>$src['rspmin'] && $row['BV']>$src['bspmin'])||($row['RV']>$src['rspmin'])){
						if($row['RV']>$src['rspmin']){
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "B and R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
					}
					elseif(($row['BV']>$src['bspmin'] && $row['YV']>$src['yspmin'])||($row['YV']>$src['yspmin'])){
						if($row['YV']>$src['yspmin']){
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
					elseif(($row['BV']>$src['bspmin'] && $row['RV']>$src['rspmin']) || ($row['BV']>$src['bspmin'])){
						if($row['BV']>$src['bspmin']){
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
				}
				
				if($row['TV']<$src['spmax']){
					if(($row['RV']<$src['rspmax'] && $row['BV']<$src['bspmax'])||($row['RV']<$src['rspmax'])){
						if($row['RV']<$src['rspmax']){
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "B and R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
					}
					elseif(($row['BV']<$src['bspmax'] && $row['YV']<$src['yspmax'])||($row['YV']<$src['yspmax'])){
						if($row['YV']<$src['yspmax']){
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
					elseif(($row['BV']<$src['bspmax'] && $row['RV']<$src['rspmax']) || ($row['BV']<$src['bspmax'])){
						if($row['BV']<$src['bspmax']){
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
				}
				
				if($row['TPV']>$src['tpmin']){
					if(($row['RBV']>$src['rtpmin'] && $row['BYV']>$src['btpmin'])||($row['RBV']>$src['rtpmin'])){
						if($row['RBV']>$src['rtpmin']){
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "B and R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
					}
					elseif(($row['BYV']>$src['btpmin'] && $row['YRV']>$src['ytpmin'])||($row['YRV']>$src['ytpmin'])){
						if($row['YRV']>$src['tpmin']){
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
					elseif(($row['BYV']>$src['btpmin'] && $row['RBV']>$src['ttpmin']) || ($row['BYV']>$src['btpmin'])){
						if($row['BYV']>$src['btpmin']){
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
				}
				
				if($row['TPV']<$src['tpmax']){
					if(($row['RBV']<$src['rtpmax'] && $row['BYV']<$src['btpmax'])||($row['RBV']<$src['rtpmax'])){
						if($row['RBV']<$src['rtpmax']){
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "B and R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
					}
					elseif(($row['BYV']<$src['btpmax'] && $row['YRV']<$src['ytpmax'])||($row['YRV']<$src['ytpmax'])){
						if($row['YRV']<$src['ytpmax']){
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
					elseif(($row['BYV']<$src['btpmax'] && $row['RBV']<$src['rtpmax']) || ($row['BYV']<$src['btpmax'])){
						if($row['BYV']<$src['btpmax']){
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
				}
				
				if($row['TC']>$src['tcpmin']){
					if(($row['RC']>$src['rcpmin'] && $row['BC']>$src['bcpmin'])||($row['RC']>$src['rcpmin'])){
						if($row['RC']>$src['rcpmin']){
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "R Single Phase Coltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "B and R Single Phase Coltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
					}
					elseif(($row['BC']>$src['bcpmin'] && $row['YC']>$src['ycpmin'])||($row['YC']>$src['ycpmin'])){
						if($row['YC']>$src['ycpmin']){
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "R Single Phase Coltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Coltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
					elseif(($row['BC']>$src['bcpmin'] && $row['RC']>$src['rcpmin']) || ($row['BC']>$src['bcpmin'])){
						if($row['BC']>$src['bcpmin']){
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "R Single Phase Coltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Coltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
				}
				
				if($row['TC']<$src['tcpmax']){
					if(($row['RC']<$src['rcpmax'] && $row['BC']<$src['bcpmax'])||($row['RC']<$src['rcpmax'])){
						if($row['RC']<$src['rcpmax']){
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "R Single Phase Coltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "B and R Single Phase Coltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
					}
					elseif(($row['BC']<$src['bcpmax'] && $row['YC']<$src['ycpmax'])||($row['YC']<$src['ycpmax'])){
						if($row['YC']<$src['ycpmax']){
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "R Single Phase Coltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Coltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
					elseif(($row['BC']<$src['bcpmax'] && $row['RC']<$src['rcpmax']) || ($row['BC']<$src['bcpmax'])){
						if($row['BC']<$src['bcpmax']){
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "R Single Phase Coltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Coltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
				}
				
				if($row['TP']>$src['tppmin']){
					if(($row['RP']>$src['rppmin'] && $row['BP']>$src['bppmin'])||($row['RP']>$src['rppmin'])){
						if($row['RP']>$src['rppmin']){
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "R Single Phase Poltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "B and R Single Phase Poltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
					}
					elseif(($row['BP']>$src['bppmin'] && $row['YP']>$src['yppmin'])||($row['YP']>$src['yppmin'])){
						if($row['YP']>$src['yppmin']){
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "R Single Phase Poltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Poltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
					elseif(($row['BP']>$src['bppmin'] && $row['RP']>$src['rppmin']) || ($row['BP']>$src['bppmin'])){
						if($row['BP']>$src['bppmin']){
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "R Single Phase Poltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Poltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
				}
								
				if($row['TP']<$src['tppmax']){
					if(($row['RP']<$src['rppmax'] && $row['BP']<$src['bppmax'])||($row['RP']<$src['rppmax'])){
						if($row['RP']<$src['rppmax']){
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "R Single Phase Poltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "B and R Single Phase Poltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
					}
					elseif(($row['BP']<$src['bppmax'] && $row['YP']<$src['yppmax'])||($row['YP']<$src['yppmax'])){
						if($row['YP']<$src['yppmax']){
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "R Single Phase Poltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Poltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
					elseif(($row['BP']<$src['bppmax'] && $row['RP']<$src['rppmax']) || ($row['BP']<$src['bppmax'])){
						if($row['BP']<$src['bppmax']){
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "R Single Phase Poltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Poltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
				}
								
				if($row['FRE']> $src['fremin']){
					$table_name= "alerts";
					$idata = array("alert_id" => $id + 1,
					"alert_type" => "warning",				
					"alert_state" => "true", 
					"msg" => "High Frequency",
					"createdtime" => now(), 
					"updatedtime" => now());
					//$result = $this->fb_rest->create_record($table_name, $idata);
					//fb_pr($result);
				}
				
				if($row['FRE']<$src['fremax']){
					$table_name= "alerts";
					$idata = array("alert_id" => $id + 1,
					"alert_type" => "warning",				
					"alert_state" => "true", 
					"msg" => "High Frequency",
					"createdtime" => now(), 
					"updatedtime" => now());
					//$result = $this->fb_rest->create_record($table_name, $idata);
					//fb_pr($result);
				}
				
				if($row['PF']>$src['pfmin']){
					$table_name= "alerts";
					$idata = array("alert_id" => $id + 1,
				"alert_type" => "warning",				
				"alert_state" => "true", 
				"msg" => "Problem Power Factor ",
				"createdtime" => now(), 
				"updatedtime" => now());
					//$result = $this->fb_rest->create_record($table_name, $idata);
					//fb_pr($result);
				}
				
				if($row['PF']<$src['pfmax']){
					$table_name= "alerts";
					$idata = array("alert_id" => $id + 1,
				"alert_type" => "warning",				
				"alert_state" => "true", 
				"msg" => "Problem Power Factor ",
				"createdtime" => now(), 
				"updatedtime" => now());
					//$result = $this->fb_rest->create_record($table_name, $idata);
					//fb_pr($result);
				}
				
			}
			else{
				$id = $this->fb_rest->get_total_count("alerts");
				$table_name = "alert_settings_dg";
				$params =  array("per_page" =>1,"sort_fld" => "createdtime","sort_dir" => "desc","table_name" => $table_name);
				$msg  = $this->fb_rest->list_record($params);
				if($msg['status']=="success"){
					$record = $msg['result_set'];
					foreach($record as $rcd){
						$src = $rcd['_source'];
						
					}
				}
				if($row['TV']>$src['dgspmin']){
					if(($row['RV']>$src['dgrspmin'] && $row['BV']>$src['dgbspmin'])||($row['RV']>$src['dgrspmin'])){
						if($row['RV']>$src['dgrspmin']){
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "B and R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
					}
					elseif(($row['BV']>$src['dgbspmin'] && $row['YV']>$src['dgyspmin'])||($row['YV']>$src['dgyspmin'])){
						if($row['YV']>$src['dgyspmin']){
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
					elseif(($row['BV']>$src['dgbspmin'] && $row['RV']>$src['dgrspmin']) || ($row['BV']>$src['dgbspmin'])){
						if($row['BV']>$src['dgbspmin']){
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
				}
				
				if($row['TV']<$src['dgspmax']){
					if(($row['RV']<$src['dgrspmax'] && $row['BV']<$src['dgbspmax'])||($row['RV']<$src['dgrspmax'])){
						if($row['RV']<$src['dgrspmax']){
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "B and R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
					}
					elseif(($row['BV']<$src['dgbspmax'] && $row['YV']<$src['dgyspmax'])||($row['YV']<$src['dgyspmax'])){
						if($row['YV']<$src['dgyspmax']){
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
					elseif(($row['BV']<$src['dgbspmax'] && $row['RV']<$src['dgrspmax']) || ($row['BV']<$src['dgbspmax'])){
						if($row['BV']<$src['dgbspmax']){
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
				}
				
				if($row['TPV']>$src['dgtpmin']){
					if(($row['RBV']>$src['dgrtpmin'] && $row['BYV']>$src['dgbtpmin'])||($row['RBV']>$src['dgrtpmin'])){
						if($row['RBV']>$src['dgrtpmin']){
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "B and R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
					}
					elseif(($row['BYV']>$src['dgbtpmin'] && $row['YRV']>$src['dgytpmin'])||($row['YRV']>$src['dgytpmin'])){
						if($row['YRV']>$src['dgtpmin']){
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
					elseif(($row['BYV']>$src['dgbtpmin'] && $row['RBV']>$src['dgttpmin']) || ($row['BYV']>$src['dgbtpmin'])){
						if($row['BYV']>$src['dgbtpmin']){
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
				}
				
				if($row['TPV']<$src['dgtpmax']){
					if(($row['RBV']<$src['dgrtpmax'] && $row['BYV']<$src['dgbtpmax'])||($row['RBV']<$src['dgrtpmax'])){
						if($row['RBV']<$src['dgrtpmax']){
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "B and R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
					}
					elseif(($row['BYV']<$src['dgbtpmax'] && $row['YRV']<$src['dgytpmax'])||($row['YRV']<$src['dgytpmax'])){
						if($row['YRV']<$src['dgytpmax']){
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
					elseif(($row['BYV']<$src['dgbtpmax'] && $row['RBV']<$src['dgrtpmax']) || ($row['BYV']<$src['dgbtpmax'])){
						if($row['BYV']<$src['dgbtpmax']){
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "R Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Voltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
				}
				
				if($row['TC']>$src['dgtcpmin']){
					if(($row['RC']>$src['dgrcpmin'] && $row['BC']>$src['dgbcpmin'])||($row['RC']>$src['dgrcpmin'])){
						if($row['RC']>$src['dgrcpmin']){
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "R Single Phase Coltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "B and R Single Phase Coltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
					}
					elseif(($row['BC']>$src['dgbcpmin'] && $row['YC']>$src['dgycpmin'])||($row['YC']>$src['dgycpmin'])){
						if($row['YC']>$src['dgycpmin']){
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "R Single Phase Coltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Coltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
					elseif(($row['BC']>$src['dgbcpmin'] && $row['RC']>$src['dgrcpmin']) || ($row['BC']>$src['dgbcpmin'])){
						if($row['BC']>$src['dgbcpmin']){
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "R Single Phase Coltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Coltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
				}
				
				if($row['TC']<$src['dgtcpmax']){
					if(($row['RC']<$src['dgrcpmax'] && $row['BC']<$src['dgbcpmax'])||($row['RC']<$src['dgrcpmax'])){
						if($row['RC']<$src['dgrcpmax']){
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "R Single Phase Coltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "B and R Single Phase Coltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
					}
					elseif(($row['BC']<$src['dgbcpmax'] && $row['YC']<$src['dgycpmax'])||($row['YC']<$src['dgycpmax'])){
						if($row['YC']<$src['dgycpmax']){
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "R Single Phase Coltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Coltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
					elseif(($row['BC']<$src['dgbcpmax'] && $row['RC']<$src['dgrcpmax']) || ($row['BC']<$src['dgbcpmax'])){
						if($row['BC']<$src['dgbcpmax']){
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "R Single Phase Coltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Coltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
				}
				
				if($row['TP']>$src['dgtppmin']){
					if(($row['RP']>$src['dgrppmin'] && $row['BP']>$src['dgbppmin'])||($row['RP']>$src['dgrppmin'])){
						if($row['RP']>$src['dgrppmin']){
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "R Single Phase Poltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "B and R Single Phase Poltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
					}
					elseif(($row['BP']>$src['dgbppmin'] && $row['YP']>$src['dgyppmin'])||($row['YP']>$src['dgyppmin'])){
						if($row['YP']>$src['dgyppmin']){
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "R Single Phase Poltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Poltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
					elseif(($row['BP']>$src['dgbppmin'] && $row['RP']>$src['dgrppmin']) || ($row['BP']>$src['dgbppmin'])){
						if($row['BP']>$src['dgbppmin']){
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "R Single Phase Poltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Poltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
				}
								
				if($row['TP']<$src['dgtppmax']){
					if(($row['RP']<$src['dgrppmax'] && $row['BP']<$src['dgbppmax'])||($row['RP']<$src['dgrppmax'])){
						if($row['RP']<$src['dgrppmax']){
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "R Single Phase Poltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "B and R Single Phase Poltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
					}
					elseif(($row['BP']<$src['dgbppmax'] && $row['YP']<$src['dgyppmax'])||($row['YP']<$src['dgyppmax'])){
						if($row['YP']<$src['dgyppmax']){
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "R Single Phase Poltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Poltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
					elseif(($row['BP']<$src['dgbppmax'] && $row['RP']<$src['dgrppmax']) || ($row['BP']<$src['dgbppmax'])){
						if($row['BP']<$src['dgbppmax']){
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "R Single Phase Poltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
						
						else{
							$table_name= "alerts";
							$idata = array("alert_id" => $id + 1, 
							"alert_state" => "true", 
							"msg" => "B and Y Single Phase Poltage is Low",
							"createdtime" => now(), 
							"updatedtime" => now());
							/* $result = $this->fb_rest->create_record($table_name, $idata);
							fb_pr($result); */
						}
					}
				}
								
				if($row['FRE']> $src['dgfremin']){
					$table_name= "alerts";
					$idata = array("alert_id" => $id + 1,
					"alert_type" => "warning",				
					"alert_state" => "true", 
					"msg" => "High Frequency",
					"createdtime" => now(), 
					"updatedtime" => now());
					//$result = $this->fb_rest->create_record($table_name, $idata);
					//fb_pr($result);
				}
				
				if($row['FRE']<$src['dgfremax']){
					$table_name= "alerts";
					$idata = array("alert_id" => $id + 1,
					"alert_type" => "warning",				
					"alert_state" => "true", 
					"msg" => "High Frequency",
					"createdtime" => now(), 
					"updatedtime" => now());
					//$result = $this->fb_rest->create_record($table_name, $idata);
					//fb_pr($result);
				}
				
				if($row['PF']>$src['dgpfmin']){
					$table_name= "alerts";
					$idata = array("alert_id" => $id + 1,
				"alert_type" => "warning",				
				"alert_state" => "true", 
				"msg" => "Problem Power Factor ",
				"createdtime" => now(), 
				"updatedtime" => now());
					//$result = $this->fb_rest->create_record($table_name, $idata);
					//fb_pr($result);
				}
				
				if($row['PF']<$src['dgpfmax']){
					$table_name= "alerts";
					$idata = array("alert_id" => $id + 1,
				"alert_type" => "warning",				
				"alert_state" => "true", 
				"msg" => "Problem Power Factor ",
				"createdtime" => now(), 
				"updatedtime" => now());
					//$result = $this->fb_rest->create_record($table_name, $idata);
					//fb_pr($result);
				}
				

			}
	}
	
	public function send_mail() { 

		$config = Array(
			'protocol' => 'smtp',
			'smtp_host' => 'ssl://smtp.googlemail.com',
			'smtp_port' => 465,
			'smtp_user' => 'lalasarjun@gmail.com',
			'smtp_pass' => 'sourashtraclg',
			'mailtype'  => 'html', 
			'charset'   => 'iso-8859-1'
		);
		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");

		$this->email->from('lalasarjun@gmail.com', 'JeyanthiLal');
        $this->email->to('lalaslal@gmail.com'); 

        $this->email->subject('Email Test');
        $this->email->message('Testing the email class.');

		$result = $this->email->send();
        echo $result;
      } 

      public function genset(){

		if($this->fb_rest->isloggedin()){
			$this->load->view('dashboard');			
		}else{
			redirect('/login');
		}

      }

      public function getDG_status(){

      	
      	$rec_id = "350412";
		$result = $this->iot_rest->getDG($rec_id);
		 if($result["status"] == "success" && !(empty($result['data']))){
			$status=  $result['data']['gen'];
			$time= $result['data']['createdtime'];
        	$arrPower = array();
        	$arrPower= array_merge(array('status' =>$status) ,array('time'=>$time));
        	//return $arrPower;
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($arrPower));
		}else{
			return 0;
		}

      }
	
	public function getdg_reports() {
		if($this->fb_rest->isloggedin()){
			$data=array();
			$presult_data = array();
			$rec_id = "350412";
			$result = $this->iot_rest->getdg_record($rec_id);
			//fb_pr($result); exit;
            $this->load->view('include/header');
            $this->load->view('include/left-sidebar');
			if($result["status"] == "success"){
				
				$data["title"] = "DG Reports";
				$real_data = $result['aggs'];
				foreach($real_data as $row){
					$aggregation_data[] = $row['tops']['hits']['hits'];
					
				}
				$data["presult_data"] = $aggregation_data;
				$this->load->view('reports',$data);	
			}
			else{
				$this->load->view("layout/error", $data);
			}
            $this->load->view('include/footer');
		}else{
			redirect('/login');
		}
	}
	public function geteb_reports() {
		if($this->fb_rest->isloggedin()){
			$data=array();
			$presult_data = array();
			$rec_id = "350412";
			$result = $this->iot_rest->geteb_record($rec_id);
			//fb_pr($result); exit;
            $this->load->view('include/header');
            $this->load->view('include/left-sidebar');
			if($result["status"] == "success"){
				$data["title"] = "EB Reports";
				$real_data = $result['aggs'];
				foreach($real_data as $row){
					$aggregation_data[] = $row['tops']['hits']['hits'];
					
				}
				//fb_pr($aggregation_data);exit;
				$data["presult_data"] = $aggregation_data;
				$this->load->view('reports',$data);	
			}
			else{
				$this->load->view("layout/error", $data);
			}
            $this->load->view('include/footer');
		}else{
			redirect('/login');
		}
	}

	public function dragdrop(){
		$action = $this->input->post('action');
		$updateRecordsArray = $this->input->post('recordsArray');
		$listingCounter = 1; 
		foreach ($updateRecordsArray as $recordIDValue) {

			$this->db->set('position', $listingCounter);
			$this->db->where('id', $recordIDValue);
			$update = $this->db->update('dashboard'); 

	        $listingCounter += 1;

     }
	}


	public function get_meters(){
		$rec_id = "350414";
		$result = $this->iot_rest->getmeter_list($rec_id);
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($result));
	}	
	
	public function get_dg_running_hrs(){
		if($this->fb_rest->isloggedin()){
			$table_name = "genset_activity";
			$data=array();
			$from = 0;
			$size = 0;
			$orderfld = "last_on_time";
			$orderdir = "desc";

			$this->load->library('parser');
			$this->parser->set_delimiters("__","__");
			$qpms = array("size" => $size, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir);
			$query_str = $this->parser->parse('query/script_fields', $qpms, true);

	

			$result = $this->fb_rest->get_query_agg_result($table_name, $query_str);
			$this->load->view('include/header');
            $this->load->view('include/left-sidebar');
			if($result["status"]=="success"){
				$aggs["aggs"] = $result["aggs"];
				foreach($aggs as $row){
					
					foreach($row as $result){
						$runtime=$result["key"];
						 
						 $src = $result['tops']['hits']['hits'];
						
						foreach($src as $res){
							$temp = $res["_source"];
							$temp["rtime"]=$runtime;
							$source[] = $temp; 
							
						} 
						
					}
					
				}
				$result_set[] = $source;
				
				//fb_pr($result_set); exit;
				$data["aggs"] = $result_set;
				$this->load->view('dg_running',$data);	
			}
			else{
				$this->load->view("layout/error", $data);
			}
            $this->load->view('include/footer');
		}else{
			redirect('/login');
		}
	}

	public function getFrequency(){
		$result= $this->iot_rest->getFrequency();
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($result));		
	}

	public function getrecord_byValue(){

		$val = $this->input->post('filter');
		
		$result= $this->iot_rest->get_recordsByValue('1y');

		$data = $result['data'];
		$dataCount = count($data);
		if($result["status"]=="success" && $dataCount>0){
			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($data));
		}else{
			echo "Search failed";
		}

	}
	public function get_eb_running_hrs(){
		if($this->fb_rest->isloggedin()){
			$table_name = "eb_status";
			$data=array();
			$from = 0;
			$size = 0;
			$orderfld = "status_on";
			$orderdir = "desc";

			$this->load->library('parser');
			$this->parser->set_delimiters("__","__");
			$qpms = array("size" => $size, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir);
			$query_str = $this->parser->parse('query/eb_script_fields', $qpms, true);

	

			$result = $this->fb_rest->get_query_agg_result($table_name, $query_str);
			$this->load->view('include/header');
            $this->load->view('include/left-sidebar');
			if($result["status"]=="success"){
				$aggs["aggs"] = $result["aggs"];
				foreach($aggs as $row){
					
					foreach($row as $result){
						$runtime=$result["key"];
						$src = $result['tops']['hits']['hits'];
						foreach($src as $res){
							$temp = $res["_source"];
							$temp["rtime"]=$runtime;
							$source[] = $temp; 
							
						} 
						
					}
					
				}
				$result_set[] = $source;
				
				//fb_pr($result_set); exit;
				$data["aggs"] = $result_set;
				$this->load->view('dg_running',$data);	
			}
			else{
				$this->load->view("layout/error", $data);
			}
            $this->load->view('include/footer');
		}else{
			redirect('/login');
		}
	}
	public function test(){
		
		$table_name = "genset_activity";
			$data=array();
			$from = 0;
			$size = 0;
			$orderfld = "last_on_time";
			$orderdir = "desc";

			$this->load->library('parser');
			$this->parser->set_delimiters("__","__");
			$qpms = array("size" => $size, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir);
			$query_str = $this->parser->parse('query/script_fields', $qpms, true);
			$result = $this->fb_rest->get_query_agg_result($table_name, $query_str);
		if($result["status"]=="success"){
			$aggs["aggs"] = $result["aggs"];
			foreach($aggs as $row){
				
				foreach($row as $result){
					$runtime=$result["key"];
					
				}
				echo $no_of_hrs = gmdate('H:i:s',$runtime);
			}
			return $no_of_hrs;
		}
		else{
			$this->load->view("layout/error", $data);
		}
    }
}
