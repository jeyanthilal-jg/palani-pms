<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	
	
	public function __construct() {
		parent::__construct();
		$this->load->helper('date');
		$this->table="alert_settings";
		
	}

	public function index()
	{
		/*if($this->fb_rest->isloggedin()){
			$this->load->view('settings_content');
		}	*/		
		
		  if($this->fb_rest->isloggedin()){
			$table_name=$this->table;
			$data = array();
			$table_name = "alert_settings_dg";
			$params =  array("page_no" => 1, "per_page" =>1, "uri_segment" => "2",
		"search" => "", "sort_fld" => "updatedtime", "sort_dir" => "desc", "page_burl" => "", "table_name" => $table_name);
			$msg2  = $this->fb_rest->list_record($params);
			$table_name = "alert_settings";
			$params =  array("page_no" => 1, "per_page" =>1, "uri_segment" => "2",
		"search" => "", "sort_fld" => "updatedtime", "sort_dir" => "desc", "page_burl" => "", "table_name" => $table_name);
			$msg  = $this->fb_rest->list_record($params);
			//fb_pr($msg);
			$table_name1 = "alert_notify";
			$params1 =  array("page_no" => 1, "per_page" =>10, "uri_segment" => "2",
		"search" => "", "sort_fld" => "createdtime", "sort_dir" => "desc", "page_burl" => "", "table_name" => $table_name1);
			$msg1 = $this->fb_rest->list_record($params1);
			if($msg["status"] == "success" && $msg1['status']== 'success' && $msg2['status']== 'success')
			{
				$data["result_set"] = $msg["result_set"];
				$data["result_set1"] = $msg1["result_set"];
				$data["dgresult_set"] = $msg2["result_set"];
				//fb_pr($data);
				$this->load->view('settings_content', $data);
			}
			else{
				$this->load->view("layout/error", $data);
			}
		} else{
			redirect('/login');
		} 
	}
	
	public function create(){
		$table_name=$this->table;
		//$tbl_id= 350401;
		$form_data = $this->input->post();
		$form_data['createdtime']=now();
		$form_data['updatedtime']=now();
		$result = $this->fb_rest->create_record($table_name,$form_data);
//		print_r($result);
		if($result['status']=="success"){
		$this->session->set_flashdata('c_success',"Success");
		redirect('/settings');
		}else{
		$this->session->set_flashdata('c_failed',"Failed");
		redirect('/settings');
		}
	}
    
	public function create_genset(){
		$table_name="alert_settings_dg";
		//$tbl_id= 350401;
		$form_data = $this->input->post();
		$form_data['createdtime']=now();
		$form_data['updatedtime']=now();
		$result = $this->fb_rest->create_record($table_name,$form_data);
//		print_r($result);
		if($result['status']=="success"){
		$this->session->set_flashdata('c_dg_success',"Success");
		redirect('/settings');
		}else{
		$this->session->set_flashdata('c_dg_failed',"Failed");
		redirect('/settings');
		}
	}
	
	public function add_record(){
		$table_name="alert_notify";
		//$tbl_id= 350401;
		$form_data = $this->input->post();
		$form_data['createdtime']=now();
		$form_data['updatedtime']=now();
		$result = $this->fb_rest->create_record($table_name,$form_data);
//		print_r($result);
		if($result['status']=="success"){
			$this->session->set_flashdata('a_success',"Success");
			redirect('/settings');
		}else{
			$this->session->set_flashdata('a_failed',"Failed");
			redirect('/settings');
		}
	}
	
	
	function delete(){
		// echo "welcome";
		$table_name="alert_notify";
		$rkey = $this->input->post("rid");	
		$result= $this->fb_rest->delete_record($table_name, $rkey);
		
		if($result['status']=="success"){
			$this->session->set_flashdata('delete_success',"delete success");
			redirect('/settings');
		}else{
			$this->session->set_flashdata('delete_failed',"delete failed");
			redirect('/settings');
		}
	}
	
	function edit(){
		$data = array();
		$table_name="alert_notify";
		$rkey = $this->input->post("rid");
		$record= $this->fb_rest->get_record($table_name, $rkey);
		//fb_pr($record);
		if($record["status"] == "success")
		 {	
		 	$data['record'] = $record["result_set"];
			$data['rkey'] = $rkey;
			$this->load->view("settings_edit_content", $data);
		 }
		
	}
	function update(){
		$table_name="alert_notify";
		$form_data = $this->input->post();
		$form_data['updatedtime']=now();	
		//$built_date = $this->input->post("built_date", true);
		
		$rkey = $this->input->post("rkey");

		$oresult = $this->fb_rest->get_record($table_name, $rkey);
		$orecord = $oresult["result_set"];

		$result = $this->fb_rest->update_record($table_name,$form_data,$rkey);
		
//		print_r($result);
		if($result['status']=="success"){
		$this->session->set_flashdata('update_success',"update success");
		redirect('/settings');
		}else{
		$this->session->set_flashdata('update_failed',"update failed");
		redirect('/settings');
		}
	}
}
